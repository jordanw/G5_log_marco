<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Almoxarifado extends Model
{
    protected $fillable = [
        '_date'=>'required',
        'produto'=>'required',
        'frota_equipamento'=>'required',
        'nota_fiscal'=>'required',
        'responsavel'=>'required',
        'custo_unitario'=>'required',
        'quantidade'=>'required',
        'fornecedor'=>'required',
        'estoque_estimado'=>'required',
        'data_de_entrega'=>'required',
        'total_de_compra'=>'required',
        'total_de_saida'=>'required',
        'numero_os'=>'required'
    ];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'almoxarifados';
}
