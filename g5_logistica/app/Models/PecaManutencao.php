<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\EntradaVeiculo;

class PecaManutencao extends Model
{
    protected $fillable =['veiculo_id','peca_manutencao','criado_por'];


      public function acesso_veiculos()
      {
        return $this->belongsToMany('App\Models\AcessoVeiculo','peca_manutencao_veiculo','veiculo_id','peca_manutencao_id');
      }


}
