<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AcessoVeiculo extends Model
{
  protected $table = 'veiculos';

  protected $fillable = [

    'entrada_veiculo_id',
    'os_num',
    'falhas',
    'tempo_estimado',
    'concedido_acesso_por',
    'observacao',
    'date_concedido_acesso',
    'concluido_acesso_por',
    'comentarios',
    'date_concluido_acesso',
    'ativo',

  ];

  public function entrada()
  {
    return $this->belongsTo('App\Models\EntradaVeiculo','entrada_veiculo_id');
  }

  public function local_manutencaos()
  {
    return $this->belongsToMany('App\Models\LocalManutencao','local_manutencao_veiculo','veiculo_id','local_manutencao_id')->withTimestamps();
  }

  public function peca_manutencaos()
  {
    return $this->belongsToMany('App\Models\PecaManutencao','peca_manutencao_veiculo','veiculo_id','peca_manutencao_id')->withTimestamps();
  }



}
