<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Placa extends Model
{

  protected $fillable = [
      'placa',
      'marca',
      'modelo',
      'ano_fabricacao',
      'cor',
      'combustivel',
  ];


}
