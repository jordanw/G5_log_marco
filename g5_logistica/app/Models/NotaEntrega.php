<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Entrada;

class NotaEntrega extends Model
{
  protected $table = 'nota_entregas';

  protected $fillable = [
    'entrada_id',
    'qtdade',
    'date',
    'titulo',
    'autor',
    'isbn',
    'user_cadastrou',
    'percent',
    'valor_unit',
    'valor_percent',
    'valor_total',
    'valor_pvp',
    'ativo',
    'nf',
    'prestado_por',
    'date_prestado',
    'restaurado_por',
    'date_restaurado'
  ];

  public function entrada()
  {
    return $this->belongsTo('App\Models\Entrada');
  }


}
