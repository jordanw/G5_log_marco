<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocalManutencao extends Model
{
      protected $fillable =['veiculo_id','local_manutencao','criado_por'];


  public function acesso_veiculos()
  {
    return $this->belongsToMany('App\Models\AcessoVeiculo','local_manutencao_veiculo','veiculo_id','local_manutencao_id');
  }

}
