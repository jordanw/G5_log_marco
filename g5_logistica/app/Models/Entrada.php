<?php

namespace App\Models;
use App\Models\Entrada;
use SoftDeletes;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Entrada extends Model
{
  protected $table = 'entradas';
  protected $fillable = [

    'nome',
    'image',
    'user_id',
    'cpf',
    'identidade',
    'telefone',
    'sexo',
    'tipo',
    'responsavel',
    'cnpj',
    'email',
    'ativo',

  ];
    protected $dates = ['deleted_at'];

    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function nota_entregas()
    {
      return $this->hasMany('App\Models\NotaEntrega');
    }

    public function tasks()
    {
      return $this->hasMany('App\Models\Task');
    }

    public function acessos()
    {
      return $this->hasMany('App\Models\Acesso');
    }


  }
