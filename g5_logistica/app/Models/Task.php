<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
  protected $fillable = [
    'user_id',
    'entrada_id',
    'tarefa',
    'observacao',
    'data_conclusao',
    'ativo',
    'responsavel',
    'data_estimada',
    'telefone',
    'email',
  ];

  public function entrada()
  {
    return $this->belongsTo('App\Models\Entrada');
  }



}
