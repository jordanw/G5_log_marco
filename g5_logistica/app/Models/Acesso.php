<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Acesso extends Model
{
  protected $table = 'acessos';

  protected $fillable = [

    'entrada_id',
    'tipo_fornecedor',
    'tempo_estimado',
    'observacao',
    'concedido_acesso_por',
    'date_concedido_acesso',
    'concluido_acesso_por',
    'date_concluido_acesso',
    'ativo',
    'local_especifico',
    'local_acesso'

  ];

  public function entrada()
  {
    return $this->belongsTo('App\Models\Entrada');
  }

}
