<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\EntradaVeiculo;
use App\User;

class EntradaVeiculo extends Model
{

  protected $table = 'entrada_veiculos';

  protected $fillable = [

    'image',
    'user_id',
    'city',
    'model_year',
    'plate',
    'color',
    'status_code',
    'brand',
    'return_code',
    'date',
    'state',
    'chassis',
    'year',
    'return_message',
    'model',
    'status_message',
    'responsavel',
    'observacao',
    'ativo',

  ];

  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public function acessos()
  {
    return $this->hasMany('App\Models\AcessoVeiculo');
  }

  public function manutencao()
  {
    return $this->hasMany('App\Models\Manutencao');
  }

  public function peca()
  {
    return $this->hasManyThrough('App\Models\PecaManutencao',
        'App\Models\AcessoVeiculo','peca_manutencao_id','veiculo_id');
  }

}
