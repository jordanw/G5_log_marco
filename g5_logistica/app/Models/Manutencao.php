<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Manutencao extends Model
{
  protected $fillable = [

    'entrada_veiculo_id',
    'os_num',
    'falha',
    'local',
    'tempo_estimado',
    'tipo_manutencao',
    'concedido_acesso_por',
    'observacao',
    'date_concedido_acesso',
    'concluido_acesso_por',
    'comentarios',
    'date_concluido_acesso',
    'ativo',
     'ativo_view',

  ];

  protected $dates = ['date_concedido_acesso','date_concluido_acesso']; //'tempo_estimado'
  protected $table = "manutencaos";

  public function entrada()
  {
    return $this->belongsTo('App\Models\EntradaVeiculo','entrada_veiculo_id','id');
  }
}
