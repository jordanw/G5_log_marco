<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AcessoVeiculo;
use App\Models\EntradaVeiculo;
use App\Models\Manutencao;
use App\Models\PecaManutencao;
use App\Models\LocalManutencao;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\App;
use Session;
use Auth;
use DB;
use Validator;
class ManutencaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $title  = 'Veículos';
    public $route = 'veiculos';
    protected $totalPage = 12;
    public $view = 'painel.manutencaoVeiculos';

    /* as variaveis abaixo referem-se ao formulario de editar, para manter a view generica
        para qualquer parte do programa que deseje utiliza-la, atribuir ao ManutencaoController
    a responsabilidade de passar os dados necessarios para a atualização das manutenções.

    */
    public $peca = array(
        'CILINDRO MESTRE AVARIADO' => 'CILINDRO MESTRE AVARIADO',
        'SENSOR DO PEDAL FALHANDO' => 'SENSOR DO PEDAL FALHANDO',
        'VAZAMENTO NO CILINDRO DO CLAMP' => 'VAZAMENTO NO CILINDRO DO CLAMP',
        'BAIXO NÍVEL DE ÓLEO HIDRÁULICO' => 'BAIXO NÍVEL DE ÓLEO HIDRÁULICO',
        'BAIXO NÍVEL DE ÓLEO DE TRANSMISSÃO' => 'BAIXO NÍVEL DE ÓLEO DE TRANSMISSÃO',
        'PARADA PROGRAMADA DE 500 HORAS' => 'PARADA PROGRAMADA DE 500 HORAS',
        'VAZAMENTO NA MANGUEIRA DE ELEVAÇÃO' => 'VAZAMENTO NA MANGUEIRA DE ELEVAÇÃO',
        'VAZAMENTO NA MANGUEIRA DE COMANDO' => 'VAZAMENTO NA MANGUEIRA DE COMANDO',
        'RODOGÁS ENCHARCADO' => 'RODOGÁS ENCHARCADO',
        'VAZAMENTO NO CILINDRO DE INCLINAÇÃO' => 'VAZAMENTO NO CILINDRO DE INCLINAÇÃO',
        'BAIXO NÍVEL DE ÁGUA DO RADIADOR' => 'BAIXO NÍVEL DE ÁGUA DO RADIADOR',
        'INSTALAÇÃO DO RADIADOR' => 'INSTALAÇÃO DO RADIADOR',
        'VAZAMENTO DE AGUA' => 'VAZAMENTO DE AGUA',
        'BATERIA DESCARREGADA' => 'BATERIA DESCARREGADA',
        'VAZAMENTO DE ÓLEO NA ALAVANCA DA CABINE' => 'VAZAMENTO DE ÓLEO NA ALAVANCA DA CABINE',
        'VAZAMENTO NA MANGUEIRA DO ORBITROL' => 'VAZAMENTO NA MANGUEIRA DO ORBITROL',
        'CLAMP AVARIADO' => 'CLAMP AVARIADO',
        'VAZAMENTO NA MANGUEIRA  DO CILINDRO DE DIREÇÃO' => 'VAZAMENTO NA MANGUEIRA  DO CILINDRO DE DIREÇÃO',
        'VÁLVULAS DE RETENÇÃO DO CLAMP OBSTRUÍDA' => 'VÁLVULAS DE RETENÇÃO DO CLAMP OBSTRUÍDA',
        'CABO DA BATERIA FOLGADO' => 'CABO DA BATERIA FOLGADO',
        'BORNES DA BATERIA DESGASTADOS' => 'BORNES DA BATERIA DESGASTADOS',
        'VAZAMENTO NA MANGUEIRA DE ACIONAMENTO DO CLAMP' => 'VAZAMENTO NA MANGUEIRA DE ACIONAMENTO DO CLAMP',
        'VÁLVULA DE RETENÇÃO AVARIADA' => 'VÁLVULA DE RETENÇÃO AVARIADA',
        'PNEU FURADO' => 'PNEU FURADO',
        'FAROL DIANTEIRO AVARIADO' => 'FAROL DIANTEIRO AVARIADO',
        'MOTOR DE PARTIDA AVARIADO' => 'MOTOR DE PARTIDA AVARIADO',
        'ROLAMENTO DO CARRO DE CARGA AVARIADO' => 'ROLAMENTO DO CARRO DE CARGA AVARIADO',
        'VAZAMENTO NA BOMBA HIDRÁULICA' => 'VAZAMENTO NA BOMBA HIDRÁULICA',
        'VAZAMENTO NA MANGUEIRA DO CILINDRO DE INCLINAÇÃO' => 'VAZAMENTO NA MANGUEIRA DO CILINDRO DE INCLINAÇÃO',
        'BAIXO NÍVEL DE ÓLEO DE MOTOR' => 'BAIXO NÍVEL DE ÓLEO DE MOTOR'

    );
    public $local = array('OFICINA VILA MARANHAO' => 'OFICINA VILA MARANHAO',
        'OFICINA PORTO' => 'OFICINA PORTO');
    public function __construct(Manutencao $manutencao)
    {
        $this->middleware('auth');
        $this->model = $manutencao;

    }

    public function index()
    {
        $title =  $this->title;
        $dataAcesso = $this->model->with('entrada')
            ->where('ativo', '1')
            ->whereHas('entrada', function ($query) {
                $query->where('ativo', '=', 1);
            })->get();
        // $dataA = Carbon::now();
        // $dataA = $this->model->pluck('tempo_estimado');
        // $dataA =
        // $date = $date->addMonth();
        // $dataB =
        foreach ($dataAcesso as $key => $value) {
            # code...
            // dd($value->created_at,$value->tempo_estimado,$value->date_concedido_acesso,$value->date_concluido_acesso);
        }
        // dd($dataAcesso);
        return view("{$this->view}.index",compact('title','users','dataAcesso'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function criar($id)
    {


        //dd($id);
        $tiltle = "Veículo ";
        // dd($data->nome_pesq);
        $dataAtual = Carbon::now();


        $peca  = PecaManutencao::pluck('peca_manutencao','peca_manutencao');
        $local = LocalManutencao::pluck('local_manutencao','local_manutencao');


        $dataAcesso = Manutencao::with('entrada')
            ->where('entrada_veiculo_id', $id)
            ->whereHas('entrada', function ($query) use ($id) {
                $dataAtual = Carbon::now();
                $query->where('entrada_veiculo_id', $id);
            })->first();


        // dd($dataAcesso);


        $acessosAtivos = Manutencao::with('entrada')
            ->where('ativo', 1)
            ->whereHas('entrada', function ($query) use ($id) {
                $dataAtual = Carbon::now();
                $query->where('id', $id);
            })->count();

        $acessosFechados = Manutencao::with('entrada')
            ->where('ativo', 0)
            ->whereHas('entrada', function ($query) use ($id) {
                $dataAtual = Carbon::now();
                $query->where('id', $id);
            })->count();

        // dd($acessosAtivos,$acessosFechados);
        // $prest = Entrada::with('acessos')->findOrFail($id);

        $ultimoAcesso = Manutencao::with('entrada')
            ->where('date_concedido_acesso', '<' , $dataAtual)
            ->whereHas('entrada', function ($query) use ($id) {
                $dataAtual = Carbon::now();
                $query->where('date_concedido_acesso', '<' , $dataAtual)
                    ->where('id', $id);
            })
            ->where('ativo', 0)
            ->orderBy('id', 'desc')
            ->pluck('date_concluido_acesso')
            ->first();

        // dd($ultimoAcesso->diffInDays($dataAtual));

        $user2 = EntradaVeiculo::with('manutencao')->findOrFail($id);

        $user = EntradaVeiculo::with('manutencao')->findOrFail($id);
        // dd($user);

        $editar = null;
        return view("{$this->view}.create",
            compact('tiltle','peca','local','dataAcesso',
                'editar','user','user2','acessosAtivos','acessosFechados','ultimoAcesso'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {

        $this->validate($request, [
            'os_num'  => 'required',
            'tipo_manutencao'   => 'required',
            'falha'   => 'required',
            'local'   => 'required',
            'tempo_estimado'   => 'required',
        ], [
            'os_num.required' => 'Campo Obrigatório: Os_Num',
            'tipo_manutencao.required'  => 'Campo Obrigatório:Tipo Manutenção',
            'falha.required'  => 'Campo Obrigatório: Falha Ocorrida',
            'local.required'  => 'Campo Obrigatório: Local Manutenção',
            'tempo_estimado.required'  => 'Campo Obrigatório:Tempo Estimado',
        ]);




        $dataUser = new $this->model;
        $dt = Carbon::now();
        $dataUser = $request->all();
        $dataUser['falhas'] = $request['falhas'];
        $dataUser['peca_manutencao']  = $request['peca_manutencao']+1;
        $dataUser['local_manutencao'] = $request['local_manutencao']+1;
        // $dataUser['tempo_estimado'] = \Carbon\Carbon::now()->format('Y-m-d');
        $dataUser['concedido_acesso_por'] = Auth::user()->name;
        $dataUser['date_concedido_acesso'] = \Carbon\Carbon::now()->format('Y-m-d H:i:s');

        if ($dataUser['tempo_estimado'] == 1) {
            $dataUser['tempo_estimado'] = $dt->addHour();
        }elseif ($dataUser['tempo_estimado'] == 2) {
            $dataUser['tempo_estimado'] = $dt->addHours(2);
        }elseif ($dataUser['tempo_estimado'] == 4) {
            $dataUser['tempo_estimado'] = $dt->addHours(4);
        }elseif ($dataUser['tempo_estimado'] == 6) {
            $dataUser['tempo_estimado'] = $dt->addHours(6);
        }elseif ($dataUser['tempo_estimado'] == 8) {
            $dataUser['tempo_estimado'] = $dt->addHours(8);
        }elseif ($dataUser['tempo_estimado'] == 10) {
            $dataUser['tempo_estimado'] = $dt->addHours(10);
        }elseif ($dataUser['tempo_estimado'] == 25) {
            $dataUser['tempo_estimado'] = $dt->addHours(25);
        }elseif ($dataUser['tempo_estimado'] == 50) {
            $dataUser['tempo_estimado'] = $dt->addHours(50);
        }elseif ($dataUser['tempo_estimado'] == 100) {
            $dataUser['tempo_estimado'] = $dt->addHours(100);
        }elseif ($dataUser['tempo_estimado'] == 150) {
            $dataUser['tempo_estimado'] = $dt->addHours(150);
        }elseif ($dataUser['tempo_estimado'] == 200) {
            $dataUser['tempo_estimado'] = $dt->addHours(200);
        }elseif ($dataUser['tempo_estimado'] == 500) {
            $dataUser['tempo_estimado'] = $dt->addHours(500);
        }else {
            $dataUser['tempo_estimado'] = '';
        }
        // dd($dataUser);

        // if ($dataUser['nascimento'] == null) {
        //   $dataUser['nascimento'] == null;
        // }else {
        //   $dataUser['nascimento'] = str_replace('/', '-', $dataUser['nascimento']);
        //   $dataUser['nascimento'] = date('Y-m-d', strtotime($dataUser['nascimento']));
        // }

        $dataUser['entrada_veiculo_id'] = $request['id'];
        //
        $insert = $this->model->create($dataUser);
        // dd($insert);


        // dd($insert);
        // dd($dataUser);



        if ($insert) {


            return redirect()
                ->route('/painelVeiculos')
                ->with(['status' => 'Entrada cadastrada com sucesso!']);
        } else {
            return redirect()
                ->route('manutencao.veiculos.create', ['id' => $id])
                ->withErrors(['errors' => 'Falha ao cadastrar!, veja a conexão de Internet!'])
                ->withInput();
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Manutencao::findOrFail($id);
        return view('painel.manutencaoVeiculos.form._formBase-manutencao_edita',
            ['user'=>$user, 'peca' => $this->peca, 'local' => $this->local]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $manutencaos = Manutencao::findOrFail($id);
        $manutencaos->os_num = $request->os_num;
        $manutencaos->tipo_manutencao = $request -> tipo_manutencao;
        $manutencaos->falha = $request -> falha;
        $manutencaos->local = $request ->local;
        $manutencaos->tempo_estimado = $request -> tempo_estimado;
        $manutencaos->observacao = $request -> observacao;
        $manutencaos->concluido_acesso_por = Auth::user()->name;
        $manutencaos->date_concedido_acesso = \Carbon\Carbon::now()->format('Y-m-d H:i:s');

        $verify = $manutencaos->update();


        if($verify){
            Session::flash('manutencaoMessage', 'Manutenção atualizada com sucesso');
            return redirect('/painel/manutenção-veiculo/7/criar');
        }
        else
        {
            Session::flash('manutencaoMessage', 'Atualização falhou. Tente novante');
            return redirect()->back();
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $dataUser = $request->all();
        $dataUser = Manutencao::findOrFail($id);
        $dataUser['comentarios'] = $request->input('id');
        $dataUser['ativo']= 0;
        $dataUser['concluido_acesso_por']= Auth::user()->name;
        $dataUser['date_concluido_acesso'] = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
        // dd($dataUser);
        // dd($dataUser);

        $dataUser->save();
        // Session::flash('flash_message','Adiantamento excluído com sucesso');
        return redirect()->back()->with(['delete' => 'Manutenção concluída com sucesso!']);
          //return redirect()->route('manutencao.veiculos.destroy');

    }

    public function deletaManutencaoConcluida(Request $request, $id)
    {

        $dataUser = $request->all();
        $dataUser = Manutencao::findOrFail($id);
        $dataUser['comentarios'] = $request->input('id');
        $dataUser['ativo']= 0;
        $dataUser['ativo_view'] = 0;
        $dataUser['concluido_acesso_por']= Auth::user()->name;
        $dataUser['date_concluido_acesso'] = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
        // dd($dataUser);
        // dd($dataUser);

        $dataUser->save();
        Session::flash('deleteMessage','Manutenção excluída com sucesso');
        return redirect()->back();
        //return redirect()->route('manutencao.veiculos.destroy');
    }
}
