<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Entrada;
use App\Models\EntradaVeiculo;
use App\Models\Acesso;
use App\Models\Manutencao;
use App\Models\AcessoVeiculo;
use App\Models\PecaManutencao;
use Carbon\Carbon;
use App\User;
use Session;
use Auth;
use DB;


class PainelController extends Controller
{
  private $painel;
  protected $totalPage = 10;
  public $title  = 'Dashboard';

  public function __construct() {
    $this->middleware('auth');
  }

  public function index()
  {

    $title = $this->title;
    $date = Carbon::now('America/Sao_Paulo')->format('h:i a');
    $tiltle = "Cadastrar ";
    // dd($data->nome_pesq);
    $dataAtual = date('Y-m-d');
    $horaAtual = date('Y-m-d H:i:s');

    $veiculosCadastrados = Entrada::where('ativo', '=', 1)->count();

    $veiculosFalha = Acesso::with('entrada')
    ->where('ativo', '=', 1)
    ->whereHas('entrada', function ($query) {
      $query->where('ativo', '=', 1);
    })->count();

    $veiculosManutAtiva = Acesso::with('entrada')
    ->where('ativo', '=', 1)
    ->whereHas('entrada', function ($query) {
      $query->where('ativo', '=', 1);
    })->count();

    $veiculosManConcluida = Acesso::with('entrada')
    ->where('ativo', '=', 1)
    ->whereHas('entrada', function ($query) {
      $query->where('ativo', '=', 1);
    })->count();
    // dd($veiculosManConcluida);

    $data = Entrada::with('user')
    ->where('ativo', '=',1)
    ->orderBy('id','=','desc')
    ->get();

    $dataAcesso = Acesso::with('entrada')
    ->where('ativo', '1')
    ->whereHas('entrada', function ($query) {
      $query->where('ativo', '=', 1);
    })->get();


    return view('painel.index', compact('date','data','dataAcesso','title','veiculosManutAtiva','veiculosManConcluida','veiculosCadastrados','abertoAtrasado','fechadoAtrasado','veiculosFalha'), array('user' => Auth::user()));
  }

  public function indexVeiculos()
  {
    $title = $this->title;
    $date = Carbon::now('America/Sao_Paulo')->format('h:i a');
    $tiltle = "Cadastrar ";
    // dd($data->nome_pesq);
    $dataAtual = date('Y-m-d');
    $horaAtual = date('Y-m-d H:i:s');



    $concedido_acesso  = Manutencao::with('entrada')->sum('date_concedido_acesso');
    $concluido_acesso  = Manutencao::with('entrada')->sum('date_concluido_acesso');




    $totalP = DB::select("SELECT SEC_TO_TIME( SUM( TIME_TO_SEC( DATE_FORMAT(date_concluido_acesso, '%h:%i:%s') ) ) ) AS total_horas FROM manutencaos");
    $totalP2 = DB::select("SELECT SEC_TO_TIME( SUM( TIME_TO_SEC( DATE_FORMAT(date_concedido_acesso, '%h:%i:%s') ) ) ) AS total_horas FROM manutencaos");

    // dd($totalP,$totalP2);

    $local_qt = DB::select("SELECT COUNT(local) AS local_count, local
    FROM manutencaos
    GROUP BY local");

    $falha_qt = DB::select("SELECT COUNT(falha) AS falha_count, falha
    FROM manutencaos
    GROUP BY falha");

    $local_qt_elo = DB::table('manutencaos')
    ->select(DB::raw('count(*) as local_count, local'))
    ->groupBy('local')
    ->get();


    $count_falha = DB::table('manutencaos')
    ->select(DB::raw('count(*) as falha_count, falha'))
    ->orderBy('falha_count','desc')
    ->groupBy('falha')
    ->get()->toArray();

    if ($count_falha == null) {
      $falhaMaisFrequente = 0;
      $falhaMaisFrequente_count = 0;
    }else {
      $falhaMaisFrequente = $count_falha[0]->falha;
      $falhaMaisFrequente_count = $count_falha[0]->falha_count;
    }


    // dd($falhaMaisFrequente_count);


    // dd($falhaMaisFrequente,$collection_falha);

    $falha_all_elo = DB::table('manutencaos')
    ->select(DB::raw('count(*) as falha_count, falha'))
    ->groupBy('falha')
    ->orderBy('falha_count','desc')
    ->get();

    // dd($local_qt , $falha_qt, $local_qt_elo,$falhaMaisFrequente);

    $veiculosCadastrados = EntradaVeiculo::where('ativo', 1)->count();

    $empCadastrados = EntradaVeiculo::where('ativo', 1)->where('chassis', null)->count();
    $camCadastrados = EntradaVeiculo::where('ativo', 1)->where('chassis', 'Caminhão')->count();

    // dd($veiculosCadastrados);

    $veiculosFalha = Manutencao::with('entrada')
    ->whereHas('entrada', function ($query) {
      $query->where('ativo', '=', 1);
    })->count();


    $veiculosManutAtiva = Manutencao::with('entrada')
    ->where('ativo', '=', 1)
    ->whereHas('entrada', function ($query) {
      $query->where('ativo', '=', 1);
    })->count();

    $veiculosManConcluida = Manutencao::with('entrada')
    ->where('ativo', '=', 0)
    ->whereHas('entrada', function ($query) {
      $query->where('ativo', '=', 1);
    })->count();
    // dd($veiculosManConcluida);

    $data = EntradaVeiculo::with('user')
    ->where('ativo', '=',1)
    ->orderBy('id','=','desc')
    ->get();

    $dataAcesso = Manutencao::with('entrada')
    ->where('ativo', '1')
    ->whereHas('entrada', function ($query) {
      $query->where('ativo', '=', 1);
    })->get();



    // $acc = Manutencao::withCount('peca_manutencaos')->get();
    // $cont = $posts->peca_manutencaos()->count();


    // dd($posts);



    //
    // $user = EntradaVeiculo::get();
    // $user->load(['acessos.peca_manutencaos' => function ($q) use ( &$peca_manutencaos ) {
    //   $peca_manutencaos = $q->get()->unique();
    // }]);
    // foreach ($peca_manutencaos as $p) {
    //   # code...
    //   $p->peca_manutencao;
    // }
    // dd($p->peca_manutencao);

    // dd($dataAcesso);

    $maisManutencao = Manutencao::with('local_manutencaos')->count();
    // dd($maisManutencao);

    $veiculosFalhaCount = DB::table('manutencaos')
    ->join('entrada_veiculos', 'entrada_veiculos.id', '=', 'manutencaos.entrada_veiculo_id')
    ->select(DB::raw('count(manutencaos.entrada_veiculo_id) as quantFalhas,  entrada_veiculos.plate as placa'))
    ->groupBy( 'manutencaos.entrada_veiculo_id','entrada_veiculos.id', 'entrada_veiculos.plate')
    ->orderBy('quantFalhas', 'DESC')
    ->take(2)
    ->get();

    // dd($veiculosFalhaCount);

    return view('painel.indexVeiculos', compact('veiculosFalhaCount','horasParadas','empCadastrados','camCadastrados','parado_total','local_qt_elo','falhaMaisFrequente','local_qt_elo_count','falhaMaisFrequente_count','totalParado','acc','date','data','dataAcesso','title','veiculosManutAtiva','veiculosManConcluida','veiculosCadastrados','abertoAtrasado','fechadoAtrasado','veiculosFalha'), array('user' => Auth::user()));
  }

  public function chart()
  {
    return view('painel.charts.chart', compact('parado_total','local_qt_elo','falhaMaisFrequente','local_qt_elo_count','falhaMaisFrequente_count','totalParado','acc','date','data','dataAcesso','title','veiculosManutAtiva','veiculosManConcluida','veiculosCadastrados','abertoAtrasado','fechadoAtrasado','veiculosFalha'), array('user' => Auth::user()));
  }

}
