<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Stock;
use App\Models\Entrada;
use App\Models\EntradaVeiculo;
use App\Models\Acesso;
use App\Models\Manutencao;
use App\Models\AcessoVeiculo;
use App\Models\PecaManutencao;
use Carbon\Carbon;
use App\User;
use Session;
use Auth;
use DB;

class StockController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */


    public $title  = 'Gráfico';
    public $route = 'stock';
    protected $totalPage = 12;
    public $view = 'painel.entradas';
    protected $input;
    protected $input2;

    public function __construct(Stock $entrada)
    {
        $this->middleware('auth');
        $this->model = $entrada;

    }

    public function index()
    {
        $title = "Lista de ".$this->title."s";

        $resultado = \DB::table('manutencaos')
        ->select(DB::raw('count(*) as quantFalha, falha'))
        ->groupBy('falha')
        ->orderBy('quantFalha', 'DESC')
        ->get();
        $from = null;
        $to = null;

        // dd($resultado);

        return view('painel.charts.chart',compact('from' , 'to','resultado','title','data'));
    }

    public function chartRangem(Request $request)
    {
        $title = "Lista de ".$this->title."s";


        // dd($request);

        $dataForm = $request->all();
        $dt = Carbon::now();



        $dataForm['start'] = Carbon::createFromFormat('d/m/Y',$dataForm['start'])->startOfDay();
        $dataForm['end'] = Carbon::createFromFormat('d/m/Y',$dataForm['end'])->startOfDay();
        $startDate = $dataForm['start'];
        $endDate = $dataForm['end'];

        $dates = Manutencao::pluck('created_at');



        $resultado = \DB::table('manutencaos')
        ->whereBetween('created_at', [$dataForm['start'],$dataForm['end']])
        ->select(DB::raw('count(*) as quantFalha, falha'))
        ->groupBy('falha')
        ->orderBy('quantFalha', 'DESC')
        ->get();


        $jsonR = json_encode($resultado);

        return view('painel.charts.chartRange',compact('resultado','jsonR','title','data'));
    }

    public function chartRangem2(Request $request)
    {
        $title = "Lista de ".$this->title."s";



        $dataForm = $request->all();
        $dt = Carbon::now();



        $dataForm['start'] = Carbon::createFromFormat('d/m/Y',$dataForm['start'])->startOfDay();
        $dataForm['end'] = Carbon::createFromFormat('d/m/Y',$dataForm['end'])->startOfDay();
        $startDate = $dataForm['start'];
        $endDate = $dataForm['end'];

        $dates = Manutencao::pluck('created_at');


        $resultado = DB::table('manutencaos')
        ->join('entrada_veiculos', 'entrada_veiculos.id', '=', 'manutencaos.entrada_veiculo_id')
        ->whereBetween('manutencaos.created_at', [$dataForm['start'],$dataForm['end']])
        ->select(DB::raw('count(manutencaos.entrada_veiculo_id) as quantFalhas,  entrada_veiculos.plate as placa'))
        ->groupBy( 'manutencaos.entrada_veiculo_id','entrada_veiculos.id', 'entrada_veiculos.plate')
        ->orderBy('quantFalhas', 'DESC')
        ->take(10)
        ->get();


        $jsonR = json_encode($resultado);

        // dd($jsonR);



        return view('painel.charts.chartRange2',compact('resultado','jsonR','title','data'));
    }




    public function index2()
    {
        $title = "Lista de ".$this->title."s";
        // $data = $this->model->with('user')
        // ->where('ativo', '=',1)
        // ->orderBy('id','=','desc')
        // ->get();

        $resultado = DB::table('manutencaos')
        ->join('entrada_veiculos', 'entrada_veiculos.id', '=', 'manutencaos.entrada_veiculo_id')
        ->select(DB::raw('count(manutencaos.entrada_veiculo_id) as quantFalhas,  entrada_veiculos.plate as placa'))
        ->groupBy( 'manutencaos.entrada_veiculo_id','entrada_veiculos.id', 'entrada_veiculos.plate')
        ->orderBy('quantFalhas', 'DESC')
        ->take(10)
        ->get();

        // dd($resultado);


        return view('painel.charts.chart2',compact('resultado','title','data'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('Stock');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $stock = new Stock([
            'stockName' => $request->get('stockName'),
            'stockPrice' => $request->get('stockPrice'),
            'stockYear' => $request->get('stockYear'),
        ]);
        $stock->save();

        return redirect('stocks');
    }

    /**
    * Fetch the particular company details
    * @return json response
    */
    public function chart()
    {

        $result = \DB::table('manutencaos')
        ->select(DB::raw('count(*) as quantFalha, falha'))
        ->groupBy('falha')
        ->orderBy('quantFalha', 'DESC')
        ->take(10)
        ->get();


        // dd($totalRevenue);
        return response()->json($result);
        // return response()->json(array(
        //     'result' => $result,
        //     'result2' => $result2,
        // ));
    }

    public function jsonRange(Request $request)
    {

        $title = "Lista de ".$this->title."s";
        //
        // $test = $this->chartRangem($request);
        //
        // dd($test);
        // $dataForm = $request->all();
        // dd($dataForm);
        $dt = Carbon::now();

        $input  = $request->input('start');
        $input2  = $request->input('end');

        $input  = '28/01/2018';
        $input2 = '02/02/2018';



        $format = 'd/m/Y';


        $dataForm['start'] = Carbon::createFromFormat('d/m/Y',$input)->startOfDay();
        $dataForm['end'] = Carbon::createFromFormat('d/m/Y',$input2)->startOfDay();
        $startDate = $dataForm['start'];
        $endDate = $dataForm['end'];

        $dates = Manutencao::pluck('created_at');


        //
        // dd($dates);
        // dd($startDate,$endDate);

        $resultado = \DB::table('manutencaos')
        ->whereBetween('created_at', [$dataForm['start'],$dataForm['end']])
        ->select(DB::raw('count(*) as quantFalha, falha'))
        ->groupBy('falha')
        ->orderBy('quantFalha', 'DESC')
        ->get();

        //
        // $result = \DB::table('manutencaos')
        // ->select(DB::raw('count(*) as quantFalha, falha'))
        // ->groupBy('falha')
        // ->orderBy('quantFalha', 'DESC')
        // ->take(5)
        // ->get();


        // dd($totalRevenue);
        return response()->json($resultado);
        // return view('painel.charts.chartRange',compact('resultado','title','data'));

    }


    public function chart2()
    {


        $result2 = DB::table('manutencaos')
        ->join('entrada_veiculos', 'entrada_veiculos.id', '=', 'manutencaos.entrada_veiculo_id')
        ->select(DB::raw('count(manutencaos.entrada_veiculo_id) as quantFalhas,  entrada_veiculos.plate as placa'))
        ->groupBy( 'manutencaos.entrada_veiculo_id','entrada_veiculos.id', 'entrada_veiculos.plate')
        ->orderBy('quantFalhas', 'DESC')
        ->take(10)
        ->get();

        // dd($totalRevenue);
        return response()->json($result2);
        // return response()->json(array(
        //     'result' => $result,
        //     'result2' => $result2,
        // ));
    }


}
