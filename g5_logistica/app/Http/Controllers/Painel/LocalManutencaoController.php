<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EntradaVeiculo;
use App\Models\AcessoVeiculo;
use App\Models\LocalManutencao;

class LocalManutencaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public $title  = 'Local Manutenção';
     public $route = 'local-manutencao';
     protected $totalPage = 12;
     public $view = 'painel.entradas';

     public function __construct(LocalManutencao $local)
     {
       $this->middleware('auth');
       $this->model = $local;

     }
    public function index()
    {
      $data = $this->model->with('acesso_veiculos')->get();
      dd($data);
      return view("{$this->view}.index",compact('title','users','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'local_manutencao' => 'required|unique:local_manutencaos',
      ], [
        'local_manutencao.required' => 'Favor, Inserir o Local da Manutenção!',
        'local_manutencao.unique' => 'Ops, Temos essa Local Cadastrado, basta selecionar!',
      ]);

      $dataUser = new $this->model;
      $dataUser = $request->all();
    //  dd($dataUser);
      $insert = $this->model->create($dataUser);

      return back()->withInput($request->input());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
