<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests\Painel\AcessoFormRequest;
use App\Http\Controllers\Controller;
use App\Models\Entrada;
use App\Models\Acesso;
use Carbon\Carbon;
use App\User;
use Session;
use Auth;
use DB;

class AcessoController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public $title  = 'Acesso';
  public $route = 'acessos';
  protected $totalPage = 12;
  public $view = 'painel.acessos';

  public function __construct(Acesso $acesso)
  {
    $this->middleware('auth');
    $this->model = $acesso;

  }


  public function index()
  {
    $title =  $this->title;
    $dataAcesso = $this->model->with('entrada')
    ->where('ativo', '1')
    ->whereHas('entrada', function ($query) {
      $query->where('ativo', '=', 1);
    })->get();
    return view("{$this->view}.index",compact('title','users','dataAcesso'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function criar($id)
  {
    // dd($id);
    $tiltle = "Acesso ";
    // dd($data->nome_pesq);
    $dataAtual = Carbon::now();


    $dataAcesso = Acesso::with('entrada')
    ->where('entrada_id', $id)
      ->whereHas('entrada', function ($query) use ($id) {
      $dataAtual = Carbon::now();
      $query->where('entrada_id', $id);
    })->first();


    // dd($data);


    $acessosAtivos = Acesso::with('entrada')
    ->where('ativo', 1)
    ->whereHas('entrada', function ($query) use ($id) {
      $dataAtual = Carbon::now();
      $query->where('id', $id);
    })->count();

    $acessosFechados = Acesso::with('entrada')
    ->where('ativo', 0)
    ->whereHas('entrada', function ($query) use ($id) {
      $dataAtual = Carbon::now();
      $query->where('id', $id);
    })->count();

    // dd($acessosAtivos,$acessosFechados);
    // $prest = Entrada::with('acessos')->findOrFail($id);

    $ultimoAcesso = Acesso::with('entrada')
    ->where('date_concedido_acesso', '<' , $dataAtual)
    ->whereHas('entrada', function ($query) use ($id) {
      $dataAtual = Carbon::now();
      $query->where('date_concedido_acesso', '<' , $dataAtual)
      ->where('id', $id);
    })
    ->where('ativo', 0)
    ->orderBy('id', 'desc')
    ->pluck('date_concluido_acesso')
    ->first();

    // dd($ultimoAcesso->diffInDays($dataAtual));

    $user2 = Entrada::with('acessos')->findOrFail($id);
    $user = Entrada::with('acessos')->findOrFail($id);

    $editar = null;
    return view("{$this->view}.create",compact('tiltle','dataAcesso','editar','user','user2','acessosAtivos','acessosFechados','ultimoAcesso'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(AcessoFormRequest $request,$id)
  {
    $dataUser = new $this->model;
    $dt = Carbon::now();
    $dataUser = $request->all();
    // $dataUser['tempo_estimado'] = \Carbon\Carbon::now()->format('Y-m-d');
    $dataUser['concedido_acesso_por'] = Auth::user()->name;
    $dataUser['date_concedido_acesso'] = \Carbon\Carbon::now()->format('Y-m-d H:i:s');

    if ($dataUser['tempo_estimado'] == 10) {
      $dataUser['tempo_estimado'] = $dt->addMinutes(10);
    }elseif ($dataUser['tempo_estimado'] == 30) {
      $dataUser['tempo_estimado'] = $dt->addMinutes(30);
    }elseif ($dataUser['tempo_estimado'] == 60) {
      $dataUser['tempo_estimado'] = $dt->addMinutes(60);
    }elseif ($dataUser['tempo_estimado'] == '5h') {
      $dataUser['tempo_estimado'] = $dt->addMinutes(300);
    }else {
      $dataUser['tempo_estimado'] = '';
    }
    // dd($dataUser);
    // if ($dataUser['nascimento'] == null) {
    //   $dataUser['nascimento'] == null;
    // }else {
    //   $dataUser['nascimento'] = str_replace('/', '-', $dataUser['nascimento']);
    //   $dataUser['nascimento'] = date('Y-m-d', strtotime($dataUser['nascimento']));
    // }
    $dataUser['entrada_id'] = $request['id'];

    $insert = $this->model->create($dataUser);

    if ($insert) {


      return redirect()
      ->route('/painel')
      ->with(['status' => 'Entrada cadastrada com sucesso!']);
    } else {
      return redirect()
      ->route('acessos.create', ['id' => $id])
      ->withErrors(['errors' => 'Falha ao cadastrar Entrada!, veja a conexão de Internet!'])
      ->withInput();
    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $dataUser = $this->model->findOrFail($id);
    $dataUser['ativo']= 0;
    $dataUser['concluido_acesso_por']= Auth::user()->name;
    $dataUser['date_concluido_acesso'] = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
    // dd($dataUser);
    $dataUser->save();
    // Session::flash('flash_message','Adiantamento excluído com sucesso');
    return redirect()->back()->with(['delete' => 'Acesso concluído com sucesso!']);;
  }
}
