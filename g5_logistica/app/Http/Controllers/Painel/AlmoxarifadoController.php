<?php

namespace App\Http\Controllers\Painel;

use App\Http\Requests\Painel\AlmoxarifadoRequest;
use App\Models\Almoxarifado;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Validator;
class AlmoxarifadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $almoxarifado = Almoxarifado::get();
        return view('painel.almoxarifado.almoxarifado', compact('almoxarifado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('painel.almoxarifado.form_almoxarifado')
            ->with('insertMessage', 'Item cadastrado com sucesso.');
    }


    public function store(AlmoxarifadoRequest $request)
    {
        $alm = new Almoxarifado();
        $alm->produto = $request ->produto;
        $alm ->frota_equipamento = $request-> frota_equipamento;
        $alm->_date = $request ->_date;
        $alm->nota_fiscal = (int) $request ->nota_fiscal;
        $alm->responsavel = $request ->responsavel;
        $alm->custo_unitario = (float)($this->moeda($request ->custo_unitario));
        $alm->quantidade = $request ->quantidade;
        $alm->fornecedor = $request ->fornecedor;
        $alm->data_de_entrega = $request ->data_de_entrega;
        $alm->estoque_estimado = $request ->estoque_estimado;
        $alm->totaL_de_compra = (float) $this->moeda($request ->total_de_compra);
        $alm->totaL_de_saida = (float) $this -> moeda($request ->total_de_saida);
        $alm->numero_os = $request->numero_os;
        //dd($alm);
        $alm->save();

        if($alm)
        {
            //$st->status = true;
            //$st->message = 'Item cadastrado com sucesso.';
            Session::flash('insertMessage', 'Item cadastrado com sucesso');
        }
        else
        {
            Session::flash('insertMessage', 'Erro ao cadastrar. Tente novamente');
        }


        $almoxarifado = Almoxarifado::get();


        return view('painel.almoxarifado.almoxarifado', compact('almoxarifado'));


    }


    public function show($id)
    {

    }

    public function edit($id)
    {
        $almoxarifado = Almoxarifado::findOrFail($id);
        return view('painel.almoxarifado.form_almoxarifado', ['almoxarifado'=>$almoxarifado]);
    }


    public function update(AlmoxarifadoRequest $request, $id)
    {

        $almoxarifado = Almoxarifado::findOrfail($id);
        $almoxarifado = new Almoxarifado();
        $almoxarifado ->produto = $request ->produto;
        $almoxarifado ->frota_equipamento = $request-> frota_equipamento;
        $almoxarifado ->_date = $request ->_date;
        $almoxarifado ->nota_fiscal = $request ->nota_fiscal;
        $almoxarifado ->responsavel = $request ->responsavel;
        $almoxarifado ->custo_unitario = $this->moeda($request ->custo_unitario);
        $almoxarifado ->quantidade = $request ->quantidade;
        $almoxarifado ->fornecedor = $request ->fornecedor;
        $almoxarifado ->data_de_entrega = $request ->data_de_entrega;
        $almoxarifado ->estoque_estimado = $request ->estoque_estimado;
        $almoxarifado ->totaL_de_compra = $this->moeda($request ->total_de_compra);
        $almoxarifado ->totaL_de_saida = $this->moeda($request ->total_de_saida);
        $almoxarifado ->numero_os = $request->numero_os;
        //dd($alm);
        $almoxarifado->update();

        //dd($almoxarifado);

        return redirect('almoxarifado')
            ->with('updateMessage', 'Item atualizado com sucesso.');
    }

    public function destroy($id)
    {
        $almoxarifado = Almoxarifado::findOrFail($id);
        $almoxarifado->delete();
        
        return redirect('almoxarifado/salvar')
            ->with('message', 'Item deletado com sucesso.');

    }

    private function moeda($get_valor) {
        $source = array('.', ',');
        $replace = array('', '.');
        $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
        return $valor; //retorna o valor formatado para gravar no banco
    }


}
