<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests\Painel\EntradaFormRequest;
use App\Http\Controllers\Controller;
use App\Models\NotaEntrega;
use App\Models\Entrada;
use Carbon\Carbon;
use App\User;
use Session;
use Auth;

class EntradaController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public $title  = 'Visitant';
  public $route = 'entradas';
  protected $totalPage = 12;
  public $view = 'painel.entradas';

  public function __construct(Entrada $entrada)
  {
    $this->middleware('auth');
    $this->model = $entrada;

  }


  public function index()

  {
    $title = "Lista de ".$this->title."es";
    $data = $this->model->with('user')
    ->where('ativo', '=',1)
    ->orderBy('id','=','desc')
    ->get();

    return view("{$this->view}.index",compact('title','data'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */


  public function create()
  {

    $tiltle = "Cadastrar ";
    // dd($data->nome_pesq);
    $data = $this->model->paginate($this->totalPage);
    return view("{$this->view}.create-edit",compact('tiltle','data'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(EntradaFormRequest $request)
  {
    $dataUser = new $this->model;


    $dataUser = $request->all();
    $dataUser['user_id'] = Auth::user()->id;
    // if ($dataUser['nascimento'] == null) {
    //   $dataUser['nascimento'] == null;
    // }else {
    //   $dataUser['nascimento'] = str_replace('/', '-', $dataUser['nascimento']);
    //   $dataUser['nascimento'] = date('Y-m-d', strtotime($dataUser['nascimento']));
    // }


    if(!empty($_POST['imagem'])){
      $encoded_data = $_POST['imagem'];
      $binary_data = base64_decode( $encoded_data );

      // save to server (beware of permissions // set ke 775 atau 777)
      $namafoto = uniqid(date('YmdHis')).".png";
      $result = file_put_contents( 'assets/uploads/entradas/'.$namafoto, $binary_data );
      if (!$result) die("Could not save image!  Check file permissions.");

      $dataUser['image'] = $namafoto;
    }
    // dd($dataUser);




    //Verifica se existe a imagem
    if ($request->hasFile('image')) {
      //Pega a imagem
      $image = $request->file('image');

      //Define o nome para a imagem
      $nameImage = uniqid(date('YmdHis')).'.'.$image->getClientOriginalExtension();

      $upload = $image->storeAs('entradas', $nameImage);
      //dd($upload);

      if ($upload) {
        $dataUser['image'] = $nameImage;
      } else {

        return redirect('/painel/usuarios/create')
        ->withErrors(['errors' => 'Erro no Upload'])
        ->withInput();
      }
    }


    // dd($dataUser);
    $insert = $this->model->create($dataUser);

    if ($insert) {


      return redirect()
      ->route("/painel")
      ->with(['status' => 'Cadastro realizado com sucesso!']);
    } else {
      return redirect()
      ->route("/painel")
      ->withErrors(['errors' => 'Falha ao cadastrar!, veja a conexão de Internet!'])
      ->withInput();
    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //Recupera o usuário
    $user = $this->model->find($id);
    // dd($user->nome);

    $title = "Fornecedor: {$user->nome}";

    return view("{$this->view}.show", compact('user', 'title'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //Recupera o usuário pelo id
    $user = $this->model->findOrFail($id);

    $title = "Editar Usuário: {$user->name}";

    return view("{$this->view}.create-edit", compact('user', 'title'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //Pega todos os dados do usuário
    $dataUser = $request->all();
    // if ($dataUser['nascimento'] == null) {
    //   $dataUser['nascimento'] == null;
    // }else {
    //   $dataUser['nascimento'] = str_replace('/', '-', $dataUser['nascimento']);
    //   $dataUser['nascimento'] = date('Y-m-d', strtotime($dataUser['nascimento']));
    // }

    // dd($dataUser);

    //Cria o objeto de usuário
    $user = $this->model->find($id);


    if(!empty($_POST['imagem'])){
      $encoded_data = $_POST['imagem'];
      $binary_data = base64_decode( $encoded_data );

      // save to server (beware of permissions // set ke 775 atau 777)
      $namafoto = uniqid(date('YmdHis')).".png";
      $result = file_put_contents( 'assets/uploads/entradas/'.$namafoto, $binary_data );
      if (!$result) die("Could not save image!  Check file permissions.");

      $dataUser['image'] = $namafoto;
    }
    // dd($dataUser);



    //Verifica se existe a imagem
    if ($request->hasFile('image')) {
      //Pega a imagem
      $image = $request->file('image');

      //Verifica se o nome da imagem não existe
      if ($user->image == '') {
        $nameImage         = uniqid(date('YmdHis')).'.'.$image->getClientOriginalExtension();
        $dataUser['image'] = $nameImage;
      } else {
        $nameImage         = $user->image;
        $dataUser['image'] = $user->image;
      }

      $upload = $image->storeAs('users', $nameImage);

      if (!$upload) {
        return redirect()->route("{$this->route}.edit", ['id' => $id])
        ->withErrors(['errors'                         => 'Erro no Upload'])
        ->withInput();
      }
    }

    //Altera os dados do usuário
    $update = $user->update($dataUser);

    if ($update) {
      return redirect()
      ->route("{$this->route}.index")
      ->with(['success' => 'Alteração realizada com sucesso!']);
    } else {

      return redirect()->route("{$this->route}.create-edit", ['id' => $id])
      ->withErrors(['errors'                         => 'Falha ao editar'])
      ->withInput();
    }
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $dataUser = $this->model->findOrFail($id);

    $dataUser['ativo']= 0;
    // dd($dataUser);
    $dataUser->save();
    // Session::flash('flash_message','Adiantamento excluído com sucesso');

    return redirect()->route("{$this->route}.index")
    ->with(['success' => 'Excluído com sucesso!']);
  }

  public function search(Request $request) {
    //Recupera os dados do formulário
    $dataForm = $request->except('_token');
    $title = "Lista de ".$this->title."es";
    // $title    = $this->model;

    if (!$request->input('key-search')) {

      return redirect()->action(
        'Painel\EntradaController@index'
      );
    }

    //Filtra os usuários
    $data = $this->model
    ->with('user')
    ->where('nome', 'LIKE', "%{$dataForm['key-search']}%")
    ->orWhere('user_id', $dataForm['key-search'])
    ->paginate($this->totalPage);

    return view("{$this->view}.index", compact('data', 'dataForm', 'title'));
  }

}
