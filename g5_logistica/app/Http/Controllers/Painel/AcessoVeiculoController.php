<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests\Painel\AcessoVeiculoFormRequest;
use App\Http\Controllers\Controller;
use App\Models\EntradaVeiculo;
use App\Models\AcessoVeiculo;
use App\Models\PecaManutencao;
use App\Models\LocalManutencao;
use Carbon\Carbon;
use App\User;
use Session;
use Auth;
use DB;

class AcessoVeiculoController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public $title  = 'Veículos';
  public $route = 'veiculos';
  protected $totalPage = 12;
  public $view = 'painel.acessoVeiculos';

  public function __construct(AcessoVeiculo $acesso)
  {
    $this->middleware('auth');
    $this->model = $acesso;

  }


  public function index()
  {
    $title =  $this->title;
    $dataAcesso = $this->model->with('entrada')
    ->where('ativo', '1')
    ->whereHas('entrada', function ($query) {
      $query->where('ativo', '=', 1);
    })->get();
    return view("{$this->view}.index",compact('title','users','dataAcesso'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function criar($id)
  {
    //dd($id);
    $tiltle = "Veículo ";
    // dd($data->nome_pesq);
    $dataAtual = Carbon::now();


    $peca  = PecaManutencao::pluck('peca_manutencao');
    $local = LocalManutencao::pluck('local_manutencao');


    $dataAcesso = AcessoVeiculo::with('entrada')
    ->where('entrada_veiculo_id', $id)
    ->whereHas('entrada', function ($query) use ($id) {
      $dataAtual = Carbon::now();
      $query->where('entrada_veiculo_id', $id);
    })->first();


    // dd($data);


    $acessosAtivos = AcessoVeiculo::with('entrada')
    ->where('ativo', 1)
    ->whereHas('entrada', function ($query) use ($id) {
      $dataAtual = Carbon::now();
      $query->where('id', $id);
    })->count();

    $acessosFechados = AcessoVeiculo::with('entrada')
    ->where('ativo', 0)
    ->whereHas('entrada', function ($query) use ($id) {
      $dataAtual = Carbon::now();
      $query->where('id', $id);
    })->count();

    // dd($acessosAtivos,$acessosFechados);
    // $prest = Entrada::with('acessos')->findOrFail($id);

    $ultimoAcesso = AcessoVeiculo::with('entrada')
    ->where('date_concedido_acesso', '<' , $dataAtual)
    ->whereHas('entrada', function ($query) use ($id) {
      $dataAtual = Carbon::now();
      $query->where('date_concedido_acesso', '<' , $dataAtual)
      ->where('id', $id);
    })
    ->where('ativo', 0)
    ->orderBy('id', 'desc')
    ->pluck('date_concluido_acesso')
    ->first();

    // dd($ultimoAcesso->diffInDays($dataAtual));

    $user2 = EntradaVeiculo::with('acessos')->findOrFail($id);
    $user = EntradaVeiculo::with('acessos')->findOrFail($id);

    $editar = null;
    return view("{$this->view}.create",compact('tiltle','peca','local','dataAcesso','editar','user','user2','acessosAtivos','acessosFechados','ultimoAcesso'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request,$id)
  {

    $this->validate($request,[
      'plate'          =>'required',
      'city'           =>'required',
      'state'          =>'required',
      'color'          =>'required',
      'year'           =>'required',
      'model'          =>'required',
      'brand'          =>'required',

    ], [

      "plate.required" => "Preencha o campo Placa",
      "city.required"  => "Preencha o campo Cidade",
      "state.required" => "Preencha o campo UF",
      "color.required" => "Preencha o campo Cor",
      "year.required"  => "Preencha o campo Ano Veículo",
      "model.required" => "Preencha o campo Modelo",
      "brand.required" => "Preencha o campo Marca",
      // "dataAcesso['motoristas'].required"   => "Preencha o campo Motorista",
      // 'peso_referencia.required' => "Preencha o campo Peso Referência",
    ]);
    $dataUser = new $this->model;
    $dt = Carbon::now();
    $dataUser = $request->all();
    // dd($dataUser);
    $dataUser['falhas'] = $request['falhas'];
    $dataUser['peca_manutencao']  = $request['peca_manutencao']+1;
    $dataUser['local_manutencao'] = $request['local_manutencao']+1;
    // $dataUser['tempo_estimado'] = \Carbon\Carbon::now()->format('Y-m-d');
    $dataUser['concedido_acesso_por'] = Auth::user()->name;
    $dataUser['date_concedido_acesso'] = \Carbon\Carbon::now()->format('Y-m-d H:i:s');

    if ($dataUser['tempo_estimado'] == 1) {
      $dataUser['tempo_estimado'] = $dt->addDay();
    }elseif ($dataUser['tempo_estimado'] == 10) {
      $dataUser['tempo_estimado'] = $dt->addDays(10);
    }elseif ($dataUser['tempo_estimado'] == 30) {
      $dataUser['tempo_estimado'] = $dt->addDays(30);
    }elseif ($dataUser['tempo_estimado'] == 60) {
      $dataUser['tempo_estimado'] = $dt->addDays(60);
    }else {
      $dataUser['tempo_estimado'] = '';
    }
    // if ($dataUser['nascimento'] == null) {
    //   $dataUser['nascimento'] == null;
    // }else {
    //   $dataUser['nascimento'] = str_replace('/', '-', $dataUser['nascimento']);
    //   $dataUser['nascimento'] = date('Y-m-d', strtotime($dataUser['nascimento']));
    // }
    $dataUser['entrada_veiculo_id'] = $request['id'];
    $insert = $this->model->create($dataUser);

    $locmanutencao = $this->model->with('local_manutencaos')->find($id);
    $pecmanutencao = $this->model->with('peca_manutencaos')->find($id);

    // $locmanutencao = $dataUser['local_manutencao'];
    // $pecmanutencao = $dataUser['peca_manutencao'];
    // dd($dataUser,$locmanutencao,$pecmanutencao);
    $locmanutencao->local_manutencaos()->attach($dataUser['local_manutencao']);
    $pecmanutencao->peca_manutencaos()->attach($dataUser['peca_manutencao']);

    // dd($insert);
    // dd($dataUser);



    if ($insert) {


      return redirect()
      ->route('/painelVeiculos')
      ->with(['status' => 'Entrada cadastrada com sucesso!']);
    } else {
      return redirect()
      ->route('acesso.veiculos.create', ['id' => $id])
      ->withErrors(['errors' => 'Falha ao cadastrar!, veja a conexão de Internet!'])
      ->withInput();
    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy(Request $request, $id)
  {
    $dataUser = $request->all();
    $dataUser = $this->model->findOrFail($id);
    $dataUser['comentarios'] = $request->input('comentarios');
    $dataUser['ativo']= 0;
    $dataUser['concluido_acesso_por']= Auth::user()->name;
    $dataUser['date_concluido_acesso'] = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
    // dd($dataUser);

    $dataUser->save();
    // Session::flash('flash_message','Adiantamento excluído com sucesso');
    return redirect()->back()->with(['delete' => 'Manutenção concluída com sucesso!']);;
  }
}
