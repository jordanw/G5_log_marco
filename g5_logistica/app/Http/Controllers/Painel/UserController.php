<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Painel\UserFormRequest;
use App\Mail\WelcomeTask;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller {
	private $user;
	protected $totalPage = 12;

	public function __construct(User $user) {
		$this->user = $user;

		// $this->middleware('can:users')
		//         ->except(['showProfile', 'updateProfile']);
	}

	/**
	* Display a listing of the resource.
	*
	* @return \Illuminate\Http\Response
	*/
	public function index() {
		$title = 'Lista de Usuários';

		$users = $this->user
		->where('nivel', 2)
		->orWhere('nivel', 3)
		->get();
		// dd($users);

		return view('painel.users.index', compact('users','title'));
	}

	/**
	* Show the form for creating a new resource.
	*
	* @return \Illuminate\Http\Response
	*/
	public function create() {
		$title = 'Cadastrar Novo Usuário';

		return view('painel.users.create-edit', compact('title'));
	}

	/**
	* Store a newly created resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @return \Illuminate\Http\Response
	*/
	public function store(UserFormRequest $request) {
		//Pega todos os dados do usuário
		$dataUser             = $request->all();
		$img = $dataUser['imagem'];
		// $dataUser['nivel']    = 2;
		$dataUser['datetime'] = date('Y-m-d H:i:s');
		if ($dataUser['nascimento'] == null) {
			$dataUser['nascimento'] == null;
		}else {
			$dataUser['nascimento'] = str_replace('/', '-', $dataUser['nascimento']);
			$dataUser['nascimento'] = date('Y-m-d', strtotime($dataUser['nascimento']));
		}

		// dd($dataUser);
		//Criptografa a senha
		$dataUser['senha'] = bin2hex(openssl_random_pseudo_bytes(3));
		$senha = $dataUser['senha'];
		$dataUser['password'] = bcrypt($senha);
		//dd($senha, $dataUser['password'] );

		if(!empty($_POST['imagem'])){
			$encoded_data = $_POST['imagem'];
			$binary_data = base64_decode( $encoded_data );

			// save to server (beware of permissions // set ke 775 atau 777)
			$namafoto = uniqid(date('YmdHis')).".png";
			$result = file_put_contents( 'assets/uploads/users/'.$namafoto, $binary_data );
			if (!$result) die("Could not save image!  Check file permissions.");

			$dataUser['image'] = $namafoto;
		}
		// dd($dataUser);




		//Verifica se existe a imagem
		if ($request->hasFile('image')) {
			//Pega a imagem
			$image = $request->file('image');

			//Define o nome para a imagem
			$nameImage = uniqid(date('YmdHis')).'.'.$image->getClientOriginalExtension();

			$upload = $image->storeAs('users', $nameImage);
			//dd($upload);

			if ($upload) {
				$dataUser['image'] = $nameImage;
			} else {

				return redirect('/painel/usuarios/create')
				->withErrors(['errors' => 'Erro no Upload'])
				->withInput();
			}
		}

		// dd($dataUser);
		//Insere os dados do usuário
		$insert = $this->user->create($dataUser);
		  // dd($insert);

		if ($insert) {
			if ($insert->send_mail == 1) {
				\Mail::to($insert)
				->bcc("soucodigo@gmail.com")
   			        ->bcc("acesso@acesser.com.br")
				->send(new WelcomeTask($insert));
			}
			return redirect()
			->route('usuarios.index')
			->with(['success' => 'Cadastro realizado com sucesso!']);
		} else {
			return redirect()->route('usuarios.create')
			->withErrors(['errors' => 'Falha ao cadastrar!'])
			->withInput();
		}
	}

	/**
	* Display the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function show($id) {
		//Recupera o usuário
		$user = $this->user->find($id);

		$title = "Usuário: {$user->name}";

		return view('painel.users.show', compact('user', 'title'));
	}

	/**
	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function edit($id) {
		//Recupera o usuário pelo id
		$user = $this->user->find($id);

		$title = "Editar Usuário: {$user->name}";

		return view('painel.users.create-edit', compact('user', 'title'));
	}

	/**
	* Update the specified resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function update(UserFormRequest $request, $id) {
		//Pega todos os dados do usuário
		$dataUser = $request->all();

		//Cria o objeto de usuário
		$user = $this->user->find($id);
		if ($dataUser['nascimento'] == null) {
			$dataUser['nascimento'] == null;
		}else {
			$dataUser['nascimento'] = str_replace('/', '-', $dataUser['nascimento']);
			$dataUser['nascimento'] = date('Y-m-d', strtotime($dataUser['nascimento']));
		}

		// dd($dataUser);

		//Verifica se existe a imagem
		if ($request->hasFile('image')) {
			//Pega a imagem
			$image = $request->file('image');

			//Verifica se o nome da imagem não existe
			if ($user->image == '') {
				$nameImage         = uniqid(date('YmdHis')).'.'.$image->getClientOriginalExtension();
				$dataUser['image'] = $nameImage;
			} else {
				$nameImage         = $user->image;
				$dataUser['image'] = $user->image;
			}

			$upload = $image->storeAs('users', $nameImage);

			if (!$upload) {
				return redirect()->route('usuarios.edit', ['id' => $id])
				->withErrors(['errors'                         => 'Erro no Upload'])
				->withInput();
			}
		}


		//Verifica se existe a imagem WebCam

		if(!empty($_POST['imagem'])){
			$encoded_data = $_POST['imagem'];
			$binary_data = base64_decode( $encoded_data );

			// save to server (beware of permissions // set ke 775 atau 777)
			$namafoto = uniqid(date('YmdHis')).".png";
			$result = file_put_contents( 'assets/uploads/users/'.$namafoto, $binary_data );
			if (!$result) die("Could not save image!  Check file permissions.");

			$dataUser['image'] = $namafoto;
		}

		//Altera os dados do usuário
		$update = $user->update($dataUser);

		if ($update) {
			return redirect()
			->route('usuarios.index')
			->with(['success' => 'Alteração realizada com sucesso!']);
		} else {

			return redirect()->route('usuarios.edit', ['id' => $id])
			->withErrors(['errors'                         => 'Falha ao editar'])
			->withInput();
		}
	}

	/**
	* Remove the specified resource from storage.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function destroy($id) {
		//Recupera o usuário
		$user = $this->user->find($id);

		//deleta
		$delete = $user->delete();

		if ($delete) {
			return redirect()->route('usuarios.index');
		} else {
			return redirect()->route('usuarios.show', ['id' => $id])
			->withErrors(['errors'                         => 'Falha ao deletar']);
		}
	}

	public function search(Request $request) {
		//Recupera os dados do formulário
		$dataForm = $request->except('_token');

		//Filtra os usuários
		$users = $this->user
		->where('name', 'LIKE', "%{$dataForm['key-search']}%")
		->orWhere('email', $dataForm['key-search'])
		->paginate($this->totalPage);

		return view('painel.users.index', compact('users', 'dataForm'));
	}

	/**
	* Show the form for editing the specified resource.
	*
	* @return \Illuminate\Http\Response
	*/
	public function showProfile() {
		//Recupera o usuário
		$user = auth()->user();

		$title = 'Meu Perfil';

		return view('painel.users.profile', compact('user', 'title'));
	}

	/**
	* Update the specified resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function updateProfile(UserFormRequest $request, $id) {
		$this->authorize('update_profile', $id);

		//Pega todos os dados do usuário
		$dataUser = $request->all();

		//Cria o objeto de usuário
		$user = $this->user->find($id);

		//Criptografa a senha
		$dataUser['password'] = bcrypt($dataUser['password']);

		//Remove o e-mail do usuário para não atualizar
		unset($dataUser['email']);

		//Verifica se existe a imagem
		if ($request->hasFile('image')) {
			//Pega a imagem
			$image = $request->file('image');

			//Verifica se o nome da imagem não existe
			if ($user->image == '') {
				$nameImage         = uniqid(date('YmdHis')).'.'.$image->getClientOriginalExtension();
				$dataUser['image'] = $nameImage;
			} else {
				$nameImage         = $user->image;
				$dataUser['image'] = $user->image;
			}

			$upload = $image->storeAs('users', $nameImage);

			if (!$upload) {
				return redirect()->route('profile')
				->withErrors(['errors' => 'Erro no Upload'])
				->withInput();
			}
		}

		//Altera os dados do usuário
		$update = $user->update($dataUser);

		if ($update) {
			return redirect()
			->route('profile')
			->with(['success' => 'Perfil atualizado com sucesso']);
		} else {

			return redirect()->route('profile')
			->withErrors(['errors' => 'Falha ao atualizar o perfil.'])
			->withInput();
		}
	}

	//   public function send()
	// {
	//     $users = User::all();
	//       // dd($users);
	//     foreach ($users as $user) {
	//       \Mail::to($user->email)->send(new WelcomeTask($user->name));
	//     }

	// }
}
