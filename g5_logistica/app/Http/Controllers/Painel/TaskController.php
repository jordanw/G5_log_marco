<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Entrada;
use App\Models\Task;
use Carbon\Carbon;
use App\User;
use Session;
use Auth;
use DB;

class TaskController extends Controller
{
  public $title  = 'Tarefa';
  protected $totalPage = 12;
  protected $view  = 'painel.task';
  protected $route = 'tarefas';


  public function __construct(Task $task)
  {
    $this->model =  $task;
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $title =  $this->title;
      $data = $this->model->with('entrada')
      ->where('ativo', '1')
      ->whereHas('entrada', function ($query) {
        $query->where('ativo', '=', 1);
      })->get();
      return view("{$this->view}.index",compact('title','users','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $tiltle = "Cadastrar ";
      // dd($data->nome_pesq);
      $data = $this->model->paginate($this->totalPage);
      $usuarios = User::all();
      // dd($usuarios);
      return view("{$this->view}.create-edit",compact('tiltle','data','usuarios'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
