<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\NotaEntrega;
use App\Models\Entrada;
use Carbon\Carbon;
use App\User;
use Session;
use Auth;
use DB;

class NotaEntregaController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public $title  = 'Nota de Entrega';
  protected $totalPage = 12;
  protected $view  = 'painel.nota-entrega';
  protected $route = 'nota-entrega';

  public function __construct(NotaEntrega $notaEntrega)
  {
    $this->model =  $notaEntrega;
  }

  public function index()
  {
    $title =  $this->title;
    $data = $this->model->with('entrada')
    ->where('ativo', '1')
    ->whereHas('entrada', function ($query) {
      $query->where('ativo', '=', 1);
    })->get();
    return view("{$this->view}.index",compact('title','users','data'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */


  public function criar($id)
  {
    $tiltle = "Cadastrar ";
    // dd($data->nome_pesq);
    $dataAtual = date('Y-m-d');
    $user2 = Entrada::with('nota_entregas')->findOrFail($id);
    $user = Entrada::with('nota_entregas')->findOrFail($id);
    $prest = Entrada::with('nota_entregas')->findOrFail($id);
    $prestacao = $prest->nota_entregas
    ->where('date_prestado', '<' , $dataAtual)
    ->sortByDesc('date_prestado')
    ->pluck('date_prestado')->first();
    // dd($prestacao);
    return view("{$this->view}.create",compact('tiltle','user','user2','prestacao'));
  }

  // /**
  // * Store a newly created resource in storage.
  // *
  // * @param  \Illuminate\Http\Request  $request
  // * @return \Illuminate\Http\Response
  // */
  // public function store(Request $request)
  // {
  //   //
  // }
  public function gravar(Request $request, $id)
  {
    $dataUser = new $this->model;

    $dataUser = $request->all();
    $dataUser['date'] = \Carbon\Carbon::now()->format('Y-m-d');
    $dataUser['entrada_id'] = $request['id'];
    $dataUser['user_cadastrou'] = Auth::user()->name;
    $dataUser['valor_pvp'] = str_replace(',','.',str_replace('.','',$request->input('valor_pvp')));
    $dataUser['valor_unit'] =   str_replace(',','.',str_replace('.','',$request->input('valor_pvp'))) - ((str_replace(',','.',str_replace('.','',$request->input('valor_pvp'))) * $request['percent']) /100);
    $dataUser['valor_percent'] = (str_replace(',','.',str_replace('.','',$request->input('valor_pvp'))) * $request['percent']) /100;
    $dataUser['valor_total'] = str_replace(',','.',str_replace('.','',$request->input('valor_pvp'))) * $request->qtdade;


    $insert = $this->model->create($dataUser);

    if ($insert) {


      return redirect()
      ->route('criar.nota', ['id' => $id])
      ->with(['status' => 'Entrada cadastrada com sucesso!']);
    } else {
      return redirect()
      ->route('criar.nota', ['id' => $id])
      ->withErrors(['errors' => 'Falha ao cadastrar Entrada!, veja a conexão de Internet!'])
      ->withInput();
    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {

    //Recupera o usuário
    $user = $this->model->find($id);
    // dd($user->nome);

    $title = "Fornecedor: {$user->nome}";

    return view("{$this->view}.show", compact('user', 'title'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function editar($id)
  {

    // return "oi";

    $tiltle = "Editar ";
    // dd($data->nome_pesq);
    $user = $this->model->with('entrada')->findOrFail($id);
    // dd($user->nota_entregas->titulo);
    $data = 1;


    $dataAtual = date('Y-m-d');
    $user2 = Entrada::with('nota_entregas')->findOrFail($id);

    $prest = Entrada::with('nota_entregas')->findOrFail($id);
    $prestacao = $prest->nota_entregas
    ->where('date_prestado', '<' , $dataAtual)
    ->sortByDesc('date_prestado')
    ->pluck('date_prestado')->first();






    return view("{$this->view}.edit",compact('tiltle','user','data','user2','prestacao'));

  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function atualizar(Request $request, $id)
  {

    //Pega todos os dados do usuário
    $dataUser = $request->all();
    $dataUser['user_cadastrou'] = Auth::user()->name;
    $dataUser['valor_pvp'] = str_replace(',','.',str_replace('.','',$request->input('valor_pvp')));
    $dataUser['valor_unit'] =   str_replace(',','.',str_replace('.','',$request->input('valor_pvp'))) - ((str_replace(',','.',str_replace('.','',$request->input('valor_pvp'))) * $request['percent']) /100);
    $dataUser['valor_percent'] = (str_replace(',','.',str_replace('.','',$request->input('valor_pvp'))) * $request['percent']) /100;
    $dataUser['valor_total'] = str_replace(',','.',str_replace('.','',$request->input('valor_pvp'))) * $request->qtdade;

    // dd($dataUser);

    //Cria o objeto de usuário
    $user = $this->model->with('entrada')->findOrFail($id);



    //Altera os dados do usuário
    $update = $user->update($dataUser);

    if ($update) {
      return redirect()->action(
        'Painel\NotaEntregaController@criar', ['id' => $user->entrada->id]
      )
      ->with(['success' => 'Alteração realizada com sucesso!']);
    } else {

      return redirect()->action(
        'Painel\NotaEntregaController@criar', ['id' =>  $user->entrada->id]
      )
      ->withErrors(['errors'                         => 'Falha ao editar'])
      ->withInput();
    }
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $dataUser = $this->model->findOrFail($id);

    $dataUser['ativo']=0;
    $dataUser['prestado_por']= Auth::user()->name;
    $dataUser['date_prestado'] = \Carbon\Carbon::now()->format('Y-m-d H:i');
    // dd($dataUser);
    $dataUser->save();
    // Session::flash('flash_message','Adiantamento excluído com sucesso');

    return redirect()->back()->with(['delete' => 'Excluído com sucesso!']);;
  }


  public function restore($id)
  {
    $dataUser = $this->model->findOrFail($id);

    $dataUser['ativo']=1;
    $dataUser['prestado_por']= null;
    $dataUser['date_prestado'] = null;
    $dataUser['restaurado_por']= Auth::user()->name;
    $dataUser['date_restaurado'] =  \Carbon\Carbon::now()->format('Y-m-d H:i');
    // dd($dataUser);
    $dataUser->save();
    // Session::flash('flash_message','Adiantamento excluído com sucesso');

    return redirect()->back()->with(['success' => 'Restaurado com sucesso!']);;
  }


  // public function search(Request $request) {
  //   //Recupera os dados do formulário
  //   $dataForm = $request->except('_token');
  //   // $title    = $this->model;
  //   $title = "Responder ";
  //
  //   if (!$request->input('key-search')) {
  //
  //     return redirect()->action(
  //       'Painel\NotaEntregaController@index'
  //     );
  //   }
  //
  //   //Filtra os usuários
  //   $data = $this->model
  //   ->whereHas('Entrada', function ($q) {
  //          $q->where('nome', 'LIKE', "%{$dataForm['key-search']}%");
  //       })
  //   ->where('titulo', 'LIKE', "%{$dataForm['key-search']}%")
  //   ->orWhere('autor', $dataForm['key-search'])
  //   ->orWhere('isbn', $dataForm['key-search'])
  //   ->orWhere('percent', $dataForm['key-search'])
  //
  //   ->paginate($this->totalPage);
  //
  //   return view("{$this->view}.index", compact('data', 'dataForm', 'title'));
  // }


}
