<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EntradaVeiculo;
use App\Models\AcessoVeiculo;
use App\Models\PecaManutencao;

class PecaManutencaoController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public $title  = 'Peça Manutenção';
  public $route = 'peca-manutencao';
  protected $totalPage = 12;
  public $view = 'painel.entradas';

  public function __construct(PecaManutencao $peca)
  {
    $this->middleware('auth');
    $this->model = $peca;

  }
  public function index()
  {
    $data = $this->model->with('acesso_veiculos')->get();
    dd($data);
    return view("{$this->view}.index",compact('title','users','data'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {

    $this->validate($request, [
      'peca_manutencao' => 'required|unique:peca_manutencaos',
    ], [
      'peca_manutencao.required' => 'Favor, Inserir a Falha Ocorrida!',
      'peca_manutencao.unique' => 'Ops, Temos essa Falha Cadastrada, basta selecionar!',
    ]);

    $dataUser = new $this->model;

    $dataUser = $request->all();

    $insert = $this->model->create($dataUser);

    return back()->withInput($request->input());
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }
}
