<?php

namespace App\Http\Requests\Painel;

use App\Models\EntradaVeiculo;
use Illuminate\Foundation\Http\FormRequest;

class Manutencao extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'entrada_veiculo_id',
            'os_num',
            'falha',
            'local',
            'tempo_estimado',
            'tipo_manutencao',
            'concedido_acesso_por',
            'observacao',
            'date_concedido_acesso',
            'concluido_acesso_por',
            'comentarios',
            'date_concluido_acesso',
            'ativo',
        ];
    }



}
