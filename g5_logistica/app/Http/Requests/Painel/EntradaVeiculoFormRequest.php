<?php

namespace App\Http\Requests\Painel;

use Illuminate\Foundation\Http\FormRequest;

class EntradaVeiculoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'modelo'                       => 'required',
        'placa'                        => 'required',
        'cor'                          => 'required',
        ];
    }

    public function messages()
    {
      return [

        'modelo.required'         =>'O campo Modelo precisa ser preenchido',
        'placa.required'          =>'O campo Placa precisa ser selecionado',
        'cor.required'            =>'O campo Cor precisa ser selecionado',
        ];
    }
}
