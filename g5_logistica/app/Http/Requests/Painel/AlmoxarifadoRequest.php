<?php

namespace App\Http\Requests\Painel;

use Illuminate\Foundation\Http\FormRequest;

class AlmoxarifadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          '_date'=>'required',
          'produto'=>'required',
          'frota_equipamento'=>'required',
          'nota_fiscal'=>'required',
          'responsavel'=>'required',
          'custo_unitario'=>'required',
          'quantidade'=>'required',
          'fornecedor'=>'required',
          'estoque_estimado'=>'required',
          'data_de_entrega'=>'required',
          'total_de_compra'=>'required',
          'total_de_saida'=>'required',
          'numero_os'=>'required',

        ];
    }


}
