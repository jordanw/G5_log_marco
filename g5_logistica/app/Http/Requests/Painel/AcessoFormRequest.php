<?php

namespace App\Http\Requests\Painel;

use Illuminate\Foundation\Http\FormRequest;

class AcessoFormRequest extends FormRequest
{
  /**
  * Determine if the user is authorized to make this request.
  *
  * @return bool
  */
  public function authorize()
  {
    return true;
  }

  /**
  * Get the validation rules that apply to the request.
  *
  * @return array
  */
  public function rules()
  {
    return [
      'local_acesso'                  => 'required',
      'local_especifico'              => 'required',
      'tipo_fornecedor'               => 'required',
      'tempo_estimado'                => 'required',
    ];
  }

  public function messages()
  {
    return [

      'local_acesso.required'         =>'O campo Local precisa ser selecionado',
      'local_especifico.required'     =>'O campo Ap precisa ser selecionado',
      'tipo_fornecedor.required'      =>'O campo Tipo precisa ser selecionado',
      'tempo_estimado.required'       =>'O campo Tempo Estimado precisa ser selecionado',
    ];
  }
}
