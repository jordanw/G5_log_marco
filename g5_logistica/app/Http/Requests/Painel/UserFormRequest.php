<?php

namespace App\Http\Requests\Painel;

use Illuminate\Foundation\Http\FormRequest;

class UserFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
          'name'                  => "required|min:3|max:100'|unique:users,name,{$this->segment(3)},id",
          'username'              => "required|min:3|max:100'|unique:users,username,{$this->segment(3)},id",
          'telefone'              => 'required|min:3|max:100',
          'email'                 => "required|min:3|max:100|email",
          // 'email'                 => "required|min:3|max:100|email|unique:users,email,{$this->segment(3)},id",

          'image'                 => 'image|mimes:jpeg,bmp,png,jpg|dimensions:min_width=50,max_width=1500',

          // 'facebook'              => 'required|min:3|max:100',
          // 'twitter'               => 'required|min:3|max:100',
          // 'site'                  => 'required|min:3|max:200',
          // 'instagram'          => 'required|min:3|max:1000',
      ];
    }

    public function messages()
    {
        return [

          'telefone.required'     =>'O campo Telefone precisa ser preenchido',
          'username.required'     =>'O campo Username precisa ser preenchido',
          'username.unique'       =>'O campo Username precisa ser único',
          'email.required'        =>'O campo Email precisa ser preenchido',
          'email.email'           =>'O campo Email precisa ser válido',
          'image.dimensions'      =>'Tamanho máximo de 500px e Mínimo de 50px',
          'image.image'           =>'O Formato da imagem é inválido',
          'image.mimes'           =>'O Formato da imagem deve ser jpeg,bmp,png,jpg',

        ];
    }

}
