<?php

namespace App\Http\Requests\Painel;

use Illuminate\Foundation\Http\FormRequest;

class EntradaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'nome'                          => "required|min:3|max:100'|unique:entradas,nome,{$this->segment(3)},id",
        'sexo'                          => 'required',
        'tipo'                          => 'required',
        'cpf'                           =>  "unique:entradas,cpf,{$this->segment(3)},id",
        'identidade'                    =>  "unique:entradas,identidade,{$this->segment(3)},id",
      ];
    }

    public function messages()
    {
      return [

        'nome.required'         =>'O campo Nome precisa ser preenchido',
        'nome.unique'           =>'Já existe um visitate com esse nome, se não for o mesmo cadastre com nome diferente',
        'sexo.required'         =>'O campo Sexo precisa ser selecionado',
        'tipo.required'         =>'O campo Tipo precisa ser selecionado',
        'cpf.unique'            =>'Já existe um visitate com esse CPF',
        'identidade.unique'     =>'Já existe um visitate com essa Identidade',
      ];
    }
}
