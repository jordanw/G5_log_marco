-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 25/01/2018 às 15:28
-- Versão do servidor: 10.0.33-MariaDB-0ubuntu0.16.04.1
-- Versão do PHP: 7.1.13-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `stock`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `stocks`
--

CREATE TABLE `stocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `stockName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stockPrice` float NOT NULL,
  `stockYear` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `stocks`
--

INSERT INTO `stocks` (`id`, `stockName`, `stockPrice`, `stockYear`, `created_at`, `updated_at`) VALUES
(1, 'Infosys', 925, 1993, '2017-07-15 06:58:28', '2017-07-15 06:58:28'),
(2, 'Infosys', 950, 1992, '2017-07-15 07:10:46', '2017-07-15 07:10:46'),
(3, 'TCS', 2400, 1992, '2017-07-15 07:12:06', '2017-07-15 07:12:06'),
(4, 'TCS', 2500, 1993, '2017-07-15 07:12:18', '2017-07-15 07:12:18'),
(5, 'Reliance', 200, 1992, '2017-07-15 07:12:32', '2017-07-15 07:12:32'),
(6, 'Reliance', 220, 1993, '2017-07-15 07:12:43', '2017-07-15 07:12:43'),
(7, 'HUL', 100, 1994, '2017-07-15 07:13:00', '2017-07-15 07:13:00'),
(8, 'HUDCO', 20, 1996, '2017-07-15 07:32:35', '2017-07-15 07:32:35'),
(9, 'Tste Erick2', 900, 1991, '2017-07-15 14:54:17', '2017-07-15 14:54:17'),
(10, 'Infosys', 1000, 1995, '2017-07-15 14:55:08', '2017-07-15 14:55:08'),
(11, 'Infosys', 2000, 1996, '2017-07-15 14:55:19', '2017-07-15 14:55:19'),
(12, 'Infosys', 2500, 1994, '2017-07-16 03:03:26', '2017-07-16 03:03:26'),
(13, 'Tste Erick2', 1345, 1996, '2018-01-23 03:52:59', '2018-01-23 03:52:59'),
(14, 'Tste Erick2', 2000, 1991, '2018-01-23 03:55:41', '2018-01-23 03:55:41');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
