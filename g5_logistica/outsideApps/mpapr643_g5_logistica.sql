-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 17-Jan-2018 às 14:13
-- Versão do servidor: 5.5.51-38.2
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mpapr643_g5_logistica`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acessos`
--

CREATE TABLE IF NOT EXISTS `acessos` (
  `id` int(10) unsigned NOT NULL,
  `entrada_id` int(10) unsigned NOT NULL,
  `local_acesso` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `local_especifico` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_fornecedor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempo_estimado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observacao` text COLLATE utf8mb4_unicode_ci,
  `concedido_acesso_por` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_concedido_acesso` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `concluido_acesso_por` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_concluido_acesso` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `entradas`
--

CREATE TABLE IF NOT EXISTS `entradas` (
  `id` int(10) unsigned NOT NULL,
  `nome` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `cpf` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identidade` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sexo` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `responsavel` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cnpj` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `entrada_veiculos`
--

CREATE TABLE IF NOT EXISTS `entrada_veiculos` (
  `id` int(10) unsigned NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `city` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_year` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plate` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_code` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `return_code` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chassis` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `return_message` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_message` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsavel` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observacao` text COLLATE utf8mb4_unicode_ci,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `entrada_veiculos`
--

INSERT INTO `entrada_veiculos` (`id`, `image`, `user_id`, `city`, `model_year`, `plate`, `color`, `status_code`, `brand`, `return_code`, `date`, `state`, `chassis`, `year`, `return_message`, `model`, `status_message`, `responsavel`, `observacao`, `ativo`, `created_at`, `updated_at`) VALUES
(1, '201712260851055a4237a9dafa1.jpg', 10, 'SAO LUIS', '2013', 'EC-09', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H155FT', 'OPERANDO', 'User Demo', NULL, 1, '2017-12-25 13:12:46', '2017-12-26 10:51:05'),
(2, '201712251140455a410ded34037.jpg', 10, 'São Luís, Maranhão,', '2013', 'EC-13', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H155FT', 'OPERANDO', 'User Demo', NULL, 1, '2017-12-25 13:14:11', '2017-12-25 13:40:45'),
(3, '201712251140215a410dd5b731d.jpg', 10, 'São Luís, Maranhão,', '2013', 'EC-14', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H155FT', 'OPERANDO', 'User Demo', NULL, 1, '2017-12-25 13:25:23', '2017-12-25 13:40:21'),
(4, '201712251139505a410db6d7d25.jpg', 10, 'São Luís, Maranhão,', '2010', 'EC-11', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2010', NULL, 'H155FT', 'OPERANDO', 'User Demo', NULL, 1, '2017-12-25 13:26:16', '2017-12-25 13:42:37'),
(5, '201712251138455a410d7514ac5.jpg', 10, 'São Luís, Maranhão,', '2013', 'EC-15', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H155FT', 'OPERANDO', 'User Demo', NULL, 1, '2017-12-25 13:26:53', '2017-12-25 13:38:45'),
(6, '201712251136535a410d05e7e8c.jpg', 10, 'São Luís, Maranhão,', '2013', 'EC-19', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H155FT', 'OPERANDO', 'User Demo', NULL, 1, '2017-12-25 13:27:30', '2017-12-25 13:36:53'),
(7, '201712251139295a410da19356c.jpg', 10, 'São Luís, Maranhão,', '2013', 'EC-20', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H155FT', 'OPERANDO', 'User Demo', NULL, 1, '2017-12-25 13:28:29', '2017-12-25 13:39:29'),
(8, '201712251135595a410ccf59de9.jpg', 10, 'São Luís, Maranhão,', '2013', 'EC-21', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H155FT', 'OPERANDO', 'User Demo', NULL, 1, '2017-12-25 13:29:11', '2017-12-25 13:35:59'),
(9, '201712251131445a410bd0db272.jpg', 10, 'São Luís, Maranhão,', '2013', 'EC-22', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H155FT', 'OPERANDO', 'User Demo', NULL, 1, '2017-12-25 13:29:51', '2017-12-25 13:31:45'),
(10, '201712251145505a410f1e8d8a1.jpg', 10, 'São Luís, Maranhão,', '2010', 'EC-12', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2010', NULL, 'H155FT', 'OPERANDO', 'User Demo', NULL, 1, '2017-12-25 13:45:30', '2017-12-25 13:45:50'),
(11, '201712251147045a410f6863c79.jpg', 10, 'São Luís, Maranhão,', '2010', 'EC-16', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2010', NULL, 'H155FT', 'OPERANDO', 'User Demo', NULL, 1, '2017-12-25 13:47:04', '2017-12-25 13:47:04'),
(12, '201712251147545a410f9af3e50.jpg', 10, 'São Luís, Maranhão,', '2010', 'EC-17', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2010', NULL, 'H155FT', 'OPERANDO', 'User Demo', NULL, 1, '2017-12-25 13:47:54', '2017-12-25 13:47:54'),
(13, '201712260853025a42381e7d515.jpg', 10, 'São Luís, Maranhão,', '2012', 'EC-23', 'Laranja', NULL, 'HELLI', NULL, NULL, 'MA', NULL, '2012', NULL, 'CPCD70', 'OPERANDO', 'User Demo', NULL, 1, '2017-12-25 13:50:05', '2017-12-26 10:53:02'),
(14, '201712260852465a42380eef380.jpg', 10, 'São Luís, Maranhão,', '2010', 'ST-01', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2010', NULL, 'R45-35', 'OPERANDO', 'User Demo', NULL, 1, '2017-12-25 13:51:16', '2017-12-26 10:52:46'),
(15, '201712260852295a4237fd4dd53.jpg', 10, 'São Luís, Maranhão,', '2013', 'TC-01', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H155FT - 18ton', 'OPERANDO', 'User Demo', NULL, 1, '2017-12-25 13:54:12', '2017-12-26 10:52:29'),
(16, '201712260852135a4237eda0f32.jpg', 10, 'São Luís, Maranhão,', '2010', 'TC-02', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2010', NULL, 'H155FT - 18ton', 'OPERANDO', 'User Demo', NULL, 1, '2017-12-25 13:56:32', '2017-12-26 10:52:13'),
(17, '201712260851555a4237db8334f.jpg', 10, 'São Luís, Maranhão,', '2010', 'EC-07', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2010', NULL, 'H155FT', 'OPERANDO', 'User Demo', NULL, 1, '2017-12-25 13:59:41', '2017-12-26 10:51:55'),
(18, '201712260851395a4237cbdca7e.jpg', 10, 'São Luís, Maranhão,', '2010', 'EC-10', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2010', NULL, 'H155FT', 'OPERANDO', 'User Demo', NULL, 1, '2017-12-25 14:01:05', '2017-12-26 10:51:39');

-- --------------------------------------------------------

--
-- Estrutura da tabela `local_manutencaos`
--

CREATE TABLE IF NOT EXISTS `local_manutencaos` (
  `id` int(10) unsigned NOT NULL,
  `local_manutencao` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `local_manutencaos`
--

INSERT INTO `local_manutencaos` (`id`, `local_manutencao`, `created_at`, `updated_at`) VALUES
(1, 'OFICINA NO PORTO', '2017-12-25 13:20:55', '2017-12-25 13:20:55'),
(2, 'OFICINA VILA MARANHÃO', '2017-12-25 13:23:00', '2017-12-25 13:23:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `local_manutencao_veiculo`
--

CREATE TABLE IF NOT EXISTS `local_manutencao_veiculo` (
  `id` int(10) unsigned NOT NULL,
  `local_manutencao_id` int(10) unsigned NOT NULL,
  `veiculo_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `local_manutencao_veiculo`
--

INSERT INTO `local_manutencao_veiculo` (`id`, `local_manutencao_id`, `veiculo_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, NULL, NULL),
(2, 1, 5, NULL, NULL),
(3, 1, 5, NULL, NULL),
(4, 1, 11, NULL, NULL),
(5, 1, 8, NULL, NULL),
(6, 1, 12, NULL, NULL),
(7, 1, 15, '2018-01-11 10:17:32', '2018-01-11 10:17:32'),
(8, 1, 5, '2018-01-11 10:19:16', '2018-01-11 10:19:16'),
(9, 2, 18, '2018-01-16 16:32:51', '2018-01-16 16:32:51'),
(10, 2, 18, '2018-01-17 12:55:50', '2018-01-17 12:55:50');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(41, '2014_10_12_000000_create_users_table', 1),
(42, '2014_10_12_100000_create_password_resets_table', 1),
(43, '2017_10_22_233345_create_entradas_table', 1),
(44, '2017_10_23_170336_create_entrada_veiculos_table', 1),
(45, '2017_10_23_232909_create_nota_entregas_table', 1),
(46, '2017_11_17_094813_create_tasks_table', 1),
(47, '2017_11_20_131239_create_acessos_table', 1),
(48, '2017_11_23_160754_create_veiculos_table', 1),
(49, '2017_11_24_162104_create_placas_table', 1),
(50, '2017_12_17_145058_create_peca_manutencaos_table', 1),
(51, '2017_12_17_145114_create_local_manutencaos_table', 1),
(52, '2017_12_18_000053_create_local_manutencao_veiculo_table', 1),
(53, '2017_12_18_000127_create_peca_manutencao_veiculo_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `nota_entregas`
--

CREATE TABLE IF NOT EXISTS `nota_entregas` (
  `id` int(10) unsigned NOT NULL,
  `qtdade` int(11) NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nf` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entrada_id` int(10) unsigned NOT NULL,
  `titulo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `autor` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isbn` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_cadastrou` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor_pvp` double(20,2) NOT NULL,
  `percent` double(10,2) NOT NULL,
  `valor_unit` double(10,2) NOT NULL,
  `valor_percent` double(10,2) NOT NULL,
  `valor_total` double(25,2) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `prestado_por` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_prestado` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `restaurado_por` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_restaurado` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `peca_manutencaos`
--

CREATE TABLE IF NOT EXISTS `peca_manutencaos` (
  `id` int(10) unsigned NOT NULL,
  `peca_manutencao` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `peca_manutencaos`
--

INSERT INTO `peca_manutencaos` (`id`, `peca_manutencao`, `created_at`, `updated_at`) VALUES
(1, 'RADIADOR', '2017-12-25 13:20:24', '2017-12-25 13:20:24'),
(2, 'VÁLVULA DE REGULAGEM DE PRESSÃO', '2017-12-25 13:22:40', '2017-12-25 13:22:40'),
(3, 'MOTOR DE PARTIDA AVARIADO', '2017-12-26 11:00:28', '2017-12-26 11:00:28'),
(4, 'CABO DO DISTRIBUIDOR SOLTO', '2017-12-26 11:11:56', '2017-12-26 11:11:56'),
(5, 'SUPORTE DO FAROL FOLGADO', '2017-12-26 11:13:15', '2017-12-26 11:13:15'),
(6, 'VAZAMENTO NA JUNÇÃO DO ORBITROL DO COMANDO', '2017-12-26 11:46:53', '2017-12-26 11:46:53'),
(7, 'VAZAMENTO NO ORING DO ORBITROL', '2017-12-26 11:48:38', '2017-12-26 11:48:38'),
(8, 'VAZAMENTO NA MANGUEIRA DE ELEVAÇÃO', '2017-12-26 11:48:52', '2017-12-26 11:48:52'),
(9, 'CORRENTE DA TORRE FOLGADA', '2017-12-26 11:49:04', '2017-12-26 11:49:04'),
(10, 'BAIXO NÍVEL DE ÓLEO HIDRÁULICO', '2017-12-26 11:49:16', '2017-12-26 11:49:16'),
(11, 'VÁLVULA DE ELEVAÇÃO DA TORRE DESREGULADA', '2017-12-26 11:49:50', '2017-12-26 11:49:50'),
(12, 'VAZAMENTO NO MANGOTE DO RADIADOR', '2017-12-26 11:50:34', '2017-12-26 11:50:34'),
(13, 'VAZAMENTO NA BOMBA HIDRÁULICA', '2017-12-26 11:50:45', '2017-12-26 11:50:45'),
(14, 'FUSIVEL QUEIMADO', '2017-12-26 11:50:56', '2017-12-26 11:50:56'),
(15, 'BAIXO NÍVEL DE ÓLEO DO MOTOR', '2017-12-26 11:51:10', '2017-12-26 11:51:10'),
(17, 'PARADA PROGRAMADA DE 500 HORAS', '2018-01-16 16:08:22', '2018-01-16 16:08:22'),
(18, 'MANUTENÇÃO PREVENTIVA DE 1000 HORAS', '2018-01-16 16:32:33', '2018-01-16 16:32:33');

-- --------------------------------------------------------

--
-- Estrutura da tabela `peca_manutencao_veiculo`
--

CREATE TABLE IF NOT EXISTS `peca_manutencao_veiculo` (
  `id` int(10) unsigned NOT NULL,
  `peca_manutencao_id` int(10) unsigned NOT NULL,
  `veiculo_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `peca_manutencao_veiculo`
--

INSERT INTO `peca_manutencao_veiculo` (`id`, `peca_manutencao_id`, `veiculo_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, NULL, NULL),
(2, 6, 5, NULL, NULL),
(3, 7, 5, NULL, NULL),
(4, 8, 11, NULL, NULL),
(5, 16, 8, NULL, NULL),
(6, 9, 12, NULL, NULL),
(7, 10, 15, '2018-01-11 10:17:32', '2018-01-11 10:17:32'),
(8, 14, 5, '2018-01-11 10:19:16', '2018-01-11 10:19:16'),
(9, 1, 18, '2018-01-16 16:32:51', '2018-01-16 16:32:51'),
(10, 13, 18, '2018-01-17 12:55:50', '2018-01-17 12:55:50');

-- --------------------------------------------------------

--
-- Estrutura da tabela `placas`
--

CREATE TABLE IF NOT EXISTS `placas` (
  `id` int(10) unsigned NOT NULL,
  `placa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marca` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modelo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ano_fabricacao` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `combustivel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `entrada_id` int(10) unsigned NOT NULL,
  `tarefa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observacao` text COLLATE utf8mb4_unicode_ci,
  `responsavel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data_estimada` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_conclusao` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `senha` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) DEFAULT NULL,
  `nivel` int(11) NOT NULL,
  `send_mail` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nascimento` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `senha`, `telefone`, `status`, `nivel`, `send_mail`, `image`, `nascimento`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Erick Raposo', 'azafilmes@gmail.com', 'ericklive', '$2y$10$YkVjr9l8PTUkhlkuJiB2hebioY0hbsvkdDfGVO.2FeF5MaPCxiN5W', '123456S', '(98) 99195-6153', NULL, 1, 0, NULL, NULL, 'tfXz4l3iWT', NULL, NULL),
(2, 'User Demo', 'user@user.com', 'userdemo', '$2y$10$nztjfUepQENKYzwOG3v2ZO0jm4PNRkte4LpHPsvzU5TDxZjyygpey', '123456', '(98) 99195-6153', NULL, 1, 0, NULL, NULL, 'zOBmg9UIwHTTXFg2doHraKnk9eZaMgqvoVQX8hvjMooXEITklLnfjXUUwsCK', NULL, NULL),
(3, 'Usuário Teste', 'teste@teste.com', 'userteste', '$2y$10$Mj/3ehyA.6oYIqYNfJJdhOF/2NCsf/yXqAbUgteYxuw7wrxmRrqOe', '123456', '(98) 99999-9999', NULL, 1, 0, NULL, NULL, 'JrUVcRSXIn', NULL, NULL),
(13, 'Lucas R. Ferreira', 'lucasrferreira94@gmail.com', 'LRF', '$2y$10$dUkpkDv4HzCwqdiPce4gXeTF9fsAeB8HiN1kcG3OpU9itriR/IX0W', 'c15861', '(98) 98279-0870', NULL, 2, 1, NULL, '1998-01-01', 'PpHEFUseDagO4Wmf4VXsqg58m7T6FHcsaabFXdkfaJD76nzPHjtOlttIQ32m', '2017-12-26 12:01:09', '2017-12-26 12:01:09'),
(12, 'ADEILTON', 'adeilton.almeida@g5sl.com.br', 'ADE', '$2y$10$SPyky5k/MRSeeqrkhzf.POnt28H2CF9Eao1DkF5zAUCURHT51o5Sa', '5dd3ec', '(98) 99175-5149', NULL, 3, 1, NULL, '1981-10-01', NULL, '2017-12-26 11:32:48', '2017-12-26 11:32:48'),
(11, 'Thiago', 'thiago.santos@g5sl.com.br', 'TFS', '$2y$10$sTINE0GV9e2zksbGNYSs1.vJiTiFzKaToRL89JuKt99lFTh6zAbia', '9a0400', '(98) 99116-4226', NULL, 2, 1, NULL, '1993-01-01', NULL, '2017-12-26 11:28:05', '2017-12-26 11:28:05'),
(10, 'WENER SANTOS', 'wener.santos@hotmail.com', 'WMTS', '$2y$10$2C8yXd2tcQqn6gbvDLX9SePUKiShmcI3fThC08K8Z4ajfUD/tR572', 'ceacad', '(98) 98291-0276', NULL, 2, 1, NULL, '1974-10-01', 'C0LKe3MRKsFLz0wipz6LTyrXZGpgDv2vPRBLqwTHF4XD5PHHm0m8oLd6wwYr', '2017-12-26 11:23:00', '2017-12-26 11:23:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `veiculos`
--

CREATE TABLE IF NOT EXISTS `veiculos` (
  `id` int(10) unsigned NOT NULL,
  `entrada_veiculo_id` int(10) unsigned NOT NULL,
  `os_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `falhas` text COLLATE utf8mb4_unicode_ci,
  `tempo_estimado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `concedido_acesso_por` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observacao` text COLLATE utf8mb4_unicode_ci,
  `date_concedido_acesso` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `concluido_acesso_por` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comentarios` text COLLATE utf8mb4_unicode_ci,
  `date_concluido_acesso` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `veiculos`
--

INSERT INTO `veiculos` (`id`, `entrada_veiculo_id`, `os_num`, `falhas`, `tempo_estimado`, `concedido_acesso_por`, `observacao`, `date_concedido_acesso`, `concluido_acesso_por`, `comentarios`, `date_concluido_acesso`, `ativo`, `created_at`, `updated_at`) VALUES
(22, 18, '142', 'VAZAMENTO NA BOMBA HIDRÁULICA', '2018-01-18 10:55:50', 'Thiago', '<p>ssfhuoifjwuifwjioefjwfojifjeo</p>', '2018-01-17 10:55:50', 'Thiago', '<p>eweoihfwiofjeio</p>', '2018-01-17 10:56:34', 0, '2018-01-17 12:55:50', '2018-01-17 12:56:34'),
(21, 18, NULL, NULL, '2018-01-17 14:32:51', 'Thiago', '<p>Relizado conforme check list</p>', '2018-01-16 14:32:51', 'Thiago', NULL, '2018-01-16 14:37:09', 0, '2018-01-16 16:32:51', '2018-01-16 16:37:09'),
(19, 2, NULL, 'PARADA PROGRAMADA DE 500 HORAS', '2018-01-17 14:09:56', 'WENER SANTOS', '<p>Foi realizado o manuten&ccedil;&atilde;o de acordo check list de preventivas.</p>', '2018-01-16 14:09:56', 'WENER SANTOS', NULL, '2018-01-16 14:11:31', 0, '2018-01-16 16:09:56', '2018-01-16 16:11:31'),
(20, 16, '12', 'VÁLVULA DE REGULAGEM DE PRESSÃO', '2018-01-26 14:29:29', 'WENER SANTOS', '<p>buguyghyighbftuyffddcytyvuyi</p>', '2018-01-16 14:29:29', 'WENER SANTOS', NULL, '2018-01-16 14:30:29', 0, '2018-01-16 16:29:29', '2018-01-16 16:30:29'),
(18, 2, NULL, 'PARADA PROGRAMADA DE 500 HORAS', '2018-01-17 14:09:50', 'WENER SANTOS', '<p>Foi realizado o manuten&ccedil;&atilde;o de acordo check list de preventivas.</p>', '2018-01-16 14:09:50', 'WENER SANTOS', NULL, '2018-01-16 14:25:21', 0, '2018-01-16 16:09:50', '2018-01-16 16:25:21'),
(17, 2, NULL, 'PARADA PROGRAMADA DE 500 HORAS', '2018-01-17 14:09:16', 'WENER SANTOS', '<p>Foi realizado o manuten&ccedil;&atilde;o de acordo check list de preventivas.</p>', '2018-01-16 14:09:16', 'WENER SANTOS', NULL, '2018-01-16 14:14:10', 0, '2018-01-16 16:09:16', '2018-01-16 16:14:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acessos`
--
ALTER TABLE `acessos`
  ADD PRIMARY KEY (`id`), ADD KEY `acessos_entrada_id_foreign` (`entrada_id`);

--
-- Indexes for table `entradas`
--
ALTER TABLE `entradas`
  ADD PRIMARY KEY (`id`), ADD KEY `entradas_user_id_foreign` (`user_id`);

--
-- Indexes for table `entrada_veiculos`
--
ALTER TABLE `entrada_veiculos`
  ADD PRIMARY KEY (`id`), ADD KEY `entrada_veiculos_user_id_foreign` (`user_id`);

--
-- Indexes for table `local_manutencaos`
--
ALTER TABLE `local_manutencaos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `local_manutencao_veiculo`
--
ALTER TABLE `local_manutencao_veiculo`
  ADD PRIMARY KEY (`id`), ADD KEY `local_manutencao_veiculo_local_manutencao_id_foreign` (`local_manutencao_id`), ADD KEY `local_manutencao_veiculo_veiculo_id_foreign` (`veiculo_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nota_entregas`
--
ALTER TABLE `nota_entregas`
  ADD PRIMARY KEY (`id`), ADD KEY `nota_entregas_entrada_id_foreign` (`entrada_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `peca_manutencaos`
--
ALTER TABLE `peca_manutencaos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peca_manutencao_veiculo`
--
ALTER TABLE `peca_manutencao_veiculo`
  ADD PRIMARY KEY (`id`), ADD KEY `peca_manutencao_veiculo_peca_manutencao_id_foreign` (`peca_manutencao_id`), ADD KEY `peca_manutencao_veiculo_veiculo_id_foreign` (`veiculo_id`);

--
-- Indexes for table `placas`
--
ALTER TABLE `placas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`), ADD KEY `tasks_user_id_foreign` (`user_id`), ADD KEY `tasks_entrada_id_foreign` (`entrada_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_name_unique` (`name`), ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- Indexes for table `veiculos`
--
ALTER TABLE `veiculos`
  ADD PRIMARY KEY (`id`), ADD KEY `veiculos_entrada_veiculo_id_foreign` (`entrada_veiculo_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acessos`
--
ALTER TABLE `acessos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `entradas`
--
ALTER TABLE `entradas`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `entrada_veiculos`
--
ALTER TABLE `entrada_veiculos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `local_manutencaos`
--
ALTER TABLE `local_manutencaos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `local_manutencao_veiculo`
--
ALTER TABLE `local_manutencao_veiculo`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `nota_entregas`
--
ALTER TABLE `nota_entregas`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `peca_manutencaos`
--
ALTER TABLE `peca_manutencaos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `peca_manutencao_veiculo`
--
ALTER TABLE `peca_manutencao_veiculo`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `placas`
--
ALTER TABLE `placas`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `veiculos`
--
ALTER TABLE `veiculos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
