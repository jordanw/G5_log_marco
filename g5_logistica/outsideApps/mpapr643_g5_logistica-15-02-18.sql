-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 15-Fev-2018 às 17:10
-- Versão do servidor: 5.5.51-38.2
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mpapr643_g5_logistica`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acessos`
--

CREATE TABLE IF NOT EXISTS `acessos` (
  `id` int(10) unsigned NOT NULL,
  `entrada_id` int(10) unsigned NOT NULL,
  `local_acesso` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `local_especifico` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_fornecedor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempo_estimado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observacao` text COLLATE utf8mb4_unicode_ci,
  `concedido_acesso_por` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_concedido_acesso` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `concluido_acesso_por` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_concluido_acesso` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `entradas`
--

CREATE TABLE IF NOT EXISTS `entradas` (
  `id` int(10) unsigned NOT NULL,
  `nome` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `cpf` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identidade` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sexo` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `responsavel` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cnpj` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `entrada_veiculos`
--

CREATE TABLE IF NOT EXISTS `entrada_veiculos` (
  `id` int(10) unsigned NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `city` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_year` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plate` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_code` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `return_code` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chassis` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `return_message` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_message` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsavel` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observacao` text COLLATE utf8mb4_unicode_ci,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `entrada_veiculos`
--

INSERT INTO `entrada_veiculos` (`id`, `image`, `user_id`, `city`, `model_year`, `plate`, `color`, `status_code`, `brand`, `return_code`, `date`, `state`, `chassis`, `year`, `return_message`, `model`, `status_message`, `responsavel`, `observacao`, `ativo`, `created_at`, `updated_at`) VALUES
(1, '201801261255115a6b4f5f19846.jpg', 3, 'São Luís', '2013', 'CE-02', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H155FT', 'OPERANDO', 'WENER SANTOS', NULL, 1, '2018-01-26 14:55:11', '2018-01-26 14:55:11'),
(2, '201801261305225a6b51c2494b0.jpg', 3, 'São Luís', '2013', 'CF-03', 'Amarelo', NULL, 'HYUNDAI', NULL, NULL, 'MA', NULL, '2013', NULL, 'H160D-7E', 'OPERANDO', 'WENER SANTOS', NULL, 1, '2018-01-26 15:05:22', '2018-01-26 15:08:59'),
(3, '201801261313015a6b538d37e36.jpg', 3, 'São Luís', '2013', 'EC-13', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H155FT', 'OPERANDO', 'WENER SANTOS', NULL, 1, '2018-01-26 15:13:01', '2018-01-26 15:13:01'),
(4, '201801261318255a6b54d1cd28c.jpg', 3, 'São Luís', '2013', 'EC-15', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H155FT', 'OPERANDO', 'WENER SANTOS', NULL, 1, '2018-01-26 15:18:25', '2018-01-26 15:18:25'),
(5, '201801261319395a6b551b23c3a.jpg', 3, 'São Luís', '2010', 'EC-16', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2010', NULL, 'H155FT', 'OPERANDO', 'WENER SANTOS', NULL, 1, '2018-01-26 15:19:39', '2018-01-26 15:19:39'),
(6, '201801261452455a6b6aed15d38.jpg', 3, 'São Luís', '2010', 'EC-17', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2010', NULL, 'H155FT', 'OPERANDO', 'WENER SANTOS', NULL, 1, '2018-01-26 16:52:45', '2018-01-26 16:52:45'),
(7, '201801261456405a6b6bd8915c4.jpg', 3, 'São Luís', '2013', 'EC-21', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H155FT', 'OPERANDO', 'WENER SANTOS', NULL, 1, '2018-01-26 16:56:40', '2018-01-26 16:56:40'),
(8, NULL, 3, 'São Luís', '2013', 'CF-03', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H160D-7E', 'OPERANDO', 'WENER SANTOS', NULL, 1, '2018-02-02 12:25:39', '2018-02-02 12:25:39'),
(9, NULL, 3, 'São Luís', '2010', 'EC-22', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2010', NULL, 'H155FT', 'OPERANDO', 'WENER SANTOS', NULL, 1, '2018-02-02 12:27:44', '2018-02-02 12:27:44'),
(10, NULL, 3, 'São Luís', '2013', 'EC-19', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H155FT', 'OPERANDO', 'WENER SANTOS', NULL, 1, '2018-02-02 12:28:41', '2018-02-02 12:28:41'),
(11, NULL, 3, 'São Luís', '2013', 'TC-01', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H155FT', 'OPERANDO', 'WENER SANTOS', NULL, 1, '2018-02-02 12:29:56', '2018-02-02 12:29:56'),
(12, NULL, 3, 'São Luís', '2010', 'TC-02', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2010', NULL, 'H155FT - 18ton', 'OPERANDO', 'WENER SANTOS', NULL, 1, '2018-02-02 12:30:56', '2018-02-02 12:30:56'),
(13, NULL, 3, 'São Luís', '2010', 'CE-01', 'Amarelo', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2010', NULL, 'H155FT', 'OPERANDO', 'WENER SANTOS', NULL, 1, '2018-02-02 12:46:33', '2018-02-02 12:46:33'),
(14, NULL, 2, 'São Luís', '2013', 'EC-22', 'AMARELA', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H155FT', 'ATIVO', 'Lucas R. Ferreira', NULL, 0, '2018-02-08 13:42:31', '2018-02-08 13:49:02'),
(15, NULL, 2, 'São Luís', '2013', 'EC-20', 'AMARELA', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H155FT', 'ATIVO', 'Lucas R. Ferreira', NULL, 1, '2018-02-08 13:53:28', '2018-02-08 13:53:28'),
(16, NULL, 2, 'São Luís', '2013', 'EC-14', 'AMARELA', NULL, 'HYSTER', NULL, NULL, 'MA', NULL, '2013', NULL, 'H155FT', 'ATIVA', 'Lucas R. Ferreira', NULL, 1, '2018-02-15 14:08:11', '2018-02-15 14:08:11');

-- --------------------------------------------------------

--
-- Estrutura da tabela `local_manutencaos`
--

CREATE TABLE IF NOT EXISTS `local_manutencaos` (
  `id` int(10) unsigned NOT NULL,
  `local_manutencao` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `criado_por` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `local_manutencaos`
--

INSERT INTO `local_manutencaos` (`id`, `local_manutencao`, `criado_por`, `created_at`, `updated_at`) VALUES
(1, 'PORTO', NULL, '2018-01-26 15:36:30', '2018-01-26 15:36:30'),
(2, 'OFICINA VILA MARANHÃO', NULL, '2018-01-26 15:36:52', '2018-01-26 15:36:52');

-- --------------------------------------------------------

--
-- Estrutura da tabela `local_manutencao_veiculo`
--

CREATE TABLE IF NOT EXISTS `local_manutencao_veiculo` (
  `id` int(10) unsigned NOT NULL,
  `veiculo_id` int(10) unsigned NOT NULL,
  `local_manutencao_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `manutencaos`
--

CREATE TABLE IF NOT EXISTS `manutencaos` (
  `id` int(10) unsigned NOT NULL,
  `entrada_veiculo_id` int(10) unsigned NOT NULL,
  `os_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `falha` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `local` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tempo_estimado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_manutencao` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `concedido_acesso_por` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observacao` text COLLATE utf8mb4_unicode_ci,
  `date_concedido_acesso` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `concluido_acesso_por` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comentarios` text COLLATE utf8mb4_unicode_ci,
  `date_concluido_acesso` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `manutencaos`
--

INSERT INTO `manutencaos` (`id`, `entrada_veiculo_id`, `os_num`, `falha`, `local`, `tempo_estimado`, `tipo_manutencao`, `concedido_acesso_por`, `observacao`, `date_concedido_acesso`, `concluido_acesso_por`, `comentarios`, `date_concluido_acesso`, `ativo`, `created_at`, `updated_at`) VALUES
(1, 5, '54', 'CILINDRO MESTRE AVARIADO', 'PORTO', '2018-01-26 14:37:31', 'Corretiva', 'WENER SANTOS', NULL, '2018-01-26 13:37:31', 'WENER SANTOS', NULL, '2018-01-26 14:46:26', 0, '2018-01-26 15:37:31', '2018-01-26 16:46:26'),
(2, 5, '78', 'BAIXO NÍVEL DE ÓLEO HIDRÁULICO', 'PORTO', '2018-01-26 14:38:17', 'Corretiva', 'WENER SANTOS', NULL, '2018-01-26 13:38:17', 'WENER SANTOS', NULL, '2018-01-26 15:47:23', 0, '2018-01-26 15:38:17', '2018-01-26 17:47:23'),
(3, 5, '70', 'SENSOR DO PEDAL FALHANDO', 'PORTO', '2018-01-26 14:42:16', 'Corretiva', 'WENER SANTOS', NULL, '2018-01-26 13:42:16', 'WENER SANTOS', NULL, '2018-01-26 15:47:30', 0, '2018-01-26 15:42:16', '2018-01-26 17:47:30'),
(4, 5, '76', 'VAZAMENTO NO CILINDRO DO CLAMP', 'PORTO', '2018-01-26 14:42:45', 'Corretiva', 'WENER SANTOS', NULL, '2018-01-26 13:42:45', 'WENER SANTOS', NULL, '2018-01-26 15:47:40', 0, '2018-01-26 15:42:45', '2018-01-26 17:47:40'),
(5, 5, '85', 'BAIXO NÍVEL DE ÓLEO DE TRANSMISSÃO', 'OFICINA VILA MARANHÃO', '2018-01-26 15:43:21', 'Corretiva', 'WENER SANTOS', NULL, '2018-01-26 13:43:21', 'WENER SANTOS', NULL, '2018-01-26 15:47:48', 0, '2018-01-26 15:43:21', '2018-01-26 17:47:48'),
(6, 4, '28', 'PARADA PROGRAMADA DE 500 HORAS', 'PORTO', '2018-01-26 14:45:02', 'Preventiva', 'WENER SANTOS', '<p>FOI REALIZADA A INSTALA&Ccedil;&Atilde;O DO RADIADOR HIDR&Aacute;ULICO. FOI COMPLETADO O N&Iacute;VEL DE &Oacute;LEO HIDR&Aacute;ULICO COM 15L. FOI REAPERTADO A ABRA&Ccedil;ADEIRA DA MANGUEIRA DE RETORNO DE &Oacute;LEO DO COMANDO DE V&Aacute;LVULAS.</p>', '2018-01-26 13:45:02', 'WENER SANTOS', NULL, '2018-01-26 15:47:56', 0, '2018-01-26 15:45:02', '2018-01-26 17:47:56'),
(7, 4, '77', 'VAZAMENTO NA MANGUEIRA DE ELEVAÇÃO', 'PORTO', '2018-01-26 14:47:03', 'Corretiva', 'WENER SANTOS', '<p>MANGUEIRA PRINCIPAL DA ELEVA&Ccedil;&Atilde;O ESTOURADA. FOI SUBSTITUIDA A MESMA. SUBSTITUIDO O ORING DA V&Aacute;LVULA DO BLOCO DE INCLINA&Ccedil;&Atilde;O</p>', '2018-01-26 13:47:03', 'WENER SANTOS', NULL, '2018-01-26 15:48:03', 0, '2018-01-26 15:47:03', '2018-01-26 17:48:03'),
(8, 4, '81', 'BAIXO NÍVEL DE ÓLEO HIDRÁULICO', 'PORTO', '2018-01-26 14:50:41', 'Corretiva', 'WENER SANTOS', '<p>FOI COMPLETADO O N&Iacute;VEL DE &Oacute;LEO COM 15L DE &Oacute;LEO 68 E VERIFICADO &Oacute;LEO DE MOTOR E &Aacute;GUA</p>', '2018-01-26 13:50:41', 'WENER SANTOS', NULL, '2018-01-26 15:48:12', 0, '2018-01-26 15:50:41', '2018-01-26 17:48:12'),
(9, 4, '93', 'VAZAMENTO NO CILINDRO DO CLAMP', 'PORTO', '2018-01-26 15:42:35', 'Corretiva', 'WENER SANTOS', '<p>FOI FEITO REPARO NO CILINDRO. COMPLETADO O N&Iacute;VEL DE &Oacute;LEO COM 6L</p>', '2018-01-26 14:42:35', 'WENER SANTOS', NULL, '2018-01-26 14:47:45', 0, '2018-01-26 16:42:35', '2018-01-26 16:47:45'),
(10, 4, '93', 'VAZAMENTO NA MANGUEIRA DE ELEVAÇÃO', 'PORTO', '2018-01-26 15:43:36', 'Corretiva', 'WENER SANTOS', '<p>FOI SUBSTITUIDA A MESMA</p>', '2018-01-26 14:43:36', 'WENER SANTOS', NULL, '2018-01-26 15:48:19', 0, '2018-01-26 16:43:36', '2018-01-26 17:48:19'),
(11, 4, '93', 'VAZAMENTO NA MANGUEIRA DE COMANDO', 'PORTO', '2018-01-26 15:44:40', 'Corretiva', 'WENER SANTOS', '<p><strong>FOI SUBSTITUIDA A MESMA</strong></p>', '2018-01-26 14:44:40', 'WENER SANTOS', NULL, '2018-01-26 14:47:35', 0, '2018-01-26 16:44:40', '2018-01-26 16:47:35'),
(12, 6, '86', 'BAIXO NÍVEL DE ÓLEO HIDRÁULICO', 'PORTO', '2018-01-26 15:53:51', 'Corretiva', 'WENER SANTOS', '<p>COMPLETADO COM 8L DE &Oacute;LEO HIDR&Aacute;ULICO 68</p>', '2018-01-26 14:53:51', 'WENER SANTOS', NULL, '2018-01-26 15:48:25', 0, '2018-01-26 16:53:51', '2018-01-26 17:48:25'),
(13, 6, '90', 'RODOGÁS ENCHARCADO', 'PORTO', '2018-01-26 15:55:42', 'Corretiva', 'WENER SANTOS', '<p>FOI FEITA A SUBSTITUI&Ccedil;&Atilde;O DO MESMO</p>', '2018-01-26 14:55:42', 'Thiago', NULL, '2018-01-27 11:36:13', 0, '2018-01-26 16:55:42', '2018-01-27 13:36:13'),
(14, 7, '69', 'BAIXO NÍVEL DE ÁGUA DO RADIADOR', 'PORTO', '2018-01-26 15:58:29', 'Corretiva', 'WENER SANTOS', '<p>FPO COMPLETADO O N&Iacute;VEL DE &Aacute;GUA DO RADIADOR, FOI TAMB&Eacute;M VERIFICADO TRODOS N&Iacute;VEL DE &Oacute;LEO MOTOR, HIDR&Aacute;ULICO, TRANSMISS&Atilde;O</p>', '2018-01-26 14:58:29', 'Thiago', NULL, '2018-01-27 11:42:13', 0, '2018-01-26 16:58:29', '2018-01-27 13:42:13'),
(15, 7, '80', 'VAZAMENTO NO CILINDRO DE INCLINAÇÃO', 'PORTO', '2018-01-26 16:45:44', 'Corretiva', 'WENER SANTOS', '<p>FOI FEITA AN&Aacute;LISE VISUAL E DESTACADO O PROBLEMA. FOI VERIFICADO N&Iacute;VEL DE &Oacute;LEO DE MOTOR, &Oacute;LEO HIDR&Aacute;ULICO E &Aacute;GUA. FOI FEITA UMA LIMPEZA NO FILTRO DE GLP E FILTRO DE AR. FOI FEITO REPARO NO CILINDRO DE INCLINA&Ccedil;&Atilde;O</p>', '2018-01-26 15:45:44', 'Thiago', NULL, '2018-01-27 11:42:30', 0, '2018-01-26 17:45:44', '2018-01-27 13:42:30'),
(16, 7, '82', 'INSTALAÇÃO DO RADIADOR', 'PORTO', '2018-01-26 16:46:26', 'Corretiva', 'WENER SANTOS', '<p>REALIZADO O SERVI&Ccedil;O</p>', '2018-01-26 15:46:26', 'Thiago', NULL, '2018-01-27 11:42:49', 0, '2018-01-26 17:46:26', '2018-01-27 13:42:49'),
(17, 7, '2012', 'SENSOR DO PEDAL FALHANDO', 'PORTO', '2018-01-27 13:24:22', 'Corretiva', 'Thiago', '<p>EQUIPAMENTO ENCONTRA-SE COM PROBLEMAS&nbsp;</p>', '2018-01-27 11:24:22', 'Thiago', '<p>FOI REALIZADO A TROCA NECESS&Aacute;RIA</p>', '2018-01-27 11:25:23', 0, '2018-01-27 13:24:22', '2018-01-27 13:25:23'),
(18, 1, '1212', 'INSTALAÇÃO DO RADIADOR', 'OFICINA VILA MARANHÃO', '', 'Corretiva', 'Thiago', NULL, '2018-01-27 11:31:49', 'Thiago', NULL, '2018-01-27 11:43:00', 0, '2018-01-27 13:31:49', '2018-01-27 13:43:00'),
(19, 7, '121', 'CILINDRO MESTRE AVARIADO', 'OFICINA VILA MARANHÃO', '2018-01-27 17:43:58', 'Corretiva', 'Thiago', NULL, '2018-01-27 11:43:58', 'WENER SANTOS', NULL, '2018-01-29 17:32:54', 0, '2018-01-27 13:43:58', '2018-01-29 19:32:54'),
(20, 6, '9899', 'CILINDRO MESTRE AVARIADO', 'PORTO', '2018-01-27 15:44:22', 'Corretiva', 'Thiago', NULL, '2018-01-27 11:44:22', 'WENER SANTOS', '<p>Teste via tablet &nbsp;e tudo bem</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>', '2018-01-29 17:35:37', 0, '2018-01-27 13:44:22', '2018-01-29 19:35:37'),
(21, 7, '212', 'VAZAMENTO DE AGUA', 'PORTO', '2018-01-30 18:36:51', 'Corretiva', 'Thiago', '<p>O EQUIPAMENTO APRESENTOU PROBLEMAS GRANDES</p>', '2018-01-30 14:36:51', 'Thiago', 'Realizada a manutenção de acordo com a ordem de compra e tbm bla bla bla', '2018-01-30 14:38:39', 0, '2018-01-30 16:36:51', '2018-01-30 16:38:39'),
(22, 13, '219', 'PARADA PROGRAMADA DE 500 HORAS', 'PORTO', '2018-02-02 12:47:35', 'Preventiva', 'WENER SANTOS', '<p>SUBSTITUIDO FILTROS DE AR PRIM&Aacute;RIO, DE AR SECUND&Aacute;RIO, DE &Oacute;LEO DE MOTOR, &nbsp;DE LINHA DO HIDR&Aacute;ULICO, DA TRANSMISS&Atilde;O, SEPARADOR DE &Aacute;GUA, SUSPIRO DO HIDR&Aacute;ULICO E DO COMBUST&Iacute;VEL.</p>', '2018-02-02 10:47:35', 'WENER SANTOS', NULL, '2018-02-02 10:47:49', 0, '2018-02-02 12:47:35', '2018-02-02 12:47:49'),
(23, 13, '195', 'BATERIA DESCARREGADA', 'PORTO', '2018-02-02 12:48:53', 'Corretiva', 'WENER SANTOS', '<p><strong>FOI COLOCADA A BATERIA PARA CARREGAR</strong></p>', '2018-02-02 10:48:53', 'WENER SANTOS', NULL, '2018-02-02 10:49:21', 0, '2018-02-02 12:48:53', '2018-02-02 12:49:21'),
(24, 8, '224', 'VAZAMENTO DE AGUA', 'PORTO', '2018-02-02 14:52:33', 'Corretiva', 'Lucas R. Ferreira', '<p>Foi feita a substitui&ccedil;&atilde;o do mesmo</p>', '2018-02-02 10:52:33', 'Lucas R. Ferreira', '<p>Foi feita a substitui&ccedil;&atilde;o do mesmo</p>', '2018-02-02 10:53:11', 0, '2018-02-02 12:52:33', '2018-02-02 12:53:11'),
(25, 1, '6', 'VAZAMENTO DE ÓLEO NA ALAVANCA DA CABINE', 'PORTO', '2018-02-02 12:52:41', 'Corretiva', 'WENER SANTOS', '<p>PROVIDENCIAR OS ORING&#39;S DA ALAVANCA</p>', '2018-02-02 10:52:41', 'WENER SANTOS', NULL, '2018-02-02 10:53:02', 0, '2018-02-02 12:52:41', '2018-02-02 12:53:02'),
(26, 1, '25', 'VAZAMENTO NA MANGUEIRA DE COMANDO', 'PORTO', '2018-02-02 12:58:04', 'Corretiva', 'WENER SANTOS', '<p>FOI VERIFICADO QUE A MANGUEIRA QUE PEGA NO COMANDO HIDR&Aacute;ULICO PARA CILINDRO ESTA COM VAZAMENTO A MESMAS FOI REMOFICA PARA SUBSTITUI&Ccedil;&Atilde;O</p>', '2018-02-02 10:58:04', 'WENER SANTOS', '<p>Defeito corrigido</p>', '2018-02-02 10:58:32', 0, '2018-02-02 12:58:04', '2018-02-02 12:58:32'),
(27, 1, '38', 'BAIXO NÍVEL DE ÓLEO HIDRÁULICO', 'PORTO', '2018-02-02 13:02:28', 'Corretiva', 'WENER SANTOS', '<p>FOI COMPLETADO O N&Iacute;VEL DE &Oacute;LEO COM 40L DE &Oacute;LEO</p>', '2018-02-02 11:02:28', 'WENER SANTOS', NULL, '2018-02-02 11:15:23', 0, '2018-02-02 13:02:28', '2018-02-02 13:15:23'),
(28, 1, '46', 'VAZAMENTO NA MANGUEIRA DO ORBITROL', 'PORTO', '2018-02-02 15:04:06', 'Corretiva', 'WENER SANTOS', '<p>FOI FEITA A SUBSTITUI&Ccedil;&Atilde;O &nbsp;DA MESMA E FOI COLOCADO 10L&nbsp;</p>', '2018-02-02 11:04:06', 'WENER SANTOS', NULL, '2018-02-02 11:15:32', 0, '2018-02-02 13:04:06', '2018-02-02 13:15:32'),
(29, 1, '44', 'VAZAMENTO NA MANGUEIRA  DO CILINDRO DE DIREÇÃO', 'PORTO', '2018-02-02 13:11:11', 'Corretiva', 'WENER SANTOS', '<p>LADO DIREITO ESTOURADA, ONDE A MESMA FOI SUBSTITUIDA</p>', '2018-02-02 11:11:11', 'WENER SANTOS', NULL, '2018-02-02 11:15:50', 0, '2018-02-02 13:11:11', '2018-02-02 13:15:50'),
(30, 1, '60', 'CLAMP AVARIADO', 'PORTO', '2018-02-02 13:12:13', 'Corretiva', 'WENER SANTOS', '<p>FOI RETIRADO PARTE DA CHAPA AVARIADA</p>', '2018-02-02 11:12:13', 'WENER SANTOS', NULL, '2018-02-02 11:16:08', 0, '2018-02-02 13:12:13', '2018-02-02 13:16:08'),
(31, 1, '61', 'FILTROS HIDRÁULICOS OBSTRUÍDOS', 'PORTO', '2018-02-02 13:13:04', 'Corretiva', 'WENER SANTOS', '<p>M&Aacute;QUINA SEM FREIO E SEM MOVIMENTO DA TORRE E CLAMP. FOI FEITA UMA LIMPEZA NOS FILTROS HIDR&Aacute;ULICOS</p>', '2018-02-02 11:13:04', 'WENER SANTOS', NULL, '2018-02-02 11:16:16', 0, '2018-02-02 13:13:04', '2018-02-02 13:16:16'),
(32, 1, '73', 'FILTROS HIDRÁULICOS OBSTRUÍDOS', 'PORTO', '2018-02-02 13:14:13', 'Corretiva', 'WENER SANTOS', '<p>M&Aacute;QUINA SEM FREIO. FOI FEITA UMA LIMPEZA NOS FILTROS HIDR&Aacute;ULICOS. REICID&Ecirc;NCIA.</p>', '2018-02-02 11:14:13', 'WENER SANTOS', NULL, '2018-02-02 11:16:23', 0, '2018-02-02 13:14:13', '2018-02-02 13:16:23'),
(33, 1, '84', 'VAZAMENTO NA MANGUEIRA  DO CILINDRO DE DIREÇÃO', 'PORTO', '2018-02-02 13:15:05', 'Corretiva', 'WENER SANTOS', '<p>FOI FEITA A SUBSTITUI&Ccedil;&Atilde;O DA MESMA</p>', '2018-02-02 11:15:05', 'WENER SANTOS', NULL, '2018-02-02 11:15:40', 0, '2018-02-02 13:15:05', '2018-02-02 13:15:40'),
(34, 1, '88', 'VAZAMENTO NA MANGUEIRA  DO CILINDRO DE DIREÇÃO', 'PORTO', '2018-02-02 13:17:43', 'Corretiva', 'WENER SANTOS', '<p>FOI FEITA A SUBSTITUI&Ccedil;&Atilde;O DAS DUAS MANGUEIRAS E COMPLETADO O N&Iacute;VEL DE &Oacute;LEO HIDR&Aacute;ULICO</p>', '2018-02-02 11:17:43', 'WENER SANTOS', NULL, '2018-02-02 11:17:51', 0, '2018-02-02 13:17:43', '2018-02-02 13:17:51'),
(35, 1, '107', 'VÁLVULAS DE RETENÇÃO DO CLAMP OBSTRUÍDA', 'PORTO', '2018-02-02 13:19:23', 'Corretiva', 'WENER SANTOS', '<p>FOI RETIRADA A MESMA PARA LIMPEZA</p>', '2018-02-02 11:19:23', 'Lucas R. Ferreira', '<p>FOI RETIRADA A MESMA PARA LIMPEZA</p>', '2018-02-08 11:47:32', 0, '2018-02-02 13:19:23', '2018-02-08 13:47:32'),
(36, 4, '257', 'MOTOR DE PARTIDA AVARIADO', 'PORTO', '2018-02-06 12:18:50', 'Corretiva', 'Lucas R. Ferreira', '<p>Motor de partida com problema no BENDIX. Foi retirado motor de partida da EC-16 e &eacute; colocado na EC-15.</p>', '2018-02-05 11:18:50', 'Lucas R. Ferreira', NULL, '2018-02-05 11:22:53', 0, '2018-02-05 13:18:50', '2018-02-05 13:22:53'),
(37, 4, '257', 'ROLAMENTO DO CARRO DE CARGA AVARIADO', 'PORTO', '2018-02-06 12:21:52', 'Corretiva', 'Lucas R. Ferreira', '<p>Foi substitu&iacute;do os rolamentos do carro de carga, completado o n&iacute;vel de &oacute;leo do motor com 2L</p>', '2018-02-05 11:21:52', 'Lucas R. Ferreira', NULL, '2018-02-05 11:33:45', 0, '2018-02-05 13:21:52', '2018-02-05 13:33:45'),
(38, 4, '257', 'VAZAMENTO NO CILINDRO DE INCLINAÇÃO', 'PORTO', '2018-02-05 17:27:16', 'Corretiva', 'Lucas R. Ferreira', NULL, '2018-02-05 11:27:16', 'Lucas R. Ferreira', '<p>Retirado vazamento de &oacute;leo hidr&aacute;ulico bonitinho da conex&atilde;o do cilindro de inclina&ccedil;&atilde;o. completado n&iacute;vel de &oacute;leo hiehidr&aacute;ul 20L</p>', '2018-02-05 11:28:33', 0, '2018-02-05 13:27:16', '2018-02-05 13:28:33'),
(39, 14, '223', 'VAZAMENTO NA BOMBA HIDRÁULICA', 'PORTO', '2018-02-08 15:46:10', 'Corretiva', 'Lucas R. Ferreira', NULL, '2018-02-08 11:46:10', 'Lucas R. Ferreira', '<p>FOI FEITA A TROCA DO ORING DA CONEX&Atilde;O DA BOMBA HIDR&Aacute;ULICA</p>', '2018-02-08 11:46:43', 0, '2018-02-08 13:46:10', '2018-02-08 13:46:43'),
(40, 15, '222', 'VAZAMENTO NA MANGUEIRA DO CILINDRO DE INCLINAÇÃO', 'PORTO', '2018-02-08 15:57:00', 'Corretiva', 'Lucas R. Ferreira', NULL, '2018-02-08 11:57:00', 'Lucas R. Ferreira', '<p>VAZAMENTO SAINDO DO COMANDO PARA O BLOCO DE INCLINA&Ccedil;&Atilde;O</p>', '2018-02-08 11:57:34', 0, '2018-02-08 13:57:00', '2018-02-08 13:57:34'),
(41, 8, '221', 'BAIXO NÍVEL DE ÓLEO HIDRÁULICO', 'PORTO', '2018-02-08 14:17:59', 'Corretiva', 'Lucas R. Ferreira', NULL, '2018-02-08 12:17:59', 'Lucas R. Ferreira', '<p>FOI COMPLETADO COM 20L</p>\r\n\r\n<p>&nbsp;</p>', '2018-02-08 12:18:21', 0, '2018-02-08 14:17:59', '2018-02-08 14:18:21'),
(42, 13, '221', 'BAIXO NÍVEL DE ÁGUA DO RADIADOR', 'PORTO', '2018-02-15 12:51:07', 'Corretiva', 'Lucas R. Ferreira', NULL, '2018-02-15 10:51:07', NULL, NULL, NULL, 1, '2018-02-15 12:51:07', '2018-02-15 12:51:07'),
(43, 8, '221', 'BAIXO NÍVEL DE ÁGUA DO RADIADOR', 'PORTO', '2018-02-15 12:52:13', 'Corretiva', 'Lucas R. Ferreira', NULL, '2018-02-15 10:52:13', 'Lucas R. Ferreira', '<p>foi completado o n&iacute;vel de &aacute;gua do radiador</p>', '2018-02-15 10:52:51', 0, '2018-02-15 12:52:13', '2018-02-15 12:52:51'),
(44, 13, '219', 'PARADA PROGRAMADA DE 500 HORAS', 'PORTO', '2018-02-15 14:53:57', 'Preventiva', 'Lucas R. Ferreira', NULL, '2018-02-15 10:53:57', 'Lucas R. Ferreira', '<p>SUBSTITUIDO FILTROS DE AR PRIM&Aacute;RIO, DE AR SECUND&Aacute;RIO, DE &Oacute;LEO DE MOTOR, &nbsp;DE LINHA DO HIDR&Aacute;ULICO, DA TRANSMISS&Atilde;O, SEPARADOR DE &Aacute;GUA, SUSPIRO DO HIDR&Aacute;ULICO E DO COMBUST&Iacute;VEL.</p>', '2018-02-15 10:54:38', 0, '2018-02-15 12:53:57', '2018-02-15 12:54:38'),
(45, 8, '218', 'BAIXO NÍVEL DE ÓLEO DE MOTOR', 'PORTO', '2018-02-15 13:23:09', 'Corretiva', 'Lucas R. Ferreira', NULL, '2018-02-15 11:23:09', 'Lucas R. Ferreira', '<p>FOI COMPLETADO O N&Iacute;VEL E VERIFICADO N&Iacute;VEL DE &Oacute;LEO HIDR&Aacute;ULICO E &Aacute;GUA</p>', '2018-02-15 12:06:01', 0, '2018-02-15 13:23:09', '2018-02-15 14:06:01'),
(46, 16, '215', 'BAIXO NÍVEL DE ÓLEO HIDRÁULICO', 'PORTO', '2018-02-15 16:17:40', 'Corretiva', 'Lucas R. Ferreira', NULL, '2018-02-15 12:17:40', 'Lucas R. Ferreira', '<p>FOI COMPLETADO COM 20L</p>', '2018-02-15 12:18:04', 0, '2018-02-15 14:17:40', '2018-02-15 14:18:04');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=162 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(148, '2014_10_12_000000_create_users_table', 1),
(149, '2014_10_12_100000_create_password_resets_table', 1),
(150, '2017_10_22_233345_create_entradas_table', 1),
(151, '2017_10_23_170336_create_entrada_veiculos_table', 1),
(152, '2017_10_23_232909_create_nota_entregas_table', 1),
(153, '2017_11_17_094813_create_tasks_table', 1),
(154, '2017_11_20_131239_create_acessos_table', 1),
(155, '2017_11_23_160754_create_veiculos_table', 1),
(156, '2017_11_24_162104_create_placas_table', 1),
(157, '2017_12_17_145058_create_peca_manutencaos_table', 1),
(158, '2017_12_17_145114_create_local_manutencaos_table', 1),
(159, '2017_12_18_000053_create_local_manutencao_veiculo_table', 1),
(160, '2017_12_18_000127_create_peca_manutencao_veiculo_table', 1),
(161, '2018_01_21_082745_create_manutencaos_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `nota_entregas`
--

CREATE TABLE IF NOT EXISTS `nota_entregas` (
  `id` int(10) unsigned NOT NULL,
  `qtdade` int(11) NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nf` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entrada_id` int(10) unsigned NOT NULL,
  `titulo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `autor` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isbn` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_cadastrou` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor_pvp` double(20,2) NOT NULL,
  `percent` double(10,2) NOT NULL,
  `valor_unit` double(10,2) NOT NULL,
  `valor_percent` double(10,2) NOT NULL,
  `valor_total` double(25,2) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `prestado_por` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_prestado` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `restaurado_por` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_restaurado` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `peca_manutencaos`
--

CREATE TABLE IF NOT EXISTS `peca_manutencaos` (
  `id` int(10) unsigned NOT NULL,
  `peca_manutencao` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `criado_por` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `peca_manutencaos`
--

INSERT INTO `peca_manutencaos` (`id`, `peca_manutencao`, `criado_por`, `created_at`, `updated_at`) VALUES
(1, 'CILINDRO MESTRE AVARIADO', NULL, '2018-01-26 15:34:17', '2018-01-26 15:34:17'),
(2, 'SENSOR DO PEDAL FALHANDO', NULL, '2018-01-26 15:35:04', '2018-01-26 15:35:04'),
(3, 'VAZAMENTO NO CILINDRO DO CLAMP', NULL, '2018-01-26 15:35:26', '2018-01-26 15:35:26'),
(4, 'BAIXO NÍVEL DE ÓLEO HIDRÁULICO', NULL, '2018-01-26 15:35:43', '2018-01-26 15:35:43'),
(5, 'BAIXO NÍVEL DE ÓLEO DE TRANSMISSÃO', NULL, '2018-01-26 15:36:00', '2018-01-26 15:36:00'),
(6, 'PARADA PROGRAMADA DE 500 HORAS', NULL, '2018-01-26 15:44:12', '2018-01-26 15:44:12'),
(7, 'VAZAMENTO NA MANGUEIRA DE ELEVAÇÃO', NULL, '2018-01-26 15:45:33', '2018-01-26 15:45:33'),
(8, 'VAZAMENTO NA MANGUEIRA DE COMANDO', NULL, '2018-01-26 15:45:48', '2018-01-26 15:45:48'),
(9, 'RODOGÁS ENCHARCADO', NULL, '2018-01-26 16:53:20', '2018-01-26 16:53:20'),
(10, 'VAZAMENTO NO CILINDRO DE INCLINAÇÃO', NULL, '2018-01-26 16:57:18', '2018-01-26 16:57:18'),
(11, 'BAIXO NÍVEL DE ÁGUA DO RADIADOR', NULL, '2018-01-26 16:57:36', '2018-01-26 16:57:36'),
(12, 'INSTALAÇÃO DO RADIADOR', NULL, '2018-01-26 16:57:53', '2018-01-26 16:57:53'),
(13, 'VAZAMENTO DE AGUA', NULL, '2018-01-30 16:36:02', '2018-01-30 16:36:02'),
(14, 'BATERIA DESCARREGADA', NULL, '2018-02-02 12:48:12', '2018-02-02 12:48:12'),
(15, 'VAZAMENTO DE ÓLEO NA ALAVANCA DA CABINE', NULL, '2018-02-02 12:52:10', '2018-02-02 12:52:10'),
(16, 'VAZAMENTO NA MANGUEIRA DO ORBITROL', NULL, '2018-02-02 13:03:36', '2018-02-02 13:03:36'),
(17, 'CLAMP AVARIADO', NULL, '2018-02-02 13:04:55', '2018-02-02 13:04:55'),
(18, 'FILTROS HIDRÁULICOS OBSTRUÍDOS', NULL, '2018-02-02 13:05:12', '2018-02-02 13:05:12'),
(19, 'VAZAMENTO NA MANGUEIRA  DO CILINDRO DE DIREÇÃO', NULL, '2018-02-02 13:05:28', '2018-02-02 13:05:28'),
(20, 'VÁLVULAS DE RETENÇÃO DO CLAMP OBSTRUÍDA', NULL, '2018-02-02 13:05:44', '2018-02-02 13:05:44'),
(21, 'CABO DA BATERIA FOLGADO', NULL, '2018-02-02 13:05:57', '2018-02-02 13:05:57'),
(22, 'BORNES DA BATERIA DESGASTADOS', NULL, '2018-02-02 13:06:12', '2018-02-02 13:06:12'),
(23, 'VAZAMENTO NA MANGUEIRA DE ACIONAMENTO DO CLAMP', NULL, '2018-02-02 13:06:47', '2018-02-02 13:06:47'),
(24, 'VÁLVULA DE RETENÇÃO AVARIADA', NULL, '2018-02-02 13:07:15', '2018-02-02 13:07:15'),
(25, 'PNEU FURADO', NULL, '2018-02-02 13:07:27', '2018-02-02 13:07:27'),
(26, 'FAROL DIANTEIRO AVARIADO', NULL, '2018-02-02 13:07:38', '2018-02-02 13:07:38'),
(27, 'MOTOR DE PARTIDA AVARIADO', NULL, '2018-02-05 13:16:59', '2018-02-05 13:16:59'),
(28, 'ROLAMENTO DO CARRO DE CARGA AVARIADO', NULL, '2018-02-05 13:20:18', '2018-02-05 13:20:18'),
(29, 'VAZAMENTO NA BOMBA HIDRÁULICA', NULL, '2018-02-08 13:44:17', '2018-02-08 13:44:17'),
(30, 'VAZAMENTO NA MANGUEIRA DO CILINDRO DE INCLINAÇÃO', NULL, '2018-02-08 13:56:36', '2018-02-08 13:56:36'),
(31, 'BAIXO NÍVEL DE ÓLEO DE MOTOR', NULL, '2018-02-15 12:55:59', '2018-02-15 12:55:59');

-- --------------------------------------------------------

--
-- Estrutura da tabela `peca_manutencao_veiculo`
--

CREATE TABLE IF NOT EXISTS `peca_manutencao_veiculo` (
  `id` int(10) unsigned NOT NULL,
  `veiculo_id` int(10) unsigned NOT NULL,
  `peca_manutencao_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `placas`
--

CREATE TABLE IF NOT EXISTS `placas` (
  `id` int(10) unsigned NOT NULL,
  `placa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marca` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modelo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ano_fabricacao` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `combustivel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `entrada_id` int(10) unsigned NOT NULL,
  `tarefa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observacao` text COLLATE utf8mb4_unicode_ci,
  `responsavel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data_estimada` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_conclusao` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `senha` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `nivel` int(11) NOT NULL,
  `send_mail` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nascimento` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `senha`, `telefone`, `status`, `nivel`, `send_mail`, `image`, `nascimento`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Erick Raposo', 'azafilmes@gmail.com', 'ericklive', '$2y$10$NrIZ.8NR90S4Qfg14oerVehFzremXwQs1UiAleewxDx1KG/8aiBM6', '123456S', '(98) 99195-6153', 1, 1, 0, NULL, NULL, 'Fc1swfE6pg9rAqTOD0ck15avjbJYRLe3quQ9Hm1iviR2pXBA3aJXZqOvnWjQ', NULL, NULL),
(2, 'Lucas R. Ferreira', 'lucasrferreira94@gmail.com', 'LRF', '$2y$10$T.lpUbBR.lpAxDurWttMgOO/GWWbK2puC.m7KXBIsrLG/htdUOYD6', 'c15861', '(98) 98279-0870', 1, 3, 0, NULL, '1998-01-01', 'C3ldYv4rGJQp2gBNO1LYiMvD3UCvtRkOXzSzBsbP9fkeRpwif8DfHNkRplKA', NULL, NULL),
(3, 'WENER SANTOS', 'wener.santos@hotmail.com', 'WMTS', '$2y$10$l4BRX25fld7hhYJ2z8cSL.Dxt5fIyorKCT2TUkFvYnpY84/xkl/Wy', 'ceacad', '(98) 98291-0276', 1, 2, 0, NULL, '1974-10-01', 'r4ZaHL56YpalUKaVQOFaEdK2nR2NAM6rRw8HAt3AlsvyamDFWUlRCSvMNPBD', NULL, NULL),
(4, 'Thiago', 'thiago.santos@g5sl.com.br', 'TFS', '$2y$10$eiL87iU7hNw7piBPrVivDe6egm5eEjb5ikj9.6gCM59lfKkr1vu8S', '9a0400', '(98) 99116-4226', 1, 2, 0, NULL, '1993-01-01', '3emEtZR9GK', NULL, NULL),
(5, 'ADEILTON', 'adeilton.almeida@g5sl.com.br', 'ADE', '$2y$10$WhwbTLO/xAuZwDI5odiqPuxAUJXiPhLB6KbfwWOy795xLGFpaowr.', '5dd3ec', '(98) 99175-5149', 1, 3, 0, NULL, '1981-10-01', '6wzVqUfwzP', NULL, NULL),
(7, 'Jordan Willian', 'jordanwillian.price@gmail.com', 'jordanwill', '$2y$10$BEfIwuw0s/2qqpz2eLvMr.2T4J.biUiY5EOUlWR/mNj0Txb1/JxHO', 'd35aca', '(98) 99129-6681', 1, 2, 1, NULL, '1995-03-06', NULL, '2018-02-15 16:32:00', '2018-02-15 16:32:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `veiculos`
--

CREATE TABLE IF NOT EXISTS `veiculos` (
  `id` int(10) unsigned NOT NULL,
  `entrada_veiculo_id` int(10) unsigned NOT NULL,
  `os_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `falhas` text COLLATE utf8mb4_unicode_ci,
  `tempo_estimado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `concedido_acesso_por` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observacao` text COLLATE utf8mb4_unicode_ci,
  `date_concedido_acesso` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `concluido_acesso_por` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comentarios` text COLLATE utf8mb4_unicode_ci,
  `date_concluido_acesso` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acessos`
--
ALTER TABLE `acessos`
  ADD PRIMARY KEY (`id`), ADD KEY `acessos_entrada_id_foreign` (`entrada_id`);

--
-- Indexes for table `entradas`
--
ALTER TABLE `entradas`
  ADD PRIMARY KEY (`id`), ADD KEY `entradas_user_id_foreign` (`user_id`);

--
-- Indexes for table `entrada_veiculos`
--
ALTER TABLE `entrada_veiculos`
  ADD PRIMARY KEY (`id`), ADD KEY `entrada_veiculos_user_id_foreign` (`user_id`);

--
-- Indexes for table `local_manutencaos`
--
ALTER TABLE `local_manutencaos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `local_manutencao_veiculo`
--
ALTER TABLE `local_manutencao_veiculo`
  ADD PRIMARY KEY (`id`), ADD KEY `local_manutencao_veiculo_veiculo_id_foreign` (`veiculo_id`), ADD KEY `local_manutencao_veiculo_local_manutencao_id_foreign` (`local_manutencao_id`);

--
-- Indexes for table `manutencaos`
--
ALTER TABLE `manutencaos`
  ADD PRIMARY KEY (`id`), ADD KEY `manutencaos_entrada_veiculo_id_foreign` (`entrada_veiculo_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nota_entregas`
--
ALTER TABLE `nota_entregas`
  ADD PRIMARY KEY (`id`), ADD KEY `nota_entregas_entrada_id_foreign` (`entrada_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `peca_manutencaos`
--
ALTER TABLE `peca_manutencaos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peca_manutencao_veiculo`
--
ALTER TABLE `peca_manutencao_veiculo`
  ADD PRIMARY KEY (`id`), ADD KEY `peca_manutencao_veiculo_veiculo_id_foreign` (`veiculo_id`), ADD KEY `peca_manutencao_veiculo_peca_manutencao_id_foreign` (`peca_manutencao_id`);

--
-- Indexes for table `placas`
--
ALTER TABLE `placas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`), ADD KEY `tasks_user_id_foreign` (`user_id`), ADD KEY `tasks_entrada_id_foreign` (`entrada_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_name_unique` (`name`), ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- Indexes for table `veiculos`
--
ALTER TABLE `veiculos`
  ADD PRIMARY KEY (`id`), ADD KEY `veiculos_entrada_veiculo_id_foreign` (`entrada_veiculo_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acessos`
--
ALTER TABLE `acessos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `entradas`
--
ALTER TABLE `entradas`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `entrada_veiculos`
--
ALTER TABLE `entrada_veiculos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `local_manutencaos`
--
ALTER TABLE `local_manutencaos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `local_manutencao_veiculo`
--
ALTER TABLE `local_manutencao_veiculo`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `manutencaos`
--
ALTER TABLE `manutencaos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=162;
--
-- AUTO_INCREMENT for table `nota_entregas`
--
ALTER TABLE `nota_entregas`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `peca_manutencaos`
--
ALTER TABLE `peca_manutencaos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `peca_manutencao_veiculo`
--
ALTER TABLE `peca_manutencao_veiculo`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `placas`
--
ALTER TABLE `placas`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `veiculos`
--
ALTER TABLE `veiculos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `acessos`
--
ALTER TABLE `acessos`
ADD CONSTRAINT `acessos_entrada_id_foreign` FOREIGN KEY (`entrada_id`) REFERENCES `entradas` (`id`);

--
-- Limitadores para a tabela `entradas`
--
ALTER TABLE `entradas`
ADD CONSTRAINT `entradas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `entrada_veiculos`
--
ALTER TABLE `entrada_veiculos`
ADD CONSTRAINT `entrada_veiculos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `local_manutencao_veiculo`
--
ALTER TABLE `local_manutencao_veiculo`
ADD CONSTRAINT `local_manutencao_veiculo_local_manutencao_id_foreign` FOREIGN KEY (`local_manutencao_id`) REFERENCES `local_manutencaos` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `local_manutencao_veiculo_veiculo_id_foreign` FOREIGN KEY (`veiculo_id`) REFERENCES `veiculos` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `manutencaos`
--
ALTER TABLE `manutencaos`
ADD CONSTRAINT `manutencaos_entrada_veiculo_id_foreign` FOREIGN KEY (`entrada_veiculo_id`) REFERENCES `entrada_veiculos` (`id`);

--
-- Limitadores para a tabela `nota_entregas`
--
ALTER TABLE `nota_entregas`
ADD CONSTRAINT `nota_entregas_entrada_id_foreign` FOREIGN KEY (`entrada_id`) REFERENCES `entradas` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `peca_manutencao_veiculo`
--
ALTER TABLE `peca_manutencao_veiculo`
ADD CONSTRAINT `peca_manutencao_veiculo_peca_manutencao_id_foreign` FOREIGN KEY (`peca_manutencao_id`) REFERENCES `peca_manutencaos` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `peca_manutencao_veiculo_veiculo_id_foreign` FOREIGN KEY (`veiculo_id`) REFERENCES `veiculos` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `tasks`
--
ALTER TABLE `tasks`
ADD CONSTRAINT `tasks_entrada_id_foreign` FOREIGN KEY (`entrada_id`) REFERENCES `entradas` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `tasks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `veiculos`
--
ALTER TABLE `veiculos`
ADD CONSTRAINT `veiculos_entrada_veiculo_id_foreign` FOREIGN KEY (`entrada_veiculo_id`) REFERENCES `entrada_veiculos` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
