<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::patch('/manutencao/deletar/{id}', 'Painel\ManutencaoController@deletaManutencaoConcluida');
Route::patch('/manutencao/{id}/update', 'Painel\ManutencaoController@update');

Route::resource('/manutencao', 'Painel\ManutencaoController');

Route::get('/manutencao/editar/{id}', 'Painel\ManutencaoController@edit');




/*****************************************************************
        ROTAS ALMOXARIFADO
/*****************************************************************/

Route::resource('/almoxarifado/salvar', 'Painel\AlmoxarifadoController');

Route::get('/almoxarifado', 'Painel\AlmoxarifadoController@index');
Route::get('/almoxarifado/create', 'Painel\AlmoxarifadoController@create');
Route::get('/almoxarifado/{id}/edit', 'Painel\AlmoxarifadoController@edit');
Route::patch('/almoxarifado/{id}/update', 'Painel\AlmoxarifadoController@update');
Route::delete('/almoxarifado/{id}/destroy', 'Painel\AlmoxarifadoController@destroy');

/******************************************************************
* Rotas Painel
******************************************************************/
Route::group(['prefix' => 'painel', 'middleware' => 'auth'], function () {


  // Route::resource('nota-entrega','Painel\NotaEntregaController', ['names' => [
  //     'index' => 'nota-entrega.index',
  //     'show' => 'ver.nota'
  //     ]]);

  // Route::get('modal/manutencao', function{
  //   return view();
  // });
  Route::any('usuarios/pesquisar', 'Painel\UserController@search')->name('usuarios.search');

  Route::post('pecas', 'Painel\PecaManutencaoController@store')->name('pecas.store');
  Route::get('peca', 'Painel\PecaManutencaoController@index')->name('peca');
  Route::post('local-manutencao', 'Painel\LocalManutencaoController@store')->name('local-manutencao.store');
  Route::get('local', 'Painel\LocalManutencaoController@index')->name('local');

  Route::resource('usuarios', 'Painel\UserController');

  Route::get('nota-entrega/{id}','Painel\NotaEntregaController@show')->name('ver.nota');
  Route::get('nota-entrega','Painel\NotaEntregaController@index')->name('nota-entrega.index');
  Route::delete('nota-entrega/{id}/excluir','Painel\NotaEntregaController@destroy')->name('nota-entrega.destroy');
  Route::get('nota-entrega/{id}/restaurar','Painel\NotaEntregaController@restore')->name('nota-entrega.restore');

  // Route::get('criar-nota-entrega/{id}/criar', 'Painel\NotaEntregaController@criar')->name('nota-entrega.criar.nota');
  Route::get('criar-nota-entrega/{id}/criar', 'Painel\NotaEntregaController@criar')->name('criar.nota');
  Route::post('criar-nota/{id}/gravar', 'Painel\NotaEntregaController@gravar')->name('gravar.nota');
  Route::get('nota-entrega/{id}/editar', 'Painel\NotaEntregaController@editar')->name('editar.nota');
  Route::put('nota-entrega/{id}/atualizar', 'Painel\NotaEntregaController@atualizar')->name('atualizar.nota');
  Route::any('notas/pesquisar', 'Painel\NotaEntregaController@search')->name('nota-entrega.search');


  // Route::get('criar-nota-entrega/{id}/criar', 'Painel\NotaEntregaController@criar')->name('nota-entrega.criar.nota');
  Route::get('acessos', 'Painel\AcessoController@index')->name('acessos.index');
  Route::get('criar-acesso/{id}/criar', 'Painel\AcessoController@criar')->name('acessos.create');
  Route::post('acesso/{id}/gravar', 'Painel\AcessoController@store')->name('acessos.store');
  Route::delete('acesso/{id}/concluir', 'Painel\AcessoController@destroy')->name('acessos.destroy');
  // Route::get('criar-acesso/{id}/editar', 'Painel\AcessoController@editar')->name('editar.acesso');
  Route::put('acesso/{id}/atualizar', 'Painel\AcessoController@atualizar')->name('acessos.update');
  // Route::any('notas/pesquisar', 'Painel\NotaEntregaController@search')->name('nota-entrega.search');





  // Route::get('criar-nota-entrega/{id}/criar', 'Painel\NotaEntregaController@criar')->name('nota-entrega.criar.nota');



  Route::get('manutenção-veiculos', 'Painel\ManutencaoController@index')
      ->name('manutencao.veiculos.index');

  Route::get('manutenção-veiculo/{id}/criar', 'Painel\ManutencaoController@criar')
      ->name('manutencao.veiculos.create');

  Route::post('manutenção-veiculo/{id}/gravar', 'Painel\ManutencaoController@store')
      ->name('manutencao.veiculos.store');

  Route::delete('manutenção-veiculo/{id}/destroy', 'Painel\ManutencaoController@destroy')
      ->name('manutencao.veiculos.destroy');



  // Route::get('criar-manutencao/{id}/editar', 'Painel\ManutencaoController@editar')->name('editar.manutencao');
  Route::put('manutenção-veiculo/{id}/atualizar', 'Painel\ManutencaoController@update') //antes estava atualizar
      ->name('manutencao.veiculos.update');
  // Route::any('notas/pesquisar', 'Painel\NotaEntregaController@search')->name('nota-entrega.search');

  // Route::get('criar-nota-entrega/{id}/criar', 'Painel\NotaEntregaController@criar')->name('nota-entrega.criar.nota');
  Route::get('manutenção-old-veiculos', 'Painel\AcessoVeiculoController@index')->name('acesso.veiculos.index');
  Route::get('manutenção-old-veiculo/{id}/criar', 'Painel\AcessoVeiculoController@criar')->name('acesso.veiculos.create');
  Route::post('manutenção-old-veiculo/{id}/gravar', 'Painel\AcessoVeiculoController@store')->name('acesso.veiculos.store');
  Route::delete('manutenção-old-veiculo/{id}/concluir', 'Painel\AcessoVeiculoController@destroy')->name('acesso.veiculos.destroy');
  // Route::get('criar-acesso/{id}/editar', 'Painel\AcessoVeiculoController@editar')->name('editar.acesso');
  Route::put('manutenção-old-veiculo/{id}/atualizar', 'Painel\AcessoVeiculoController@atualizar')->name('acesso.veiculos.update');
  // Route::any('notas/pesquisar', 'Painel\NotaEntregaController@search')->name('nota-entrega.search');


  Route::get('criar-tarefa/{id}/criar', 'Painel\NotaEntregaController@criar')->name('criar.nota');
  Route::post('criar-tarefa/{id}/gravar', 'Painel\NotaEntregaController@gravar')->name('gravar.nota');
  Route::get('tarefa/{id}/editar', 'Painel\NotaEntregaController@editar')->name('editar.nota');
  Route::put('tarefa/{id}/atualizar', 'Painel\NotaEntregaController@atualizar')->name('atualizar.nota');

  Route::any('clientes/pesquisar', 'Painel\EntradaController@search')->name('entradas.search');

  Route::resource('visitantes','Painel\EntradaController', ['names' => [
    'index'   => 'entradas.index',
    'create'  => 'entradas.create',
    'store'   => 'entradas.store',
    'show'    => 'entradas.show',
    'edit'    => 'entradas.edit',
    'update'  => 'entradas.update',
    'destroy' => 'entradas.destroy',
    ]]);

  Route::resource('veiculos','Painel\EntradaVeiculoController', ['names' => [
    'index'   => 'entradas.veiculo.index',
    'create'  => 'entradas.veiculo.create',
    'store'   => 'entradas.veiculo.store',
    'show'    => 'entradas.veiculo.show',
    'edit'    => 'entradas.veiculo.edit',
    'update'  => 'entradas.veiculo.update',
    'destroy' => 'entradas.veiculo.destroy',
    ]]);

  Route::resource('caminhoes','Painel\EntradaCaminhaoController', ['names' => [
    'index'   => 'entradas.caminhao.index',
    'create'  => 'entradas.caminhao.create',
    'store'   => 'entradas.caminhao.store',
    'show'    => 'entradas.caminhao.show',
    'edit'    => 'entradas.caminhao.edit',
    'update'  => 'entradas.caminhao.update',
    'destroy' => 'entradas.caminhao.destroy',
    ]]);

    Route::any('tarefas/pesquisar', 'Painel\TaskController@search')->name('tasks.search');
    Route::resource('tarefas','Painel\TaskController', ['names' => [
      'index'   => 'tarefas.index',
      'create'  => 'tarefas.create',
      'store'   => 'tarefas.store',
      'show'    => 'tarefas.show',
      'edit'    => 'tarefas.edit',
      'update'  => 'tarefas.update',
      'destroy' => 'tarefas.destroy',
      ]]);




    });



    Route::resource('placas', 'Painel\PlacaController');

    /******************************************************************
    * Rotas de Autenticação
    ******************************************************************/

    // Auth::routes();
    // Authentication Routes...
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');
    //
    // // Registration Routes...
    // Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    // Route::post('register', 'Auth\RegisterController@register');


    // Password Reset Routes...
    Route::post('password/email', [
      'as' => 'password.email',
      'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
    ]);
    Route::get('password/reset', [
      'as' => 'password.request',
      'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
    ]);
    Route::post('password/reset', [
      'as' => '',
      'uses' => 'Auth\ResetPasswordController@reset'
    ]);
    Route::get('password/reset/{token}', [
      'as' => 'password.reset',
      'uses' => 'Auth\ResetPasswordController@showResetForm'
    ]);


    /******************************************************************
    * Rotas de Autenticação
    Auth::routes();
    ******************************************************************/





    Route::get('/', 'Painel\PainelController@indexVeiculos')->name('/');
    Route::get('/painel', 'Painel\PainelController@indexVeiculos')->name('/painel');
    // Route::get('/', 'Painel\PainelController@index')->name('/');
    // Route::get('/painel', 'Painel\PainelController@index')->name('/painel');
    Route::get('/painel-veiculos', 'Painel\PainelController@indexVeiculos')
        ->name('/painelVeiculos');


    Route::get('/home', 'Painel\PainelController@indexVeiculos')->name('home');


    // Route::get('stock/add','Painel\StockController@create');
    // Route::post('stock/add','Painel\StockController@store');

    Route::get('falha-mais-frequente','Painel\StockController@index')->name('dash');
    Route::get('veiculo-maior-falha','Painel\StockController@index2')->name('dash2');
    Route::get('dash/chart','Painel\StockController@chart')->name('dash.chart');
    Route::get('dash/chart2','Painel\StockController@chart2')->name('dash.chart2');

    Route::post('range', 'Painel\StockController@rangem')->name('range');

    Route::any('chart/range', 'Painel\StockController@chartRangem')->name('chart.range');
    Route::post('chart/range/veiculos', 'Painel\StockController@chartRangem2')->name('chart.range2');
    // Route::get('json/range', 'Painel\StockController@jsonRange')->name('json.range');


    /*================================
    =            Back-End            =
    ================================*/

    /*----------  Login  ----------*/

    Route::get('admin/login','Admin\LoginController@login');
    Route::get('admin/login/logout','Admin\LoginController@logout');
    Route::post('admin/login/check','Admin\LoginController@check');

    /*----------  Dashboard  ----------*/

    Route::get('admin/index','Admin\IndexController@index');
    Route::get('admin/index2','Admin\IndexController@index2');
    Route::get('admin/index3','Admin\IndexController@index3');
    Route::get('admin/index4','Admin\IndexController@index4');

    /*----------  Graph  ----------*/

    Route::get('admin/graph_chartjs','Admin\GraphController@graph_chartjs');
    Route::get('admin/graph_flot','Admin\GraphController@graph_flot');
    Route::get('admin/graph_morris','Admin\GraphController@graph_morris');
    Route::get('admin/graph_peity','Admin\GraphController@graph_peity');
    Route::get('admin/graph_rickshaw','Admin\GraphController@graph_rickshaw');
    Route::get('admin/graph_sparkline','Admin\GraphController@graph_sparkline');

    /*----------  Mailbox  ----------*/

    Route::get('admin/mailbox','Admin\EmailController@mailbox');
    Route::get('admin/mail_detail','Admin\EmailController@mail_detail');
    Route::get('admin/mail_compose','Admin\EmailController@mail_compose');
    Route::get('admin/email_template','Admin\EmailController@email_template');

    /*----------  Widgets  ----------*/

    Route::get('admin/widgets','Admin\WidgetController@widgets');

    /*----------  Forms  ----------*/

    Route::get('admin/form_advanced','Admin\FormController@form_advanced');
    Route::get('admin/form_basic','Admin\FormController@form_basic');
    Route::get('admin/form_editors','Admin\FormController@form_editors');
    Route::get('admin/form_file_upload','Admin\FormController@form_file_upload');
    Route::get('admin/form_insert','Admin\FormController@form_insert');
    Route::get('admin/form_wizard','Admin\FormController@form_wizard');

    /*----------  App Views  ----------*/

    Route::get('admin/contacts','Admin\AppController@contacts');
    Route::get('admin/projects','Admin\AppController@projects');
    Route::get('admin/project_detail','Admin\AppController@project_detail');
    Route::get('admin/file_manager','Admin\AppController@file_manager');
    Route::get('admin/calendar','Admin\AppController@calendar');
    Route::get('admin/timeline','Admin\AppController@timeline');
    Route::get('admin/pin_board','Admin\AppController@pin_board');

    /*----------  Other Pages  ----------*/

    Route::get('admin/search_results','Admin\OtherController@search_results');
    Route::get('admin/invoice','Admin\OtherController@invoice');
    // Route::get('admin/login','Admin\OtherController@login');
    Route::get('admin/login_two_columns','Admin\OtherController@login_two_columns');
    Route::get('admin/register','Admin\OtherController@register');
    Route::post('admin/register/check/username','Admin\OtherController@check_username');
    Route::get('admin/error404','Admin\OtherController@error404');
    Route::get('admin/error500','Admin\OtherController@error500');
    Route::get('admin/empty_page','Admin\OtherController@empty_page');

    /*----------  Miscellaneous  ----------*/

    Route::get('admin/toastr_notifications','Admin\MiscellaneousController@toastr_notifications');
    Route::get('admin/nestable_list','Admin\MiscellaneousController@nestable_list');
    Route::get('admin/forum_main','Admin\MiscellaneousController@forum_main');
    Route::get('admin/google_maps','Admin\MiscellaneousController@google_maps');
    Route::get('admin/code_editor','Admin\MiscellaneousController@code_editor');
    Route::get('admin/modal_window','Admin\MiscellaneousController@modal_window');
    Route::get('admin/validation','Admin\MiscellaneousController@validation');
    Route::get('admin/tree_view','Admin\MiscellaneousController@tree_view');
    Route::get('admin/chat_view','Admin\MiscellaneousController@chat_view');

    /*----------  UI Elements  ----------*/

    Route::get('admin/typography','Admin\UIController@typography');
    Route::get('admin/icons','Admin\UIController@icons');
    Route::get('admin/draggable_panels','Admin\UIController@draggable_panels');
    Route::get('admin/buttons','Admin\UIController@buttons');
    Route::get('admin/video','Admin\UIController@video');
    Route::get('admin/tabs_panels','Admin\UIController@tabs_panels');
    Route::get('admin/notifications','Admin\UIController@notifications');
    Route::get('admin/badges_labels','Admin\UIController@badges_labels');

    /*----------  Grid options  ----------*/

    Route::get('admin/grid_options','Admin\GridController@grid_options');

    /*----------  Tables  ----------*/

    Route::get('admin/table_basic','Admin\TableController@table_basic');
    Route::get('admin/table_data_tables','Admin\TableController@table_data_tables');
    Route::get('admin/jq_grid','Admin\TableController@jq_grid'); /********************/

    /*----------  Gallery  ----------*/

    Route::get('admin/basic_gallery','Admin\GalleryController@basic_gallery');
    Route::get('admin/carousel','Admin\GalleryController@carousel');

    /*----------  CSS Animations  ----------*/

    Route::get('admin/css_animation','Admin\CSSController@css_animation');

    /*----------  Landing Page  ----------*/

    Route::get('admin/css_animation','Admin\CSSController@css_animation');

    /*----------  Package  ----------*/

    Route::get('admin/package','Admin\PackageController@package');

    /*----------  Profile  ----------*/

    Route::get('admin/profile','Admin\ProfileController@profile');
    Route::get('admin/profile/edit','Admin\ProfileController@edit_profile');
    Route::get('admin/lockscreen','Admin\ProfileController@lockscreen');
    Route::post('admin/edit/password','Admin\ProfileController@change_password');
    Route::get('admin/mail','Admin\ProfileController@mail');

    /*=====  End of Back-End  ======*/
