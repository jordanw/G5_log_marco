@extends('layouts.layout.template')
@push('ck-editor')
  <script src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
@endpush
@section('content')
  @include('layouts.breadcrumb-painel')

  <div class="container">
    <!-- Button trigger modal -->


  </div>

  <div class="row">
    @include('layouts.status-delete-msg')
    <div class="col-lg-4">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <span class="label label-warning pull-right ty10"><i class="fa fa-external-link-square fa-fw fa-3x"></i></span>
          <h5>Veículos em Manutenção</h5>
        </div>
        <div class="ibox-content">
          <h1 class="no-margins">{{ $veiculosManutAtiva }}</h1>

          {{-- <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div> --}}
          <small>N<sup>o</sup>  </small>
        </div>
      </div>
    </div>

    <div class="col-lg-2">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <span class="label label-success pull-right ty10"><i class="fa fa-car fa-fw fa-3x"></i></span>
          <h5></h5>
        </div>
        <div class="ibox-content">
          <h1 class="no-margins">{{ $empCadastrados }}</h1>
          <small>Empilhadeiras</small>
        </div>

        {{-- <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div> --}}
      </div>
    </div>
    <div class="col-lg-2">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <span class="label label-warning pull-right ty10"><i class="fa fa-truck fa-fw fa-3x"></i></span>
          <h5></h5>
        </div>
        <div class="ibox-content">
          <h1 class="no-margins">{{ $camCadastrados }}</h1>
          <small>Caminhões</small>
        </div>

        {{-- <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div> --}}
      </div>
    </div>

    <div class="col-lg-4">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <span class="label label-danger pull-right ty10"><i class="fa fa-check fa-fw fa-3x"></i></span>
          <h5>Manutenções Concluída</h5>
        </div>
        <div class="ibox-content">
          <h1 class="no-margins">{{ $veiculosManConcluida }}</h1>
          {{-- <div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div> --}}
          <small>N<sup>o</sup></small>
        </div>
      </div>
    </div>

    <div class="col-lg-4">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <span class="label label-danger pull-right ty10"><i class="fa fa-flash fa-fw fa-3x"></i></span>
          <h5>Tempo Total de Parada</h5>
        </div>
        <div class="ibox-content">
          <h1 class="no-margins">00</h1>
          {{-- <div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div> --}}
          <small>Horas<sup>o</sup></small>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <a href="{{route('dash2')}}"><span class="label label-primary pull-right ty10"><i class="fa fa-globe fa-fw fa-3x"></i></span></a>
          <h5><a href="{{route('dash2')}}">Veículos Falha</a></h5>
        </div>
        <div class="ibox-content">
          @foreach ($veiculosFalhaCount as $veiculosFalha)
            <h4 class="no-margins">{{$veiculosFalha->placa}} &nbsp&nbsp | &nbsp&nbsp {{$veiculosFalha->quantFalhas}} </h4>
          @endforeach
          {{-- <div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i></div> --}}
          <small>Placa | N<sup>o</sup></small>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <a href="{{route('dash')}}"><span class="label label-warning pull-right ty10"></h5><i class="fa fa-bullseye fa-fw fa-3x"></i></span></a>
          <h5><a href="{{route('dash')}}">Falha mais Frequente</a></h5>
        </div>
        <div class="ibox-content">
          <h3 class="no-margins">{{$falhaMaisFrequente}}</h3>
          {{-- <div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i></div> --}}
          <small>N<sup>o</sup> <b>{{$falhaMaisFrequente_count}}</b></small>
        </div>
      </div>
    </div>
  </div>


  <div class="col-md-12">
    @if( Session::has('success') )
      <div class="alert alert-success hide-msg" style="float: left; width: 100%; margin: 10px 0px;">
        {{Session::get('success')}}
      </div>
    @endif
  </div>

  @include('layouts.cadastrar-veiculos')
  @include('painel.entradasVeiculos.formManutencao')
</div>



<div class="row">
  @include('painel.entradasVeiculos.formIndex')
</div>

@stop


@push('stack-js')


  <script type="text/javascript">
  $("textarea").each(function(){
    CKEDITOR.replace( this );
  });

  </script>


  @include('layouts.paginacaoPontos')

@endpush
