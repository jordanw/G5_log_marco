<?php
use Carbon\Carbon;
?>
@include('painel.acessoVeiculos.form.modal-local_manutencao')
@include('painel.acessoVeiculos.form.modal-peca_manutencao')
@if( isset($editar) )
  {!! Form::model($user, ['route' => ['acesso.veiculos.update', $user->id], 'class' => 'form form-search form-ds', 'files' => true, 'method' => 'PUT']) !!}
  <input type="hidden"  value="{{$disabled = 1}}"/>

@else
  <input type="hidden"  value="{{$disabled = 0}}"/>
  {!! Form::open(['route' => ['acesso.veiculos.store', $user->id],  'class' => 'form form-search form-ds', 'files' => true]) !!}
@endif

<div class="col-xs-12 col-sm-12">
  <div class="row">

    <div class="form-group">

      <div class="row">
        <div class="col-sm-4">
          <div class="row">
            <div class="col-sm-12">
              {!! Form::label("os_num","Os_Num:") !!}
              {!! Form::text("os_num",null,["class"=>"form-control"]) !!}
            </div>
            <div class="col-sm-12">
              {!! Form::label("tipo_manutencao","Tipo Manutenção:") !!}
              {!! Form::select('tipo_manutencao', [
                "Preventiva"=>"Preventiva",
                "Corretiva"=>"Corretiva",
              ], null, ['placeholder' => 'Selecione...',"class"=>"form-control"]) !!}
            </div>

            <div class="col-sm-12">
              {!! Form::label("peca_manutencao","Falha Ocorrida:") !!}
              <div class="input-group">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" data-toggle="modal" data-target="#myModal">  <i class="fa fa-plus fa-lg"></i></button>
                </span>
                {!! Form::select('peca_manutencao', $peca, null, ['placeholder' => 'Selecione...',"class"=>"form-control", "onchange"=>"setTextField(this)"]) !!}
              </div><!-- /input-group -->
            </div>

            {!! Form::hidden('falhas', '',["onchange"=>"setTextField(this)","id"=>"falhas"]) !!}
            <script type="text/javascript">
            function setTextField(ddl) {
              document.getElementById('falhas').value = ddl.options[ddl.selectedIndex].text;
            }
          </script>





          <div class="col-sm-12">
            {!! Form::label("local_manutencao","Local Manutenção:") !!}
            <div class="input-group">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button" data-toggle="modal" data-target="#myModal2">  <i class="fa fa-plus fa-lg"></i></button>
              </span>
              {!! Form::select('local_manutencao', $local, null, ['placeholder' => 'Selecione...',"class"=>"form-control"]) !!}
            </div><!-- /input-group -->
          </div>

          <div class="col-sm-12">
            {!! Form::label("tempo_estimado","Tempo Estimado:") !!}
            {!! Form::select('tempo_estimado', [
              "1"=>"até 1 Dia",
              "10"=>"até 10 Dias",
              "30"=>"até 30 Dias",
              "60"=>"até 60 Dias",
              "NS"=>"não sabe / não informou",
            ], null, ['placeholder' => 'Selecione...',"class"=>"form-control"]) !!}
          </div>
        </div>
      </div>

      <div class="col-sm-8">
        <div class="row">
          <div class="col-sm-12">
            {!! Form::label("observacao","Observação:") !!}
            {!! Form::textarea("observacao",null,["class"=>"form-control", 'id'=>'editor1']) !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-sm-12 form-group">
  {{Form::submit('Salvar',['class'=>'btn btn-primary','style'=>'font-weight:bold; margin-top:3%;'])}}
</div>

</div>

{!! Form::close() !!}



{{-- @include('layouts.mascaras') --}}
