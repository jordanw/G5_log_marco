<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Adicionar Item</h4>
      </div>
      {!! Form::open(['method' => 'POST', 'route' => 'pecas.store', 'name'=>'form-3']) !!}
      <div class="modal-body">
        {!! Form::label("peca_manutencao","Falha Ocorrida:") !!}
        {!! Form::text("peca_manutencao",null,["class"=>"form-control"]) !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
        <button type="submit" class="btn btn-primary" name="form-3">Salvar</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
