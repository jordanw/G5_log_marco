<!-- Modal -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Adicionar Item</h4>
      </div>
      {!! Form::open(['method' => 'POST', 'route' => 'local-manutencao.store', 'name'=>'form-2']) !!}
      <div class="modal-body">
        {!! Form::label("local_manutencao","Local:") !!}
        {!! Form::text("local_manutencao",null,["class"=>"form-control"]) !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
        <button type="submit" class="btn btn-primary" name="form-2">Salvar</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
