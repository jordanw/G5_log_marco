@extends('layouts.layout.template')

@section('content')


  <div class="row">
    <div class="col-md-6 text-left">
      <h4 class="breadcrumb-title">{{$user->nome or 'Nova Tarefa'}}</h4>
    </div>
    <div class="col-md-6">
      <ol class="breadcrumb hidden-xs text-right mt1">
        <li><a href="{{url('/painel')}}">Home</a></li>
        <li><a href="{{url('/painel/escrtitores')}}">Tarefa</a></li>
        <li class="active"><a href="#">{{$user->nome or 'Nova Tarefa'}}</a></li>
      </ol>
    </div>
  </div>

  {{-- <div class="container">
  @if( isset($user) )
  @include('layouts.userImageCard')
@endif
</div> --}}
<div class="col-md-12">

  @include('painel.task.form')

</div>

@endsection

@push('stack-css')
  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="{{asset('assets/js/plugins/prism/prism.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{asset('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{asset('assets/js/plugins/chartist-js/chartist.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{asset('assets/js/plugins/dropify/css/dropify.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
@endpush

@push('stack-js')


  <script type="text/javascript" src="{{asset('/assets/js/jquery.maskedinput.min.js')}}"></script>
  <script type="text/javascript">
  jQuery(function($){

    $("#cpf").mask("999.999.999-99");

    $("#cnpj").mask("99.999.999/9999-99");
    $("#datetimepicker").mask("99/99/9999");

  });
  </script>
  <script type="text/javascript">


  function mascara(t, mask){
    var i = t.value.length;
    var saida = mask.substring(1,0);
    var texto = mask.substring(i)
    if (texto.substring(0,1) != saida){
      t.value += texto.substring(0,1);
    }
  }

  $( function() {
    $( "#datetimepicker" ).datepicker({
        format: 'dd/mm/yyyy',
        language: 'pt-BR'
      });
  } );




  </script>

@endpush
