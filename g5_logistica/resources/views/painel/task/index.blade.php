@extends('layouts.layout.template')
@section('title', 'Usuários')

@section('content')

  <!-- Search for small screen -->

  @include('layouts.breadcrumb-nota-entrega')



  <div class="section">
    @include('layouts.success')
    @include('layouts.session')
  </div>

  <div class="col-md-6 text-left">
    <a href="{{route('tarefas.create')}}" class="btn btn-primary">
      <span class="glyphicon glyphicon-plus"></span>
      Cadastrar
    </a>
  </div>


<div class="col-md-12">
  @if( Session::has('success') )
    <div class="alert alert-success hide-msg" style="float: left; width: 100%; margin: 10px 0px;">
      {{Session::get('success')}}
    </div>
  @endif
</div>
<div class="col-md-12" style="margin-top:10px;">
  <div class="panel panel-default">
    <div class="table-responsive">
      <table class="table table-striped table-hover table-responsive" id="myTable">
        <thead>
          <tr>
            <th>Cod</th>
            <th>Entrada</th>
            <th>Qtdade</th>
            <th>Escritor / Entidade</th>
            <th>Titulo</th>
            <th>Autor</th>
            <th>ISBN</th>
            <th>%</th>
            <th>AMEI</th>
            <th>PVP</th>
            <th>Valor Total</th>
            <th>Data | Hora</th>
            <th class="text-center" class="center" style="width:3%">Ações</th>
          </tr>
        </thead>

        <tbody>
          @forelse($data as $user)
            <tr>
              <td>{{$user->id}}</td>
              <td style="text-align: center; vertical-align: middle">
                <a href="{{route('criar.nota',$user->entrada->id)}}"
                  class="btn btn-default" title="Nova {{$title}}">
                  <i class="fa fa-plus" aria-hidden="true" style="width: 15px;"></i>
                </a>
              </td>
              <td>{{$user->qtdade}}</td>
              <td>{{$user->entrada->nome}}</td>
              <td>{{$user->titulo}}</td>
              <td>{{$user->autor}}</td>
              <td>{{$user->isbn}}</td>
              <td>{{floatval($user->percent)}}%</td>
              <td>R${{number_format($user->valor_percent, 2, ',', '.')}}</td>
              <td>R${{number_format($user->valor_pvp, 2, ',', '.')}}</td>
              <td>R${{number_format($user->valor_total, 2, ',', '.')}}</td>
              <td>{{date('d/m/Y | H:i:s', strtotime($user->created_at))}}</td>

              <td class="text-center" style="width:16%;">
                {{Form::open(['route'=>['nota-entrega.destroy',$user->id], 'method'=>'DELETE'])}}
                <a class="btn btn-default btn-sm" title="Editar: {{$user->nome}}"   href='{{route('editar.nota',$user->id)}}'><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                <a class="btn btn-default btn-sm" title="Ver: {{$user->nome}}"   href='{{route('ver.nota',$user->id )}}'><i class="fa fa-eye" aria-hidden="true"></i></a>

                {{ Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', ['class' => 'btn btn-default btn-sm', 'title'=>'Excluir', 'role' => 'button', 'type' => 'submit', 'onclick'=>"return confirm('Tem certeza que deseja excluir o Entrada: $user->nome?')"]) }}
                {{Form::close()}}
              </td>
            </tr>
          @empty
            <div class="col-md-12 text-center">
              <h1>
                <p>Nenhuma {{$title}} Cadastrada</p>
              </h1>
            </div>
          @endforelse
        </tbody>
      </div>
    </div>
  </div>
@endsection
@push('stack-js')
  @include('layouts.paginacaoPontos')
@endpush
