@extends('layouts.layout.template')


@section('content')

    @include('layouts.breadcrumb-users')


  <div class="title-pg">
    <h2 class="title-pg">Gestão de Usuário: <b>{{$user->name or 'Novo Usuário'}}</b></h2>
  </div>


  <div class="content-din">
    @if( isset($user) )
      {!! Form::model($user, ['route' => ['usuarios.update', $user->id], 'class' => 'form form-search form-ds', 'files' => true, 'method' => 'PUT']) !!}
      <input type="hidden"  value="{{$disabled = 1}}"/>

    @else

      <input type="hidden"  value="{{$disabled = 0}}"/>
      {!! Form::open(['route' => 'usuarios.store', 'class' => 'form form-search form-ds', 'files' => true]) !!}
    @endif
    <div class="panel panel-default col-md-12">
      <div class="mT5">
        @if( isset($errors) && count($errors) > 0 )
          <div class="alert alert-warning">
            @foreach($errors->all() as $error)
              <p>{{$error}}</p>
            @endforeach
          </div>
        @endif
      </div>


      <div class="mT5">
        <div class="form-group col-sm-6">
          {!! Form::text('name', null, ['placeholder' => 'Nome:*', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-sm-4">
          {!! Form::text('username', null, ['placeholder' => 'Username:*', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-sm-2">
          {!! Form::select('nivel', [
            "2"=>"Admin",
            "3"=>"User",
          ], null, ['placeholder' => 'Selecione...',"class"=>"form-control"]) !!}
        </div>
        <div class="form-group col-sm-4">
          {!! Form::email('email', null, ['placeholder' => 'Email:*', 'class' => 'form-control']) !!}
        </div>

        <div class="form-group col-sm-4">
          {!! Form::text('telefone', null, ['placeholder' => 'Telefone:*', 'class' => 'form-control', 'id' => 'telefone',  'maxlength'=>'15']) !!}
        </div>
        <div class="col-sm-4 form-group">
        @if( isset($user) )
          @if ($user->nascimento == null)
            {!! Form::text("nascimento",'Não Informada',['placeholder' => 'Data Nascimento:',"class"=>"form-control", "id"=>"datetimepicker"]) !!}
          @else
            {!! Form::text("nascimento",\Carbon\Carbon::parse($user->nascimento)->format('d/m/Y'),['placeholder' => 'Data Nascimento:', "class"=>"form-control", "id"=>"datetimepicker"]) !!}
          @endif
        @else
          {!! Form::text('nascimento',null,['placeholder' => 'Data Nascimento:', 'class' => 'form-control', 'id'=>'datetimepicker']) !!}
        @endif
      </div>

        <div class="col-sm-12">
          <div class="title-pg text-center">
            <h2 class="title-pg"><b>Tirar foto ou Selecionar Arquivo</b></h2>
          </div>

        </div>

        <div class="col-sm-12 col-md-6">
          <div class="col-sm-12 col-md-6 mB5">
            <div id="my_camera"></div>
            <input class="btn btn-danger mT5" type=button value="Webcam" onClick="configure()">
            <input class="btn btn-default mT5" type=button value="Click!" onClick="take_snapshot()">
            {{ Form::hidden('imagem', '', array('id' => 'imagem')) }}
          </div>

          <div class="col-sm-12 col-md-6">
            <div id="results" value="imagem"></div>
          </div>
        </div>


        <div class="form-group col-sm-6 mT5">
          {!! Form::file('image', ['class' => 'form-control', 'class' => 'btn btn-primary']) !!}
          <p><b>Largura Mínima: 50px, Largura Máxima: 500px</b></p>
          <p>Caso deseje alterar a senha, usar o link "Esqueci a Senha" no campo Login!</p>
          @if ($disabled == 0)
            <div class="form-group">
              {!! Form::Label('send_mail', 'Enviar email de Cadastro') !!}

              {!! Form::checkbox('send_mail', 1, true) !!}
            </div>
          @endif
        </div>

        <div class="col-sm-12">
          <div class="form-group col-sm-12" style="transform: TranslateY(170%);">
            {!! Form::submit('Enviar', ['class' => 'btn btn-primary', 'style' => 'float:right;']) !!}
          </div>
        </div>

        {!! Form::close() !!}

      </div><!--Content DinÃ¢mico-->
    </div><!--Content DinÃ¢mico-->
  </div><!--Content DinÃ¢mico-->
</div><!--Content DinÃ¢mico-->

{{-- @include('painel.templates.telefone') --}}
<script type="text/javascript">

/* Máscaras ER */
function mascaratel(o,f){
  v_obj=o
  v_fun=f
  setTimeout("execmascara()",1)
}
function execmascara(){
  v_obj.value=v_fun(v_obj.value)
}
function mtel(v){
  v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
  v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
  v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
  return v;
}
function id( el ){
  return document.getElementById( el );
}
window.onload = function(){
  id('telefone').onkeyup = function(){
    mascaratel( this, mtel );
  }
}
</script>

@endsection

@push('stack-js')

  <style>
  #my_camera{
    width: 213px;
    height: 160px;
    border: 1px solid #4577b3;
  }
</style>


<script type="text/javascript" src="{{asset('assets/webcamjs/webcam.min.js')}}"></script>


<!-- Code to handle taking the snapshot and displaying it locally -->
<script language="JavaScript">

// Configure a few settings and attach camera
function configure(){
  Webcam.set({
    width: 213,
    height: 160,
    image_format: 'jpeg',
    jpeg_quality: 90
  });
  Webcam.attach( '#my_camera' );
}
// A button for taking snaps


// preload shutter audio clip
var shutter = new Audio();
shutter.autoplay = false;
shutter.src = navigator.userAgent.match(/Firefox/) ? 'shutter.ogg' : 'shutter.mp3';

function take_snapshot() {
  // play sound effect
  shutter.play();

  // take snapshot and get image data
  Webcam.snap( function(data_uri) {
    // display results in page
    document.getElementById('results').innerHTML =
    '<img id="imageprev" src="'+data_uri+'"/>';

    var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');

    document.getElementById('imagem').value = raw_image_data;
    document.getElementById('myform').submit();


  } );

}

function saveSnap(){
  // Get base64 value from <img id='imageprev'> source
  var base64image = document.getElementById("imageprev").src;

  Webcam.upload( base64image, '{{route('usuarios.store')}}', function(code, text) {
    console.log('Save successfully');
    console.log(text);
  });

}
</script>



  <script type="text/javascript" src="{{asset('/assets/js/jquery.maskedinput.min.js')}}"></script>
  <script type="text/javascript">
  jQuery(function($){

    $("#cpf").mask("999.999.999-99");

    $("#cnpj").mask("99.999.999/9999-99");
    $("#datetimepicker").mask("99/99/9999");

  });
  </script>
  <script type="text/javascript">


  function mascara(t, mask){
    var i = t.value.length;
    var saida = mask.substring(1,0);
    var texto = mask.substring(i)
    if (texto.substring(0,1) != saida){
      t.value += texto.substring(0,1);
    }
  }

  $( function() {
    $( "#datetimepicker" ).datepicker({
        format: 'dd/mm/yyyy',
        language: 'pt-BR'
      });
  } );
</script>

@endpush
