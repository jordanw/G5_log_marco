@extends('layouts.layout.template')
@section('title', 'Up - Usuário Edit')
@section('content')
    @include('layouts.breadcrumb-users')

  <div class="content-din">
    @if( isset($errors) && count($errors) > 0 )
      <div class="alert alert-warning">
        @foreach($errors->all() as $error)
          <p>{{$error}}</p>
        @endforeach
      </div>
    @endif
    <div class="col-lg-4 col-md-offset-4">
      <div class="widget-head-color-box blue-bg p-lg text-center">
        <div class="m-b-md">
          <h2 class="font-bold no-margins">{{$user->name}}</h2>
          {{-- @include('painel.templates.userLevel') --}}
        </div>
        @if($user->image == null )
            <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/painel/imgs/no-image.png')}}" style="max-width:156px;"></div>
        @else
          <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/uploads/users/'.$user->image)}}" style="max-width:156px;"></div>
        @endif
      </div>
      <div class="widget-text-box">
        <h4><strong>Nome:</strong> {{$user->name}}</h4>
        <h4><strong>Email:</strong> {{$user->email}}</h4>
        <h4><strong>Telefone:</strong> {{$user->telefone}}</h4>
        <h4><strong>Data de Nascimento:</strong>
          @if ($user->nascimento == null)
            {!! 'Não Informada' !!}
          @else
            {!! \Carbon\Carbon::parse($user->nascimento)->format('d/m/Y') !!}
          @endif
        </h4>


        @if(Auth::user()->nivel == 1)
          <div class="rgt">
            {!! Form::open(['route' => ['usuarios.destroy', $user->id], 'method' => 'DELETE']) !!}
            <div class="form-group">
              
              <a class="btn btn-warning" title="Editar: {{$user->name}}"   href='{{route('usuarios.edit',$user->id)}}'><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
              {!! Form::button('<i class="fa fa-trash-o fa-2x"></i> ', ['class' => 'btn btn-danger', 'role' => 'button', 'type' => 'submit', 'onclick'=>"return confirm('Tem certeza que deseja excluir: $user->name ?')"]) !!}

          </div>
            {!! Form::close() !!}
          </div>
        @endif()
      </div>
    </div>





  </div><!--Content DinÃ¢mico-->

@endsection
