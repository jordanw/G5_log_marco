@extends('layouts.layout.template')
@section('title', 'Usuários')
@section('content')

  @include('layouts.breadcrumb-users')

  <div class="col-md-12 lft">
    <a href="{{route('usuarios.create')}}" class="btn btn-primary">
      <span class="glyphicon glyphicon-plus"></span>
      Cadastrar
    </a>
  </div>

  <div class="col-md-12">
    @if( Session::has('success') )
      <div class="alert alert-success hide-msg" style="float: left; width: 100%; margin: 10px 0px;">
        {{Session::get('success')}}
      </div>
    @endif
  </div>

  <div class="col-md-12">
    <div class="row">
      <div class="col-md-12" style="margin-top:10px;">
        <div class="panel panel-default">
          <div class="table-responsive">
            <table class="table table-striped table-hover table-responsive" id="myTable">
              <thead>
                <tr>
                  <th>Cod</th>
                  <th>Foto</th>
                  <th>Nome</th>
                  <th>Username</th>
                  <th>Nascimento</th>
                  <th>Email</th>
                  <th>Telefone</th>
                  <th class="text-center" class="center">Ações</th>
                </tr>
              </thead>

              <tbody>
                @forelse($users as $user)
                  <tr>
                    <td>{{$user->id}}</td>
                    <td style="text-align: center; vertical-align: middle, max-width: 50px">
                      @if($user->image == null )
                        <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/painel/imgs/no-image.png')}}" style="max-width:96px;"></div>
                      @else
                        <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/uploads/users/'.$user->image)}}" style="max-width:96px;"></div>
                      @endif
                    </td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->username}}</td>
                    <td>
                      @if ($user->nascimento == null)
                        {!! 'Não Informada' !!}
                      @else
                        {!! \Carbon\Carbon::parse($user->nascimento)->format('d/m/Y') !!}
                      @endif
                    </td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->telefone}}</td>
                    <td class="text-center" style="width:16%;">
                      {{Form::open(['route'=>['usuarios.destroy',$user->id], 'method'=>'DELETE'])}}
                      <a class="btn btn-default btn-sm" title="Editar: {{$user->name}}"   href='{{route('usuarios.edit',$user->id)}}'><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                      <a class="btn btn-default btn-sm" title="Ver: {{$user->name}}"   href='{{route('usuarios.show',$user->id)}}'><i class="fa fa-eye" aria-hidden="true"></i></a>

                      {{ Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', ['class' => 'btn btn-default btn-sm', 'title'=>'Excluir', 'role' => 'button', 'type' => 'submit', 'onclick'=>"return confirm('Tem certeza que deseja excluir o Entrada: $user->name?')"]) }}
                      {{Form::close()}}
                    </td>
                  </tr>
                @empty
                  <div class="col-md-12 text-center">
                    <h1>
                      <p>Nenhum Usuário Cadastrado</p>
                    </h1>
                  </div>
                @endforelse
              </tbody>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</div>

@endsection
@push('stack-js')
  @include('layouts.paginacaoPontos')
@endpush
