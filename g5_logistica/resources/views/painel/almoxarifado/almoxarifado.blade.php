
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

<style>
table
{
    font-size: 13px;
    font-family: 'Roboto', sans-serif;
}


</style>

@extends('layouts.layout.template')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="painel-heading">
                        <h1>Itens Almoxarifado</h1>
                        @if(session('message'))
                            <div class="alert alert-success">
                                {{session('message')}}
                            </div>
                        @endif
                        @if(session('updateMessage'))
                            <div class="alert alert-success">
                                {{session('updateMessage')}}
                            </div>
                        @endif
                        @if(Session::has('insertMessage'))
                            <div class="alert alert-success">
                                {{Session::get('insertMessage')}}
                            </div>
                        @endif
                        <a class="btn btn-danger btn-lg" href="{{url('/almoxarifado/create')}}" >
                            <span class="glyphicon glyphicon-plus"></span>
                            Cadastrar
                        </a>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-hover table-sm table-responsive col-md-12">
                            <thead class="col-md-12">
                                <th>Data:</th>
                                <th>Produto:</th>
                                <th>Frota/<br>Equipamento:</th>
                                <th>Nota fiscal:</th>
                                <th>Responsável:</th>
                                <th>Custo<br>unitário: </th>
                                <th>Qtd:</th>
                                <th>Fornecedor: </th>
                                <th>Estoque<br>estimado:</th>
                                <th>Data de<br>entrega:</th>
                                <th>Total de<br>compra:</th>
                                <th>Total de<br>saída:</th>
                                <th>Criado em:</th>
                                <th>Número OS:</th>
                                <th>Editar/Excluir</th>
                            </thead>
                            <tbody class="col-md-12">

                            @foreach($almoxarifado as $al)
                                <tr>
                                    <td>{{date('d/m/Y', strtotime($al->_date))}}</td>
                                    <td>{{$al->produto}}</td>
                                    <td>{{$al->frota_equipamento}}</td>
                                    <td>{{$al->nota_fiscal}}</td>
                                    <td>{{$al->responsavel}}</td>
                                    <td>R$ {{$al->custo_unitario}}</td>
                                    <td>{{$al->quantidade}}</td>
                                    <td>{{$al->fornecedor}}</td>
                                    <td>{{$al->estoque_estimado}}</td>
                                    <td>{{$al->data_de_entrega}}</td>
                                    <td>R$ {{$al->total_de_compra}}</td>
                                    <td>R$ {{$al->total_de_saida}}</td>
                                    <td>R$ {{$al->numero_os}}</td>
                                    <td>{{date('d/m/Y', strtotime($al->created_at))}}</td>
                                    <td>
                                        <a title="Editar" style="display:inline-block" class="btn btn-default btn-small"
                                           href="/almoxarifado/{{$al->id}}/edit"
                                        ><span class="glyphicon glyphicon-pencil"></span></a>
                                    {!! Form::open(['method'=>'DELETE', 'url'=> "/almoxarifado/".$al->id."/destroy",
                                        'style'=> 'display:inline']) !!}
                                        <button type="submit" class="btn btn-default btn-small"
                                                title="Excluir"   >
                                            <span class="glyphicon glyphicon-remove-circle"></span></button>
                                        {!! Form::close() !!}</td>
                                </tr>
                             @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
