@extends('layouts.layout.template')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="painel-heading">
                        <h1>Cadastro Almoxarifado:</h1>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul> @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach </ul>
                            </div>
                        @endif
                    </div>
                    <div class="panel-body">
                        <!-- HERE, WE USE THE SAME FORM FOR INSERT AND EDIT
                        CHECK IF THE ROUT HAVE A 'EDIT' OR 'INCLUDE'
                        IN CASE EDIT, WE USE FORM:MODE
                        -->
                        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

                        <style>
                            form
                            {
                                font-size: 11px;
                                font-family: 'Roboto', sans-serif;
                            }
                            form input
                            {
                                width: 200px;
                            }
                        </style>


                        @if(Session::has('message'))
                            <div class="alert alert-success">Cadastro realizado com sucesso!</div>
                        @endif
                        @if(Request::is('*/edit'))
                            {{Form::model($almoxarifado, ['method'=>'PATCH','url'=>'almoxarifado/'.$almoxarifado->id.'/update'])}}
                        @else
                            {!! Form::open(['url' => '/almoxarifado/salvar'])!!}
                        @endif

                        {!! csrf_field() !!}

                        <div class="form-group col-md-4">
                            {!! Form::label('produto','Produto: ') !!}
                            {!! Form::input('text','produto', null,['class'=>'form-control', 'autofocus','placeholder'=>'Produto']) !!}
                        </div>

                        <div class="form-group col-md-4">
                            {!! Form::label('frota_equipamento','Frota/Equipamento: ') !!}
                            {!! Form::input('text','frota_equipamento', null,['class'=>'form-control', 'autofocus','placeholder'=>'Frota/Equipamento']) !!}
                        </div>


                        <div class="form-group col-md-4">
                            {!! Form::label('data_atual', 'Data:') !!}
                            {!! Form::date('_date', \Carbon\Carbon::now(),null,['class'=>'form-control', 'autofocus']) !!}
                        </div>


                        <div class="form-group col-md-4">
                            {!! Form::label('custo_unitario', 'Custo unitário') !!}
                            {!! Form::text('custo_unitario',null,['class'=>'form-control', 'autofocus',]) !!}
                        </div>

                        <div class="form-group col-md-4">
                            {!! Form::label('quantidade', 'Quantidade') !!}
                            {!! Form::number('quantidade',null,['class'=>'form-control', 'autofocus']) !!}
                        </div>

                        <div class="form-group col-md-4">
                            {!! Form::label('fornecedor', 'Fornecedor') !!}
                            {!! Form::input('text','fornecedor',null,['class'=>'form-control', 'autofocus','placeholder'=>'Fornecedor']) !!}
                        </div>

                        <div class="form-group col-md-4">
                            {!! Form::label('data_de_entrega', 'Data de entrega:') !!}
                            {!! Form::date('data_de_entrega', \Carbon\Carbon::now(),null,['class'=>'form-control', 'autofocus']) !!}
                        </div>

                        <div class="form-group col-md-4">
                            {!! Form::label('estoque_estimado', 'Estoque estimado:') !!}
                            {!! Form::number('estoque_estimado',null,['class'=>'form-control', 'autofocus']) !!}
                        </div>

                        <div class="form-group col-md-4">
                            {!! Form::label('total_de_compra', 'Total de compras:') !!}
                            {!! Form::number('total_de_compra',null,['class'=>'form-control', 'autofocus']) !!}
                        </div>

                        <div class="form-group col-md-4">
                            {!! Form::label('total_de_saida', 'Total de saídas:') !!}
                            {!! Form::number('total_de_saida',null,['class'=>'form-control', 'autofocus']) !!}
                        </div>

                        <div class="form-group col-md-4">
                            {!! Form::label('nota_fiscal','Nota Fiscal') !!}
                            {!! Form::number('nota_fiscal',null,['class'=>'form-control', 'autofocus','placeholder'=>'Insira o código da nota fiscal']) !!}
                        </div>

                        <div class="form-group col-md-4">
                            {!! Form::label('responsavel','Responsável') !!}
                            {!! Form::text('responsavel',null,['class'=>'form-control', 'autofocus','placeholder'=>'Responsável']) !!}
                        </div>

                        <div class="form-group col-md-4">
                            {!! Form::label('numero_os','Número da OS:') !!}
                            {!! Form::number('numero_os',null,['class'=>'form-control', 'autofocus','placeholder'=>'Número OS']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Salvar',['class'=>'btn btn-primary']) !!}
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>

            </div>
        </div>
    </div>
@endsection