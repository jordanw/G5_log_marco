@extends('layouts.layout.template')

@php
  use Carbon\Carbon;
@endphp

@section('content')

 oiiiiiiiiiiiiiii

@endsection

@push('stack-css')
  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="{{asset('assets/js/plugins/prism/prism.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{asset('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{asset('assets/js/plugins/chartist-js/chartist.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{asset('assets/js/plugins/dropify/css/dropify.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
@endpush

@push('stack-js')

  @include('layouts.paginacaoPontos')

  <script type="text/javascript" src="{{asset('/assets/js/jquery.maskedinput.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('/assets/js/jquery.maskMoney.js')}}"></script>
  <script type="text/javascript">
  jQuery(function($){

    $("#cpf").mask("999.999.999-99");

    $("#cnpj").mask("99.999.999/9999-99");
    $("#datetimepicker").mask("99/99/9999");
    $("#valor").maskMoney();


  });




  </script>



@endpush
