@extends('layouts.layout.template')
@section('title', 'Usuários')

@section('content')

  <!-- Search for small screen -->

  @include('layouts.breadcrumb-nota-entrega')



  <div class="section">
    @include('layouts.success')
    @include('layouts.session')
  </div>



  <div class="col-md-12">
    @if( Session::has('success') )
      <div class="alert alert-success hide-msg" style="float: left; width: 100%; margin: 10px 0px;">
        {{Session::get('success')}}
      </div>
    @endif
  </div>
  <div class="col-md-12" style="margin-top:10px;">
    <div class="panel panel-default">
      <div class="table-responsive">
        <table class="table table-striped table-hover table-responsive" id="myTable">
          <thead>
            <tr>
              <th>Cod</th>
              <th class="text-center" class="center">Concluir</th>
              <th>Foto</th>
              <th>Nome</th>
              <th>Local</th>
              <th>Ap</th>
              <th>Tipo</th>
              <th>Autorizado:</th>
              <th>Data | Hora</th>
              <th>Tempo Estimado</th>
            </tr>
          </thead>

          <tbody>
            @forelse($dataAcesso as $user)
              <tr>
                <td>{{$user->id}}</td>
                <td class="text-center" style="width:10%;">
                  {{Form::open(['route'=>['acessos.destroy',$user->id], 'method'=>'DELETE'])}}

                  {{ Form::button('<i class="fa fa-check fa-3x" aria-hidden="true"></i>', ['class' => 'btn btn-danger btn-sm', 'title'=>'Concluir', 'role' => 'button', 'type' => 'submit', 'onclick'=>"return confirm('Tem certeza que deseja excluir o Acesso de: {$user->entrada->nome}?')"]) }}
                  {{Form::close()}}
                </td>
                <td style="text-align: center; vertical-align: middle, max-width: 50px">
                  @if($user->entrada->image == null )
                    <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/painel/imgs/no-image.png')}}" style="max-width:96px;"></div>
                  @else
                    <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/uploads/entradas/'.$user->entrada->image)}}" style="max-width:96px;"></div>
                  @endif
                </td>
                <td>{{$user->entrada->nome}}</td>
                <td>{{$user->local_acesso}}</td>
                <td>{{$user->local_especifico}}</td>
                <td>{{$user->tipo_fornecedor}}</td>
                <td>{{$user->concedido_acesso_por}}</td>
                <td>{{date('H:i:s | d/m/Y', strtotime($user->date_concedido_acesso))}}</td>
                @if ($user->tempo_estimado == null)
                  <td>Não Informado</td>
                @else
                  <td>{{date('H:i:s | d/m/Y', strtotime($user->tempo_estimado))}}</td>
                @endif


              </tr>
            @empty
              <div class="col-md-12 text-center">
                <h1>
                  <p>Nenhum Acesso Ativo</p>
                </h1>
              </div>
            @endforelse
          </tbody>
        </div>
      </div>
    </div>
  @endsection
  @push('stack-js')
    @include('layouts.paginacaoPontos')
  @endpush
