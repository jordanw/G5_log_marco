@extends('layouts.layout.template')

@php
use Carbon\Carbon;
@endphp

@section('content')


  <div class="row">
    <div class="col-md-6 text-left">
      <h4 class="breadcrumb-title">{{$user->nome or 'Nova Nota de Entrega'}} </h4>
    </div>
    <div class="col-md-6">
      <ol class="breadcrumb hidden-xs text-right mt1">
        <li><a href="{{url('/painel')}}">Home</a></li>
        <li><a href="{{url('/painel/escrtitores')}}">Entradas</a></li>
        <li class="active"><a href="#">{{$user->nome or 'Nova Nota de Entrega'}}</a></li>
      </ol>
    </div>
  </div>

  <div class="section">
    @include('layouts.success')
    @include('layouts.session')
  </div>


  {{-- <div class="container">
  @if( isset($user) )
  @include('layouts.userImageCard')
@endif
</div> --}}
<div class="col-md-5" style="margin-top: 3%;">
  <div class="ibox float-e-margins">

    <div class="ibox-content">
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Cod</th>
              <th>Foto</th>
              <th>Nome </th>
              <th>Telefone </th>


            </tr>
          </thead>
          <tbody>
            @isset($jom)
              <tr>
                <td>{{$user->entrada->id}}</td>
                <td style="text-align: center; vertical-align: middle, max-width: 50px">
                  @if($user->entrada->image == null )
                    <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/painel/imgs/no-image.png')}}" style="max-width:96px;"></div>
                  @else
                    <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/uploads/entradas/'.$user->entrada->image)}}" style="max-width:96px;"></div>
                  @endif
                </td>

                <td>{{$user->entrada->nome}}</td>
                <td>{{$user->entrada->telefone}}</td>
              </tr>
            @else
              <tr>
                <td>{{$user->id}}</td>
                <td style="text-align: center; vertical-align: middle, max-width: 50px">
                  @if($user->image == null )
                    <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/painel/imgs/no-image.png')}}" style="max-width:96px;"></div>
                  @else
                    <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/uploads/entradas/'.$user->image)}}" style="max-width:96px;"></div>
                  @endif
                </td>
                <td>{{$user->nome}}</td>
                <td>{{$user->telefone}}</td>
              </tr>
            @endisset
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="col-md-7">
  <div class="row">

    <div class="col-lg-4 col-sm-4">
      <div class="circle-tile">
        <a href="#">
          <div class="circle-tile-heading orange">
            <i class="fa fa-external-link-square fa-fw fa-3x"></i>

          </div>
        </a>
        <div class="circle-tile-content orange">
          <div class="circle-tile-description text-faded">
            Acessos Ativos
          </div>
          <div class="circle-tile-number text-faded">
            {{$acessosAtivos}}
          </div>
          <a href="#" class="circle-tile-footer">N<sup>o</sup> <i class="fa fa-chevron-circle-right"></i></a>

        </div>
      </div>
    </div>
    <div class="col-lg-4 col-sm-4">
      <div class="circle-tile">
        <a href="#">
          <div class="circle-tile-heading green">
            <i class="fa fa-check-square-o fa-fw fa-3x"></i>
          </div>
        </a>
        <div class="circle-tile-content green">
          <div class="circle-tile-description text-faded">
            Acessos Fechados
          </div>
          <div class="circle-tile-number text-faded">
            {{ $acessosFechados}}
          </div>
          <a href="#" class="circle-tile-footer">N<sup>o</sup> <i class="fa fa-chevron-circle-right"></i></a>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-sm-4">
      <div class="circle-tile">
        <a href="#">
          <div class="circle-tile-heading blue">
            <i class="fa fa-bell fa-fw fa-3x"></i>
          </div>
        </a>
        <div class="circle-tile-content blue">
          <div class="circle-tile-description text-faded">
            Último acesso
          </div>
          <div class="circle-tile-number text-faded">
            {{-- {{date("d/m/Y | H:i" , strtotime($user->date_prestado))}} --}}
            {{  Carbon::parse($ultimoAcesso)->diffInDays()}}
          </div>
          <a href="#" class="circle-tile-footer">Dias <i class="fa fa-chevron-circle-right"></i></a>
        </div>
      </div>
    </div>


  </div>

</div>


<div class="col-md-12">
  <div class="ibox float-e-margins">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="panel-head">
          <h3>Registrar Entrada</h3>
        </div>
        <div class="col-xs-12 col-sm-12">
          @include('painel.acessos.form._formBase-acesso')

        </div>
      </div>

    </div>

    <div class="ibox-content">
      <div class="panel-head">
        <h3>Entradas Registradas</h3>
      </div>
      <div class="table-responsive">
        <table class="table table-striped" id="myTable">
          <thead>
            <tr>
              <th>Cod</th>
              <th class="text-center" class="center">Concluir</th>
              <th>Nome</th>
              <th>Local</th>
              <th>Tipo</th>
              <th>Tempo Estimado</th>
              <th>Autorizado por:</th>
              <th>Hora | Data Autorização</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($user->acessos->where('ativo', '1') as $user)
              <tr>
                <td>{{$user->id}}</td>
                <td class="text-center" style="width:10%;">
                  {{Form::open(['route'=>['acessos.destroy',$user->id], 'method'=>'DELETE'])}}

                  {{ Form::button('<i class="fa fa-check fa-3x" aria-hidden="true"></i>', ['class' => 'btn btn-danger btn-sm', 'title'=>'Concluir', 'role' => 'button', 'type' => 'submit', 'onclick'=>"return confirm('Tem certeza que deseja excluir o Acesso de: {$user->entrada->nome}?')"]) }}
                  {{Form::close()}}
                </td>
                <td>{{$user->entrada->nome}}</td>
                <td>{{$user->local_acesso}}</td>
                <td>{{$user->tipo_fornecedor}}</td>
                @if ($user->tempo_estimado == '')
                  <td>Não Informou</td>
                @else
                  <td>{{date('H:i:s | d/m/Y', strtotime($user->tempo_estimado))}}</td>
                @endif
                <td>{{$user->concedido_acesso_por}}</td>
                <td>{{date('H:i:s | d/m/Y', strtotime($user->date_concedido_acesso))}}</td>



              </tr>
            @endforeach
          </tbody>
        </table>

      </div>
    </div>
    <div class="ibox-content">
      <div class="panel-head">
        <h3>Saídas</h3>
      </div>
      <div class="table-responsive">
        <table class="table" id="myTable3">
          <thead>
            <tr>
              <th>Cod</th>
              <th>Nome</th>
              <th>Local</th>
              <th>Tipo</th>
              <th>Tempo Estimado</th>
              <th>Concluído por:</th>
              <th>Hora | Data Cnclusão</th>
              {{-- <th class="text-center" class="center" style="width:3%">Ações</th> --}}
            </tr>
          </thead>
          <tbody>
            @foreach ($user2->acessos->where('ativo', '0') as $user)
              <tr class="strikeout" style="color: darkgray;">
                <td>{{$user->id}}</td>
                <td>{{$user->entrada->nome}}</td>
                <td>{{$user->local_acesso}}</td>
                <td>{{$user->tipo_fornecedor}}</td>
                @if ($user->tempo_estimado == '')
                  <td>Não Informou</td>
                @else
                  <td>{{date('H:i:s | d/m/Y', strtotime($user->tempo_estimado))}}</td>
                @endif
                <td>{{$user->concluido_acesso_por}}</td>
                <td>{{date('H:i:s | d/m/Y', strtotime($user->date_concluido_acesso))}}</td>

                {{-- <td class="text-center" style="width:16%;">
                {{Form::open(['route'=>['acessos.destroy',$user->id], 'method'=>'DELETE'])}}

                {{ Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', ['class' => 'btn btn-default btn-sm', 'title'=>'Excluir', 'role' => 'button', 'type' => 'submit', 'onclick'=>"return confirm('Tem certeza que deseja excluir o Entrada: $user->nome?')"]) }}
                {{Form::close()}}
              </td> --}}

            </tr>
          @endforeach
        </tbody>
      </table>

    </div>
  </div>
  {{-- <div class="col-sm-12 col-xs-12">
  <div class="col-sm-6 col-xs-6"><h5>Total Compras</h5></div>
  <div class="col-sm-6 col-xs-6 right"><h4>R$ {{ number_format($totalAdiantCliente,2,',','.') }}</h4></div>
</div> --}}
</div>
</div>

@endsection

@push('stack-css')
  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="{{asset('assets/js/plugins/prism/prism.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{asset('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{asset('assets/js/plugins/chartist-js/chartist.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{asset('assets/js/plugins/dropify/css/dropify.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
@endpush

@push('stack-js')

  @include('layouts.paginacaoPontos')

  <script type="text/javascript" src="{{asset('/assets/js/jquery.maskedinput.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('/assets/js/jquery.maskMoney.js')}}"></script>
  <script type="text/javascript">
  jQuery(function($){

    $("#cpf").mask("999.999.999-99");

    $("#cnpj").mask("99.999.999/9999-99");
    $("#datetimepicker").mask("99/99/9999");
    $("#valor").maskMoney();


  });




  </script>



@endpush
