<?php
use Carbon\Carbon;
?>
@if( isset($editar) )
  {!! Form::model($user, ['route' => ['acessos.update', $user->id], 'class' => 'form form-search form-ds', 'files' => true, 'method' => 'PUT']) !!}
  <input type="hidden"  value="{{$disabled = 1}}"/>

@else
  <input type="hidden"  value="{{$disabled = 0}}"/>
  {!! Form::open(['route' => ['acessos.store', $user->id],  'class' => 'form form-search form-ds', 'files' => true]) !!}
@endif

<div class="col-xs-12 col-sm-12">

  <div class="form-group">

    <div class="col-sm-3">
      {!! Form::label("local_acesso","Local:") !!}
      {!! Form::select('local_acesso', [
        "Bloco I"=>"Bloco I",
        "Bloco II"=>"Bloco II",
      ], null, ['placeholder' => 'Selecione...',"class"=>"form-control"]) !!}
    </div>

    <div class="col-sm-3">
      {!! Form::label("local_especifico","Ap:") !!}
      {!! Form::select('local_especifico', [
        "101"         => "101",
        "102"         => "102",
        "103"         => "103",
        "104"         => "104",
        "201"         => "201",
        "202"         => "202",
        "203"         => "203",
        "204"         => "204",
        "301"         => "301",
        "302"         => "302",
        "303"         => "303",
        "304"         => "304",
        "401"         => "401",
        "402"         => "402",
        "403"         => "403",
        "404"         => "404",
        "501"         => "501",
        "502"         => "502",
        "503"         => "503",
        "504"         => "504",
        "Condomínio"  => "Condomínio"

      ], null, ['placeholder' => 'Selecione...',"class"=>"form-control"]) !!}
    </div>

    <div class="col-sm-3">
      {!! Form::label("tipo_fornecedor","Tipo:") !!}
      {!! Form::select('tipo_fornecedor', [
        "Água"        =>"Água",
        "Amigo(a)"    =>"Amigo(a)",
        "Delivery"    =>"Delivery",
        "Dedetização" =>"Dedetização",
        "Família"     =>"Família",
        "Gás"         =>"Gás",
        "Justiça"     =>"Justiça",
        "Loja"        =>"Loja",
        "Manutenção"  =>"Manutenção",
        "Médico"      =>"Médico",
        "Montador"    =>"Montador",
        "Motorista"   =>"Motorista",
        "Mudança"     =>"Mudança",
        "Polícia"     =>"Polícia",
        "Saúde"       =>"Saúde",
        "Outros"      =>"Outros",
      ], null, ['placeholder' => 'Selecione...',"class"=>"form-control"]) !!}
    </div>

    <div class="col-sm-3">
      {!! Form::label("tempo_estimado","Tempo Estimado:") !!}
      {!! Form::select('tempo_estimado', [
        "10"=>"até 10 minutos",
        "30"=>"até 30 minutos",
        "60"=>"até 60 minutos",
        "5h"=>"até 5 horas",
        "NS"=>"não sabe / não informou",

      ], null, ['placeholder' => 'Selecione...',"class"=>"form-control"]) !!}
    </div>



    <div class="col-sm-12">
      {!! Form::label("observacao","Observação:") !!}
      {!! Form::textarea("observacao",null,["class"=>"form-control"]) !!}
    </div>



  </div>

  <div class="col-sm-12 form-group">
    {{Form::submit('Salvar',['class'=>'btn btn-primary','style'=>'font-weight:bold; margin-top:3%;'])}}
  </div>

</div>

{!! Form::close() !!}



{{-- @include('layouts.mascaras') --}}
