@extends('pack.template.templateGestor')
@section('content')
  @if ($errors->any())
    <div>
      @foreach ($errors->all() as $error)
        <li><i>{{$error}}</i></li>
      @endforeach
    </div>

  @endif

  @if(Session::has('flash_message'))
    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
  @endif


  <div class="row">
    <h2>{{$nomePagina}}</h2>


    <div class="col-md-4">
      <div class="ibox float-e-margins">

        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Cod</th>
                  <th>Nome </th>
                  <th>Telefone </th>


                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{{$adiantamento->id}}</td>
                  <td>{{$adiantamento->nome}}</td>
                  <td>{{$adiantamento->telefone}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      @include('pack.clientes.pontos.pontos')
    </div>
    <div class="col-md-8">
      <div class="ibox float-e-margins">

        {!! Form::open(array('route' => ['adiantamentos.update',$adiantamento->id], 'name'=>'form' , 'method' => 'PUT','files' => true)) !!}
        <div class="col-xs-12 col-sm-12">
          @include('pack.adiantamentos.clientes.form._formBase_okEdit')
          {!! Form::close() !!}
        </div>

        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table table-striped table-hover"  id="myTable">
              <thead>
                <tr>
                  <th class="lft">Cod. </th>
                  <th class="lft">Responsável </th>
                  <th>Data</th>
                  <th>N<sup>o</sup> da Nota </th>
                  <th class="rgt">Valor </th>
                  <th class="rgt">Adquiridos </th>
                  @if (Auth::user()->nivel == 1)
                    <th class="ctr">Excluir</th>
                  @endif
                </tr>
              </thead>
              <tbody>
                @foreach ($adiantamento->adiantamento_clientes as $ad)
                  <tr>
                    <td class="lft">{{$ad->id}}</td>
                    <td class="lft">{{$ad->nome_usuario}}</td>
                    <td>{{date("d/m/Y", strtotime($ad->data))}}</td>
                    <td class="rgt">{{$ad->numero_nota}}</td>
                    <td class="rgt">R${{ number_format($ad->valor_adiantamento_cliente,2,',','.') }}</td>
                    <td class="rgt">{{ round($ad->pontos) }}</td>
                    @if (Auth::user()->nivel == 1)
                      <td class="ctr">
                        {{Form::open(['route'=>['adiantamentos.destroy',$ad->id], 'method'=>'DELETE'])}}
                        {{ Form::button('<i class="fa fa-trash-o"></i>', ['class' => 'btn btn-default', 'title'=>'Excluir', 'style'=>'width: 41px', 'role' => 'button', 'type' => 'submit', 'onclick'=>"return confirm('Tem certeza que deseja Excluir, P-0$ad->id ?')"]) }}
                        {{Form::close()}}
                      </td>
                    @endif
                  </tr>
                @endforeach
              </tbody>
            </table>

          </div>
        </div>
        <div class="col-sm-12 col-xs-12">
          <div class="col-sm-6 col-xs-6"><h5>Total Compras</h5></div>
          <div class="col-sm-6 col-xs-6 ctr"><h4>R$ {{ number_format($totalAdiantCliente,2,',','.') }}</h4></div>
        </div>
      </div>
    </div>
  </div>
</div>



@stop

@section('js')
  @include('pack.template.paginacaoDate')
  <script>
  var data1 = [
    [0,4],[1,8],[2,5],[3,10],[4,4],[5,16],[6,5],[7,11],[8,6],[9,11],[10,30],[11,10],[12,13],[13,4],[14,3],[15,3],[16,6]
  ];
  var data2 = [
    [0,1],[1,0],[2,2],[3,0],[4,1],[5,3],[6,1],[7,5],[8,2],[9,3],[10,2],[11,1],[12,0],[13,2],[14,8],[15,0],[16,0]
  ];
  $("#flot-dashboard-chart").length && $.plot($("#flot-dashboard-chart"), [data1, data2], {
    series: {
      lines: {show: false, fill: true},
      splines: {show: true, tension: 0.4, lineWidth: 1, fill: 0.4},
      points: {radius: 0, show: true},
      shadowSize: 2
    },
    grid: {
      hoverable: true,
      clickable: true,
      tickColor: "#d5d5d5",
      borderWidth: 1,
      color: '#d5d5d5'
    },
    colors: ["#1ab394", "#464f88"], xaxis:{ },
    yaxis: {ticks: 4},
    tooltip: false
  });
  var doughnutData = [{
    value: 300,
    color: "#a3e1d4",
    highlight: "#1ab394",
    label: "App"
  },{
    value: 50,
    color: "#dedede",
    highlight: "#1ab394",
    label: "Software"
  },{
    value: 100,
    color: "#b5b8cf",
    highlight: "#1ab394",
    label: "Laptop"
  }];
  var doughnutOptions = {
    segmentShowStroke: true,
    segmentStrokeColor: "#fff",
    segmentStrokeWidth: 2,
    percentageInnerCutout: 45, // This is 0 for Pie charts
    animationSteps: 100,
    animationEasing: "easeOutBounce",
    animateRotate: true,
    animateScale: false,
  };
  var ctx = document.getElementById("doughnutChart").getContext("2d");
  var DoughnutChart = new Chart(ctx).Doughnut(doughnutData, doughnutOptions);
  var polarData = [{
    value: 300,
    color: "#a3e1d4",
    highlight: "#1ab394",
    label: "App"
  },{
    value: 140,
    color: "#dedede",
    highlight: "#1ab394",
    label: "Software"
  },{
    value: 200,
    color: "#b5b8cf",
    highlight: "#1ab394",
    label: "Laptop"
  }];
  var polarOptions = {
    scaleShowLabelBackdrop: true,
    scaleBackdropColor: "rgba(255,255,255,0.75)",
    scaleBeginAtZero: true,
    scaleBackdropPaddingY: 1,
    scaleBackdropPaddingX: 1,
    scaleShowLine: true,
    segmentShowStroke: true,
    segmentStrokeColor: "#fff",
    segmentStrokeWidth: 2,
    animationSteps: 100,
    animationEasing: "easeOutBounce",
    animateRotate: true,
    animateScale: false,
  };
  var ctx = document.getElementById("polarChart").getContext("2d");
  var Polarchart = new Chart(ctx).PolarArea(polarData, polarOptions);
});
</script>
@stop
