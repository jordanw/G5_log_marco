@extends('pack.template.templateGestor')
@section('content')

  @if(Session::has('flash_message'))
    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
  @endif
  <div class="wrapper wrapper-content animated fadeInRight">

    <div class="ibox-content">
      <div class="row">
        <div class="col-sm-12" style="margin-bottom: 12px">

        </div>
        <div class="col-sm-12">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="myTable">
              <thead>
                <tr>
                  <th style="text-align: center"><b>Cod.</b></th>
                  {{-- <th><b>LOGO</b></th> --}}
                  <th><b>Pontuar</b></th>
                  <th><b>Nome</b></th>
                  <th><b>CPF</b></th>
                  <th><b>Telefone</b></th>
                  <th><b>Email</b></th>


                  <th style="text-align: center">Ver Pontos</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($clientes as $cliente)

                  <tr>
                    <td style="text-align: center; vertical-align: middle;width: 6%;">{{$cliente->id}}</td>
                    <td style="text-align: center; vertical-align: middle;width: 9%;"><a href="{{ url('adiantamentos/'.$cliente->id.'/edit')}}"
                      class="btn btn-default" title="Nova Pontuação">
                      <i class="fa fa-plus" aria-hidden="true"  style="width: 11px;"></i>
                    </a></td>

                    <td style="vertical-align: middle">{{$cliente->nome}}</td>
                    <td style="vertical-align: middle">{{$cliente->cpf}}</td>
                    <td style="vertical-align: middle">{{$cliente->telefone}}</td>
                    <td style="vertical-align: middle">{{$cliente->email}}</td>


                    <td width="6%" style="text-align: center; vertical-align: middle">
                      <a href="{{ url('adiantamentos/'.$cliente->id)}}" class="btn btn-default" title="Ver Pontos" style="text-align: center; vertical-align: middle;"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    </td>
                  </tr>
                @endforeach

              </tbody>

            </table>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>


@include('pack.template.footer')
@stop

@section('js')
  @include('pack.template.paginacao')
  <script>
  var data1 = [
    [0,4],[1,8],[2,5],[3,10],[4,4],[5,16],[6,5],[7,11],[8,6],[9,11],[10,30],[11,10],[12,13],[13,4],[14,3],[15,3],[16,6]
  ];
  var data2 = [
    [0,1],[1,0],[2,2],[3,0],[4,1],[5,3],[6,1],[7,5],[8,2],[9,3],[10,2],[11,1],[12,0],[13,2],[14,8],[15,0],[16,0]
  ];
  $("#flot-dashboard-chart").length && $.plot($("#flot-dashboard-chart"), [data1, data2], {
    series: {
      lines: {show: false, fill: true},
      splines: {show: true, tension: 0.4, lineWidth: 1, fill: 0.4},
      points: {radius: 0, show: true},
      shadowSize: 2
    },
    grid: {
      hoverable: true,
      clickable: true,
      tickColor: "#d5d5d5",
      borderWidth: 1,
      color: '#d5d5d5'
    },
    colors: ["#1ab394", "#464f88"], xaxis:{ },
    yaxis: {ticks: 4},
    tooltip: false
  });
  var doughnutData = [{
    value: 300,
    color: "#a3e1d4",
    highlight: "#1ab394",
    label: "App"
  },{
    value: 50,
    color: "#dedede",
    highlight: "#1ab394",
    label: "Software"
  },{
    value: 100,
    color: "#b5b8cf",
    highlight: "#1ab394",
    label: "Laptop"
  }];
  var doughnutOptions = {
    segmentShowStroke: true,
    segmentStrokeColor: "#fff",
    segmentStrokeWidth: 2,
    percentageInnerCutout: 45, // This is 0 for Pie charts
    animationSteps: 100,
    animationEasing: "easeOutBounce",
    animateRotate: true,
    animateScale: false,
  };
  var ctx = document.getElementById("doughnutChart").getContext("2d");
  var DoughnutChart = new Chart(ctx).Doughnut(doughnutData, doughnutOptions);
  var polarData = [{
    value: 300,
    color: "#a3e1d4",
    highlight: "#1ab394",
    label: "App"
  },{
    value: 140,
    color: "#dedede",
    highlight: "#1ab394",
    label: "Software"
  },{
    value: 200,
    color: "#b5b8cf",
    highlight: "#1ab394",
    label: "Laptop"
  }];
  var polarOptions = {
    scaleShowLabelBackdrop: true,
    scaleBackdropColor: "rgba(255,255,255,0.75)",
    scaleBeginAtZero: true,
    scaleBackdropPaddingY: 1,
    scaleBackdropPaddingX: 1,
    scaleShowLine: true,
    segmentShowStroke: true,
    segmentStrokeColor: "#fff",
    segmentStrokeWidth: 2,
    animationSteps: 100,
    animationEasing: "easeOutBounce",
    animateRotate: true,
    animateScale: false,
  };
  var ctx = document.getElementById("polarChart").getContext("2d");
  var Polarchart = new Chart(ctx).PolarArea(polarData, polarOptions);
});
</script>
@stop
