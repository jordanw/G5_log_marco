@extends('pack.template.templateGestor')
@section('content')
  @if(Session::has('flash_message'))
    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
  @endif
  <div class="row">
    <h2>{{$nomePagina}}</h2>
    <div class="col-lg-4">
      <div class="ibox float-e-margins">
        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Cod</th>
                  <th>Nome </th>
                  <th class="ctr">Nova Pontuação</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{{$adiantamento->id}}</td>
                  <td>{{$adiantamento->nome}}</td>
                  <td class="ctr"><a href="{{ url('adiantamentos/'.$adiantamento->id.'/edit')}}"
                    class="btn btn-default" title="Nova Pontuação">
                    <i class="fa fa-plus" aria-hidden="true"  style="width: 11px;"></i>
                  </a> </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
        @include('pack.clientes.pontos.pontos')
    </div>
    <div class="col-lg-8">
      <div class="ibox float-e-margins">

        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table table-striped" id="myTable">
              <thead>
                <tr>
                  <th>Cod.</th>
                  <th>Reponsável</th>
                  <th>Data </th>
                  <th style="width: 30%">N<sup>o</sup> da Nota </th>
                  <th class="ctr">Valor </th>
                  <th class="ctr">Disponível </th>

                </tr>
              </thead>
              <tbody>
                @foreach ($adiantamento->adiantamento_clientes as $ad)
                  <tr>
                    <td>{{$ad->id}}</td>
                    <td>{{$ad->nome_usuario}}</td>
                    <td>{{date("d/m/Y", strtotime($ad->data))}}</td>
                    <td class="rgt">{{$ad->numero_nota}}</td>
                    <td class="rgt">R${{ number_format($ad->valor_adiantamento_cliente,2,',','.') }}</td>
                    <td class="rgt">{{$ad->pontos}}</td>

                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div div style="text-align: right;">
        <div class="col-sm-8"><h5>Total Compras</h5></div>
        <div class="col-sm-4"><h4>R$ {{ number_format($totalAdiantCliente,2,',','.') }}</h4></div>
      </div>
    </div>
    <div>
      @include('pack.template.footer')
    </div>
  </div>

@stop

@section('js')
  @include('pack.template.paginacaoDate')
  <script>
  var data1 = [
    [0,4],[1,8],[2,5],[3,10],[4,4],[5,16],[6,5],[7,11],[8,6],[9,11],[10,30],[11,10],[12,13],[13,4],[14,3],[15,3],[16,6]
  ];
  var data2 = [
    [0,1],[1,0],[2,2],[3,0],[4,1],[5,3],[6,1],[7,5],[8,2],[9,3],[10,2],[11,1],[12,0],[13,2],[14,8],[15,0],[16,0]
  ];
  $("#flot-dashboard-chart").length && $.plot($("#flot-dashboard-chart"), [data1, data2], {
    series: {
      lines: {show: false, fill: true},
      splines: {show: true, tension: 0.4, lineWidth: 1, fill: 0.4},
      points: {radius: 0, show: true},
      shadowSize: 2
    },
    grid: {
      hoverable: true,
      clickable: true,
      tickColor: "#d5d5d5",
      borderWidth: 1,
      color: '#d5d5d5'
    },
    colors: ["#1ab394", "#464f88"], xaxis:{ },
    yaxis: {ticks: 4},
    tooltip: false
  });
  var doughnutData = [{
    value: 300,
    color: "#a3e1d4",
    highlight: "#1ab394",
    label: "App"
  },{
    value: 50,
    color: "#dedede",
    highlight: "#1ab394",
    label: "Software"
  },{
    value: 100,
    color: "#b5b8cf",
    highlight: "#1ab394",
    label: "Laptop"
  }];
  var doughnutOptions = {
    segmentShowStroke: true,
    segmentStrokeColor: "#fff",
    segmentStrokeWidth: 2,
    percentageInnerCutout: 45, // This is 0 for Pie charts
    animationSteps: 100,
    animationEasing: "easeOutBounce",
    animateRotate: true,
    animateScale: false,
  };
  var ctx = document.getElementById("doughnutChart").getContext("2d");
  var DoughnutChart = new Chart(ctx).Doughnut(doughnutData, doughnutOptions);
  var polarData = [{
    value: 300,
    color: "#a3e1d4",
    highlight: "#1ab394",
    label: "App"
  },{
    value: 140,
    color: "#dedede",
    highlight: "#1ab394",
    label: "Software"
  },{
    value: 200,
    color: "#b5b8cf",
    highlight: "#1ab394",
    label: "Laptop"
  }];
  var polarOptions = {
    scaleShowLabelBackdrop: true,
    scaleBackdropColor: "rgba(255,255,255,0.75)",
    scaleBeginAtZero: true,
    scaleBackdropPaddingY: 1,
    scaleBackdropPaddingX: 1,
    scaleShowLine: true,
    segmentShowStroke: true,
    segmentStrokeColor: "#fff",
    segmentStrokeWidth: 2,
    animationSteps: 100,
    animationEasing: "easeOutBounce",
    animateRotate: true,
    animateScale: false,
  };
  var ctx = document.getElementById("polarChart").getContext("2d");
  var Polarchart = new Chart(ctx).PolarArea(polarData, polarOptions);
});
</script>
@stop
