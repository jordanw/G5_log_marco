
<div class="col-xs-12 col-sm-12">

  <div class="form-group">


      {!! Form::hidden("data",\Carbon\Carbon::now()->format('Y-m-d'),["class"=>"form-control"]) !!}


      {{ Form::hidden('nome', $adiantamento->nome) }}
    <div class="col-sm-5">
      <div class="col-sm-12">
        {!! Form::label("numero_nota","Número da Nota:") !!}
      </div>
      {!! Form::text("numero_nota",null,["class"=>"form-control", 'id'=>'total', 'maxlength'=>'15']) !!}
    </div>
    <div class="col-sm-5">
      <div class="col-sm-12">
      {!! Form::label("valor_adiantamento_cliente","Valor da Nota:") !!}
      </div>
      {!! Form::text("valor_adiantamento_cliente",null,['placeholder'=>'R$',"class"=>"form-control", 'id'=>'total', 'onkeypress'=>'mascara(this,moeda)', 'maxlength'=>'12']) !!}
    </div>


    <div class="col-sm-2">
    <div class="col-sm-12">
      {!! Form::label("salvar","Salvar:",['style'=>'color:white;']) !!}
    </div>
      {{Form::submit('Salvar',['class'=>'btn btn-success','style'=>'font-weight:bold;'])}}
    </div>

  </div>
</div>
@include('pack.template.mascaras')
