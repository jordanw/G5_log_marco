<div class="col-xs-12 col-sm-3">
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <div class="col-xs-12 col-sm-12">
        <div style="width: 100%;">
          <div class="fileinput fileinput-new" data-provides="fileinput"
          data-name="avatar" style="margin-top: 10px;">
          <div class="fileinput-new" style="margin-bottom: 15px">
            <img
            src="{{asset('assets/img/avatar-prefeitura.png')}}"
            class="m-t-xs img-responsive">
          </div>
          <div class="fileinput-preview fileinput-exists thumbnail"
          style="margin-bottom: 15px"></div>
          <div class="text-center">
            <h6>Tipos de Arquivo: jpeg, png, jpg, gif, svg - max:400px </h6>
            <span class="btn btn-success btn-file"><span
              class="fileinput-new">Selecionar logomarca</span><span
              class="fileinput-exists">Mudar</span>
              <input type="file" value=""/>
            </span>
            <a href="#" class="btn btn-danger fileinput-exists"
            data-dismiss="fileinput">Remover</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="col-xs-12 col-sm-9">
  <h3>Dados da Empresa</h3>
  <div class="form-group">
    <div class="col-xs-12 col-sm-5">
      {!! Form::label("nome","Nome da Empresa:") !!}
      {!! Form::text("nome",null,["class"=>"form-control"]) !!}
    </div>
    <div class="col-xs-12 col-sm-5">
      {!! Form::label("contato","Contato:") !!}
      {!! Form::text("contato",null,["class"=>"form-control"]) !!}
    </div>
    <div class="col-xs-12 col-sm-2">
      {!! Form::label("telefone","Telefone:") !!}
      {!! Form::text("telefone",null,["class"=>"form-control"]) !!}
    </div>
  </div>
</div>

<div class="col-xs-12 col-sm-9">
  <div class="form-group">

       <div class="col-xs-12 col-sm-4">
         {!! Form::label("email","Email:") !!}
         {!! Form::text("email",null,["class"=>"form-control"]) !!}
       </div>
       <div class="col-xs-12 col-sm-8">
         {!! Form::label("endereco","Endereço:") !!}
         {!! Form::text("endereco",null,["class"=>"form-control"]) !!}
       </div>
       <div class="col-xs-12 col-sm-2">
         {!! Form::label("numero","Número:") !!}
         {!! Form::text("numero",null,["class"=>"form-control"]) !!}
       </div>

       <div class="col-xs-12 col-sm-3">
         {!! Form::label("cep","CEP:") !!}
         {!! Form::text("cep",null,["class"=>"form-control"]) !!}
       </div>

       <div class="col-xs-12 col-sm-5">
         {!! Form::label("cidade","Cidade:") !!}
         {!! Form::text("cidade",null,["class"=>"form-control"]) !!}
       </div>
       <div class="col-xs-12 col-sm-2">
         {!! Form::label("uf","UF:") !!}
         {!! Form::select('uf', [
            "AC"=>"AC",
            "AL"=>"AL",
            "AM"=>"AM",
            "AP"=>"AP",
            "BA"=>"BA",
            "CE"=>"CE",
            "DF"=>"DF",
            "ES"=>"ES",
            "GO"=>"GO",
            "MA"=>"MA",
            "MT"=>"MT",
            "MS"=>"MS",
            "MG"=>"MG",
            "PA"=>"PA",
            "PB"=>"PB",
            "PR"=>"PR",
            "PE"=>"PE",
            "PI"=>"PI",
            "RJ"=>"RJ",
            "RN"=>"RN",
            "RO"=>"RO",
            "RS"=>"RS",
            "RR"=>"RR",
            "SC"=>"SC",
            "SE"=>"SE",
            "SP"=>"SP",
            "TO"=>"TO"
         ], null, ['placeholder' => 'Selecione...',"class"=>"form-control"]) !!}
       </div>

     </div>
   </div>

<div class="col-xs-12 col-sm-9">
  <div class="form-group">
    <div class="col-xs-12 col-sm-6">
      {!! Form::label("telefone","Telefone:") !!}
      {!! Form::text("telefone",null,["class"=>"form-control"]) !!}
    </div>
    <div class="col-xs-12 col-sm-2">
      {!! Form::label("sms","Sms:") !!}
      {!! Form::select("sms",[
        "1000"=>"1000",
        "2000"=>"2000",
        "5000"=>"5000",
         ], null, ['placeholder' => 'Selecione...',"class"=>"form-control"]) !!}
    </div>
    <div class="col-xs-12 col-sm-2">
      {!! Form::label("metas","Metas:") !!}
      {!! Form::select("metas",[
        "20"=>"20",
        "50"=>"50",
        "80"=>"80",
         ], null, ['placeholder' => 'Selecione...',"class"=>"form-control"]) !!}
    </div>
    <div class="col-xs-12 col-sm-2">
      {!! Form::label("logins","Usuários:") !!}
      {!! Form::select("logins",[
        "3"=>"3",
        "5"=>"5",
       ], null, ['placeholder' => 'Selecione...',"class"=>"form-control"]) !!}
    </div>
  </div>
</div>
