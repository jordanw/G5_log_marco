
<div class="col-xs-12 col-sm-12">
  <h3>Dados do Cliente</h3>
  <div class="form-group">
    <div class="col-xs-12 col-sm-6">
      {!! Form::label("nome","Nome:") !!}
      {!! Form::text("nome",null,["class"=>"form-control"]) !!}
    </div>
    <div class="col-xs-12 col-sm-6">
      {!! Form::label("email","Email:") !!}
      {!! Form::text("email",null,["class"=>"form-control"]) !!}
    </div>
    <div class="col-xs-12 col-sm-6">
      {!! Form::label("contato","Contato:") !!}
      {!! Form::text("contato",null,["class"=>"form-control"]) !!}
    </div>
    <div class="col-xs-12 col-sm-6">
      {!! Form::label("telefone","Telefone:") !!}
      {!! Form::text("telefone",null,["class"=>"form-control", 'id' => 'telefone']) !!}
    </div>
  </div>
</div>


<div class="col-xs-12 col-sm-12" style="margin-top: 3%;">
  <h3>Endereço</h3>
  <div class="form-group">
      <div class="col-xs-12 col-sm-3">
        {!! Form::label("cep","CEP:") !!}
        {!! Form::text("cep",null,["class"=>"form-control", "id"=>"cep"]) !!}
      </div>
      <div class="col-xs-12 col-sm-7">
        {!! Form::label("rua","Rua:") !!}
        {!! Form::text("rua",null,["class"=>"form-control", "id"=>"rua"]) !!}
      </div>
       <div class="col-xs-12 col-sm-2">
         {!! Form::label("numero","Número:") !!}
         {!! Form::text("numero",null,["class"=>"form-control", "id"=>"numero"]) !!}
       </div>
       <div class="col-xs-12 col-sm-6">
         {!! Form::label("bairro","Bairro:") !!}
         {!! Form::text("bairro",null,["class"=>"form-control", "id"=>"bairro"]) !!}
       </div>

       <div class="col-xs-12 col-sm-4">
         {!! Form::label("cidade","Cidade:") !!}
         {!! Form::text("cidade",null,["class"=>"form-control", "id"=>"cidade"]) !!}
       </div>
       <div class="col-xs-12 col-sm-2">
         {!! Form::label("uf","UF:") !!}
         {!! Form::select('uf', [
            "AC"=>"AC",
            "AL"=>"AL",
            "AM"=>"AM",
            "AP"=>"AP",
            "BA"=>"BA",
            "CE"=>"CE",
            "DF"=>"DF",
            "ES"=>"ES",
            "GO"=>"GO",
            "MA"=>"MA",
            "MT"=>"MT",
            "MS"=>"MS",
            "MG"=>"MG",
            "PA"=>"PA",
            "PB"=>"PB",
            "PR"=>"PR",
            "PE"=>"PE",
            "PI"=>"PI",
            "RJ"=>"RJ",
            "RN"=>"RN",
            "RO"=>"RO",
            "RS"=>"RS",
            "RR"=>"RR",
            "SC"=>"SC",
            "SE"=>"SE",
            "SP"=>"SP",
            "TO"=>"TO"
         ], null, ['placeholder' => 'Selecione...',"class"=>"form-control", "id"=>"estado"]) !!}
       </div>

     </div>
   </div>


   <div class="col-xs-12 col-sm-12" style="margin-top: 3%;">
       <h3>Dados Bancárioss</h3>
     <div class="form-group">
       <div class="col-xs-12 col-sm-3">
         {!! Form::label("nome_conta","Nome Conta:") !!}
         {!! Form::text("nome_conta",null,["class"=>"form-control"]) !!}
       </div>
       <div class="col-xs-12 col-sm-2">
         {!! Form::label("banco","Banco:") !!}
         {!! Form::text("banco",null,["class"=>"form-control"]) !!}
       </div>
       <div class="col-xs-12 col-sm-2">
         {!! Form::label("agencia","Agência:") !!}
         {!! Form::text("agencia",null,["class"=>"form-control"]) !!}
       </div>
       <div class="col-xs-12 col-sm-2">
         {!! Form::label("tipo_conta","Tipo:") !!}
         {!! Form::select('tipo_conta', [
            "Conta Corrente"=>"Conta Corrente",
            "Conta Poupança"=>"Conta Poupança",

         ], null, ['placeholder' => 'Selecione...',"class"=>"form-control", "id"=>"tipo_conta"]) !!}
       </div>
       <div class="col-xs-12 col-sm-3">
         {!! Form::label("conta","Conta:") !!}
         {!! Form::text("conta",null,["class"=>"form-control"]) !!}
       </div>
     </div>
   </div>



     <div class="form-group">
       <div class="col-sm-12">
   {{Form::submit('Salvar',["class"=>"btn btn-success btn-sec"])}}
 </div>
 </div>
