@extends('pack.template.templateGestor')
@section('content')
  @if ($errors->any())
    <div>
      @foreach ($errors->all() as $error)
        <li><i>{{$error}}</i></li>
      @endforeach
    </div>

  @endif


  <div class="row">
    <h2>{{$nomePagina}}</h2>


    <div class="col-md-4">
      <div class="ibox float-e-margins">

        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Cod</th>
                  <th>Nome </th>
                  <th>Telefone </th>


                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{{$adiantamento->id}}</td>
                  <td>{{$adiantamento->nome}}</td>
                  <td>{{$adiantamento->telefone}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-8">
      <div class="ibox float-e-margins">

        {!! Form::open(array('route' => ['adiantamentos.update',$adiantamento->id], 'name'=>'form' , 'method' => 'PUT','files' => true)) !!}
        <div class="col-xs-12 col-sm-12">
          @include('pack.adiantamentos.clientes.form._formBase_okEdit')
          {!! Form::close() !!}
        </div>

        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Data</th>
                  <th class="ctr">Valor </th>
                  <th class="ctr">Total </th>
                  <th style="text-align: center">Ações</th>
                </tr>
              </thead>
              <tbody>
              @foreach ($adiantamento->adiantamento_clientes as $ad)
                <tr>
                  <td>{{date("d/m/Y", strtotime($ad->data))}}</td>
                  <td class="rgt">R${{ number_format($ad->valor_adiantamento_cliente,2,',','.') }}</td>
                  <td class="rgt">R${{ number_format($ad->valor_adiantamento_total_cliente,2,',','.') }}</td>
                  <td class="ctr"><a href="#">editar</a> | <a href="#">excluir</a></td>


                </tr>
              @endforeach
              </tbody>
            </table>

          </div>
          </div>
            <div class="col-sm-12 col-xs-12">
              <div class="col-sm-6 col-xs-6"><h5>Total Compras</h5></div>
              <div class="col-sm-6 col-xs-6 right"><h4>R$ {{ number_format($totalAdiantCliente,2,',','.') }}</h4></div>
          </div>
        </div>
      </div>
    </div>
  </div>



@stop
