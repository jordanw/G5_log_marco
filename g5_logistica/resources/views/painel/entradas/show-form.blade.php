<?php
use Carbon\Carbon;
?>

<div class="panel panel-default col-md-12" style="margin-top:10px;">
  @include('layouts.session')

  <div class="col-md-12">

    <div class="col-lg-4 col-md-offset-4">
      <div class="widget-head-color-box blue-bg p-lg text-center">
        <div class="m-b-md">
          <h2 class="font-bold no-margins">{{$user->name}}</h2>
          {{-- @include('painel.templates.userLevel') --}}
        </div>
        @if($user->image == null )
            <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/painel/imgs/no-image.png')}}" style="max-width:156px;"></div>
        @else
          <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/uploads/entradas/'.$user->image)}}" style="max-width:156px;"></div>
        @endif
      </div>
      <div class="widget-text-box">
        <h4><strong>Nome:</strong> {{$user->nome}}</h4>
        <h4><strong>CPF:</strong> {{$user->cpf}}</h4>
        <h4><strong>Identidade:</strong> {{$user->identidade}}</h4>
        <h4><strong>Telefone:</strong> {{$user->telefone}}</h4>
        @if ($user->sexo == 'M')
          <h4><strong>Sexo: </strong> Masculino</h4>
          @else
            <h4><strong>Sexo: </strong> Feminino</h4>
        @endif
        <h4><strong>Tipo:</strong> Pessoa {{$user->tipo}}</h4>
        <h4><strong>Responsável: </strong>{{$user->responsavel}}</h4>
        <h4><strong>CNPJ: </strong>{{$user->cnpj}}</h4>
        <h4><strong>Email: </strong>{{$user->email}}</h4>



        @if(Auth::user()->nivel == 1)
          <div class="rgt">
            {!! Form::open(['route' => ['entradas.destroy', $user->id], 'method' => 'DELETE']) !!}
            <div class="form-group">

              <a class="btn btn-warning" title="Editar: {{$user->nome}}"   href='{{route('entradas.edit',$user->id)}}'><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
              {!! Form::button('<i class="fa fa-trash-o fa-2x"></i> ', ['class' => 'btn btn-danger', 'role' => 'button', 'type' => 'submit', 'onclick'=>"return confirm('Tem certeza que deseja excluir o visitante: $user->nome ?')"]) !!}

          </div>
            {!! Form::close() !!}
          </div>
        @endif()
      </div>
    </div>
</div>
