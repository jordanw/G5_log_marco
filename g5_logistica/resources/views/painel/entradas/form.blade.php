<?php
use Carbon\Carbon;
?>
@if( isset($user) )
  {!! Form::model($user, ['route' => ['entradas.update', $user->id], 'class' => 'form form-search form-ds',
  'files' => true, 'method' => 'PUT']) !!}
  <input type="hidden"  value="{{$disabled = 1}}"/>

@else
  <input type="hidden"  value="{{$disabled = 0}}"/>
  {!! Form::open(['route' => 'entradas.store', 'class' => 'form form-search form-ds', 'files' => true]) !!}
@endif
<div class="panel panel-default col-md-12">
  @include('layouts.session')
  <div class="col-md-12">

    <div class="row">
      <div class="col-md-12">
        @if ($disabled == 1)
          <h4>Editar Entrada: {{$user->nome}}</h4>
        @else
          <h4>Nova Entrada</h4>
        @endif
      </div>



        <div class="col-sm-12">

            <div class="title-pg text-center">
              <h2 class="title-pg"><b>Cadastro</b></h2>
            </div>

        </div>

        <div class="col-sm-12 col-md-6">

          <div class="title-pg text-center">
            <h3 class="title-pg"><b>Tirar Foto</b></h3>
          </div>
          <div class="col-sm-12 col-md-6 mB5">
            <div id="my_camera"></div>
            <input class="btn btn-danger mT5" type=button value="Webcam" onClick="configure()">
            <input class="btn btn-default mT5" type=button value="Click!" onClick="take_snapshot()">
            {{ Form::hidden('imagem', '', array('id' => 'imagem')) }}
          </div>

          <div class="col-sm-12 col-md-6">
            <div id="results" value="imagem"></div>
          </div>
        </div>

      <div class="row">
        <div class="title-pg text-center">
          <h3 class="title-pg"><b>Dados do Visitante</b></h3>
        </div>

        <div class="col-md-6 form-group">
          <label for="nome">Nome</label>
          {!! Form::text('nome', null, ['class' => 'form-control', 'id'=>'first_name']) !!}
        </div>
        <div class="col-md-2 form-group">
          <label for="cpf">CPF <small></small></label>
          {!! Form::text('cpf', null, ['class' => 'form-control', 'id'=>'cpf']) !!}
          {{-- <input id="cpf" type="telefone" class="form-control telefone" name="cpf" value=""  maxlength="15" required> --}}
        </div>
        <div class="col-md-2 form-group">
          <label for="cpf">Identidade <small></small></label>
          {!! Form::text('identidade', null, ['class' => 'form-control', 'id'=>'cpf']) !!}
          {{-- <input id="cpf" type="telefone" class="form-control telefone" name="cpf" value=""  maxlength="15" required> --}}
        </div>
        <div class="col-md-2 form-group">
          <label for="telefone">Telefone</label>
          @if( isset($user) )
            <input id="telefone" type="telefone" class="form-control telefone" name="telefone" value="{{$user->telefone}}"  maxlength="15">
          @else
            <input id="telefone" type="telefone" class="form-control telefone" name="telefone" value="{{ old('telefone') }}"  maxlength="15" required>
          @endif
        </div>

        <div class="col-md-1 form-group">
          <label for="sexo">Sexo</label>
          {!! Form::select('sexo', ['M' => 'M', 'F' => 'F'], null, ['class' => 'form-control','placeholder' => '']) !!}
        </div>
        <div class="col-md-1 form-group">
          <label for="tipo">Tipo</label>
          {!! Form::select('tipo', ['Física' => 'Física', 'Jurídica' => 'Jurídica'], null, ['class' => 'form-control','placeholder' => '']) !!}
        </div>
        <div class="col-md-4 form-group">
          <label for="responsavel">Responsável <small>(PJ - Preencher CPF)</small></label>
          {!! Form::text('responsavel', null, ['class' => 'form-control', 'id'=>'first_name']) !!}
        </div>
        <div class="col-md-3 form-group">
          <label for="cnpj">CNPJ <small>(PJ)</small></label>
          {!! Form::text('cnpj', null, ['class' => 'form-control', 'id'=>'cnpj']) !!}
        </div>
        <div class="col-md-3 form-group">
          <label for="email_empresa">Email</label>
          {!! Form::email('email', null, ['class' => 'form-control', 'id'=>'email']) !!}
        </div>


        <script type="text/javascript">

        /* Máscaras ER */
        function mascaratel(o,f){
          v_obj=o
          v_fun=f
          setTimeout("execmascara()",1)
        }
        function execmascara(){
          v_obj.value=v_fun(v_obj.value)
        }
        function mtel(v){
          v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
          v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
          v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
          return v;
        }
        function id( el ){
          return document.getElementById( el );
        }
        window.onload = function(){
          id('telefone').onkeyup = function(){
            mascaratel( this, mtel );
          }
        }
        </script>

      </div>
    </div>
  </div>


  <div class="form-group col-md-12">
    {!! Form::submit('Enviar', ['class' => 'btn btn-primary', 'style' => 'float:right;']) !!}
  </div>
  {!! Form::close() !!}
