@extends('layouts.layout.template')
@section('title', 'Usuários')

@section('content')

  <!-- Search for small screen -->

  @include('layouts.breadcrumb-fornecedores')



  <div class="section">
    @include('layouts.success')
    @include('layouts.session')

    <div class="col-md-12">

      <div class="col-md-6 text-left">
        <a href="{{route('entradas.create')}}" class="btn btn-primary">
          <span class="glyphicon glyphicon-plus"></span>
          Cadastrar
        </a>
      </div>
      {{-- <div class="col-md-6 form-search text-right">
        {!! Form::open(['route' => 'entradas.search', 'class' => 'form form-inline']) !!}
        {!! Form::text('key-search', null, ['class' => 'form-control', 'placeholder' => 'Nome:']) !!}
        {!! Form::submit('Procurar', ['class' => 'btn btn-warning']) !!}
        {!! Form::close() !!}
      </div> --}}

    </div>

    <div class="col-md-12">
      @if( Session::has('success') )
        <div class="alert alert-success hide-msg" style="float: left; width: 100%; margin: 10px 0px;">
          {{Session::get('success')}}
        </div>
      @endif
    </div>
    <div class="col-md-12" style="margin-top:10px;">
      <div class="panel panel-default">
        <div class="table-responsive">
          <table class="table table-striped table-hover table-responsive" id="myTable">
            <thead>
              <tr>
                <th>Cod</th>
                <th>Entrada</th>
                <th>Nome</th>
                <th>Nascimento</th>
                <th>Email</th>
                <th>Telefone</th>
                <th>Bairro</th>
                <th>Cep</th>
                <th class="text-center" class="center">Ações</th>
              </tr>
            </thead>

            <tbody>
              @forelse($data as $user)
                <tr>
                  <td>{{$user->id}}</td>
                  <td style="text-align: center; vertical-align: middle">
                    <a href="{{route('criar.nota',$user->id)}}"
                      class="btn btn-default" title="Nova Entrada">
                      <i class="fa fa-plus" aria-hidden="true" style="width: 15px;"></i>
                    </a>
                  </td>
                  <td>{{$user->nome}}</td>
                  <td>@if ($user->nascimento == null)
                    {!! 'Não Informada' !!}
                  @else
                    {!! \Carbon\Carbon::parse($user->nascimento)->format('d/m/Y') !!}
                  @endif
                </td>
                <td>{{$user->email}}</td>
                <td>{{$user->telefone}}</td>
                <td>{{$user->bairro}}</td>
                <td>{{$user->cep}}</td>

                <td class="text-center" style="width:16%;">
                  {{Form::open(['route'=>['entradas.destroy',$user->id], 'method'=>'DELETE'])}}
                  <a class="btn btn-default btn-sm" title="Editar: {{$user->nome}}"   href='{{route('entradas.edit',$user->id)}}'><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                  <a class="btn btn-default btn-sm" title="Ver: {{$user->nome}}"   href='{{route('entradas.show',$user->id)}}'><i class="fa fa-eye" aria-hidden="true"></i></a>

                  {{ Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', ['class' => 'btn btn-default btn-sm', 'title'=>'Excluir', 'role' => 'button', 'type' => 'submit', 'onclick'=>"return confirm('Tem certeza que deseja excluir o Entrada: $user->nome?')"]) }}
                  {{Form::close()}}
                </td>
              </tr>
            @empty
              <div class="col-md-12 text-center">
                <h1>
                  <p>Nenhum Fornecedor Cadastrado</p>
                </h1>
              </div>
            @endforelse
          </tbody>
        </div>
      </div>
    </div>

  @endsection
  @push('stack-js')
    @include('layouts.paginacaoPontos')
  @endpush
