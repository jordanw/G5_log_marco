@extends('layouts.layout.template')

@section('content')


        <div class="row">
            <div class="col-md-6 text-left">
                <h4 class="breadcrumb-title">{{$user->name or 'Novo Entrada'}}</h4>
            </div>
            <div class="col-md-6">
                <ol class="breadcrumb hidden-xs text-right mt1">
                    <li><a href="{{url('/painel')}}">Home</a></li>
                    <li><a href="{{url('/painel/escrtitores')}}">Entradas</a></li>
                    <li class="active"><a href="#">{{$user->name or 'Novo Entrada'}}</a></li>
                </ol>
            </div>
        </div>

    <div class="section">
        {{-- <div class="container">
        @if( isset($user) )
        @include('layouts.userImageCard')
    @endif
</div> --}}

<div class="col s12 left mtop3">
    <input class="btn btn-primary" action="action" onclick="window.history.go(-1); return false;" type="button" value="Voltar" />
</div><!--Content DinÃ¢mico-->
</div><!--Content DinÃ¢mico-->

<div class="content-din mtop5">
    <div class="section">

        @include('painel.entradas.show-form')

    </div><!--Content DinÃ¢mico-->
</div>
</div>

@endsection

@push('stack-css')
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{asset('assets/js/plugins/prism/prism.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{asset('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{asset('assets/js/plugins/chartist-js/chartist.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{asset('assets/js/plugins/dropify/css/dropify.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
@endpush

@push('stack-js')
    <script type="text/javascript">
    $('.datepicker').pickadate({
        monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        weekdaysFull: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabádo'],
        weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        today: 'Hoje',
        clear: 'Limpar',
        close: 'Pronto',
        labelMonthNext: 'Próximo mês',
        labelMonthPrev: 'Mês anterior',
        labelMonthSelect: 'Selecione um mês',
        labelYearSelect: 'Selecione um ano',
        selectMonths: true,
        format: 'dd-mm-yyyy',
        max: new Date(),
        autoclose: true,
        selectYears: 80,

    });

    // var diaSemana = [ 'Domingo', 'Segunda-Feira', 'Terca-Feira', 'Quarta-Feira', 'Quinta-Feira', 'Sexta-Feira', 'Sabado' ];
    // var mesAno = [ 'Janeiro', 'Fevereiro', 'Marco', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro'           , 'Dezembro' ];
    // var data = new Date();
    // var hoje = diaSemana[data.getDay()] + ', ' + mesAno[data.getMonth()] + ' de ' + data.getFullYear();
    // $("#dataPesquisa").attr("value", hoje);
    // $(".datepicker").pickadate({
    //     monthsFull: mesAno,
    //     monthsShort: [ 'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez' ],
    //     weekdaysFull: diaSemana,
    //     weekdaysShort: [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab' ],
    //     weekdaysLetter: [ 'D', 'S', 'T', 'Q', 'Q', 'S', 'S' ],
    //     selectMonths: true,
    //     selectYears: true,
    //     clear: false,
    //     format: 'dddd/mm/yyyy',
    //     today: "Hoje",
    //     close: "X",
    //     min: new Date(data.getFullYear() - 1, 0, 1),
    //     max: new Date(data.getFullYear() + 1, 11, 31),
    //     closeOnSelect: true
    // });
    //
    // $("#dataPesquisa").click(function (event) {
    //     event.stopPropagation();
    //     $(".datepicker").first().pickadate("picker").open();
    // });
    </script>

@endpush
