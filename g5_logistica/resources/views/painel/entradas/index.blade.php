@extends('layouts.layout.template')
@section('title', 'Usuários')

@section('content')

  <!-- Search for small screen -->

  @include('layouts.breadcrumb-entradas')


  <div class="section">
    @include('layouts.success')
    @include('layouts.session')

    @include('layouts.cadastrar-entrada')

    <div class="col-md-12">
      @if( Session::has('success') )
        <div class="alert alert-success hide-msg" style="float: left; width: 100%; margin: 10px 0px;">
          {{Session::get('success')}}
        </div>
      @endif
    </div>
    <div class="col-md-12" style="margin-top:10px;">
      <div class="panel panel-default">
        <div class="table-responsive">
          <table class="table table-striped table-hover table-responsive" id="myTable">
            <thead>
              <tr>
                <th>Cod</th>
                <th>Acesso</th>
                <th>Nome</th>
                <th>Foto</th>
                <th>Telefone</th>
                <th>Tipo</th>
                <th>Cadastrado por:</th>
                <th>Data | Hora Cadastro</th>
                <th class="text-center" class="center">Ações</th>
              </tr>
            </thead>

            <tbody>
              @forelse($data as $user)
                <tr>
                  <td>{{$user->id}}</td>
                  <td style="text-align: center; vertical-align: middle">
                    <a href="{{route('acessos.create',$user->id)}}"
                      class="btn btn-warning" title="Novo Acesso">
                      <i class="fa fa-external-link-square fa-fw fa-3x" aria-hidden="true"></i>
                    </a>
                  </td>
                  <td>{{$user->nome}}</td>
                  <td style="text-align: center; vertical-align: middle, max-width: 50px">
                    @if($user->image == null )
                      <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/painel/imgs/no-image.png')}}" style="max-width:96px;"></div>
                    @else
                      <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/uploads/entradas/'.$user->image)}}" style="max-width:96px;"></div>
                    @endif
                  </td>
                  <td>{{$user->telefone}}</td>
                  <td>{{$user->tipo}}</td>

                  <td>
                    {!! $user->user->name!!}
                  </td>
                  <td>
                    {!! \Carbon\Carbon::parse($user->created_at)->format('d/m/Y | H:i') !!}
                  </td>

                  <td class="text-center" style="width:16%;">
                    {{Form::open(['route'=>['entradas.destroy',$user->id], 'method'=>'DELETE'])}}
                    <a class="btn btn-default btn-sm" title="Editar: {{$user->nome}}"   href='{{route('entradas.edit',$user->id)}}'><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                    <a class="btn btn-default btn-sm" title="Ver: {{$user->nome}}"   href='{{route('entradas.show',$user->id)}}'><i class="fa fa-eye" aria-hidden="true"></i></a>

                    {{ Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', ['class' => 'btn btn-default btn-sm', 'title'=>'Excluir', 'role' => 'button', 'type' => 'submit', 'onclick'=>"return confirm('Tem certeza que deseja excluir o visitante: $user->nome?')"]) }}
                    {{Form::close()}}
                  </td>
                </tr>
              @empty
                <div class="col-md-12 text-center">
                  <h1>
                    <p>Nenhum Visitante Cadastrado</p>
                  </h1>
                </div>
              @endforelse
            </tbody>
          </div>
        </div>
      </div>

    @endsection
    @push('stack-js')
      @include('layouts.paginacaoPontos')
    @endpush
