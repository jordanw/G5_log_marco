<?php
use Carbon\Carbon;
?>

@include('painel.manutencaoVeiculos.form.modal-local_manutencao')
@include('painel.manutencaoVeiculos.form.modal-peca_manutencao')

@if( isset($editar) or Request::is('*/editar'))
  {!! Form::model($user, ['route' => ['manutencao.veiculos.update', $user->id],
  'class' => 'form form-search form-ds', 'files' => true, 'method' => 'PUT']) !!}
  <input type="hidden"  value="{{$disabled = 1}}"/>

@else
  <input type="hidden"  value="{{$disabled = 0}}"/>
  {!! Form::open(['route' => ['manutencao.veiculos.store', $user->id],  'class' => 'form form-search form-ds', 'files' => true]) !!}
@endif

<div class="col-xs-12 col-sm-12">
  <div class="row">

    <div class="form-group">

      <div class="row">
        <div class="col-sm-4">
          <div class="row">
            <div class="col-sm-12">
              {!! Form::label("os_num","Os_Num:") !!}
              {!! Form::text("os_num",null,["class"=>"form-control"]) !!}
            </div>
            <div class="col-sm-12">
              {!! Form::label("tipo_manutencao","Tipo Manutenção:") !!}
              {!! Form::select('tipo_manutencao', [
                "Preventiva"=>"Preventiva",
                "Corretiva"=>"Corretiva",
              ], null, ['placeholder' => 'Selecione...',"class"=>"form-control"]) !!}
            </div>



            <div class="col-sm-12">
              {!! Form::label("falha","Falha Ocorrida:") !!}
              <div class="input-group">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" data-toggle="modal" data-target="#myModal">  <i class="fa fa-plus fa-lg"></i></button>
                </span>
                {!! Form::select('falha',$peca
                ,
                null, ['placeholder' => 'Selecione...',"class"=>"form-control", "onchange"=>"setTextField(this)"]) !!}
              </div><!-- /input-group -->
            </div>


          </script>






          <div class="col-sm-12">
            {!! Form::label("local","Local Manutenção:") !!}
            <div class="input-group">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button" data-toggle="modal" data-target="#myModal2">  <i class="fa fa-plus fa-lg"></i></button>
              </span>
              <!-- -->
              {!! Form::select('local', $local, null, ['placeholder' => 'Selecione...',"class"=>"form-control"]) !!}
            </div><!-- /input-group -->
          </div>

          <div class="col-sm-12">
            {!! Form::label("tempo_estimado","Tempo Estimado:") !!}
            {!! Form::select('tempo_estimado', [
              "1"     =>"até 1 Hora",
              "2"     =>"até 2 Horas",
              "4"     =>"até 4 Horas",
              "6"     =>"até 6 Horas",
              "8"     =>"até 8 Horas",
              "10"    =>"até 10 Horas",
              "25"    =>"até 25 Horas",
              "50"    =>"até 50 Horas",
              "100"   =>"até 100 Horas",
              "150"   =>"até 150 Horas",
              "200"   =>"até 200 Horas",
              "500"   =>"até 500 Horas",
              "NS"    =>"não sabe / não informou",
            ], null, ['placeholder' => 'Selecione...',"class"=>"form-control"]) !!}
          </div>
        </div>
      </div>

      <div class="col-sm-8">
        <div class="row">
          <div class="col-sm-12">
            {!! Form::label("observacao","Observação:") !!}
            {!! Form::textarea("observacao",null,["class"=>"form-control", 'id'=>'editor1']) !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-sm-12 form-group">
  {{Form::submit('Salvar',['class'=>'btn btn-primary','style'=>'font-weight:bold; margin-top:3%;'])}}
</div>

</div>

{!! Form::close() !!}



{{-- @include('layouts.mascaras') --}}
