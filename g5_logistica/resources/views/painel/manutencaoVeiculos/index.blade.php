@extends('layouts.layout.template')
@push('ck-editor')
  <script src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
@endpush
@section('content')
  @include('layouts.breadcrumb-painel')

  <div class="container">
    <!-- Button trigger modal -->


  </div>



  <div class="col-md-12">
    @if( Session::has('success') )
      <div class="alert alert-success hide-msg" style="float: left; width: 100%; margin: 10px 0px;">
        {{Session::get('success')}}
      </div>
    @endif
  </div>

  @include('layouts.cadastrar-veiculos')
  @include('painel.entradasVeiculos.formManutencao')
</div>




@stop


@push('stack-js')

  <script type="text/javascript">
  CKEDITOR.replace( 'editor1', {
    language: 'pt-BR',
    uiColor: '#dedfe0'
  });
  CKEDITOR.replace( 'editor2', {
    language: 'pt-BR',
    uiColor: '#dedfe0'
  });
  CKEDITOR.replace( 'editor3', {
    language: 'pt-BR',
    uiColor: '#dedfe0'
  });
  </script>


  @include('layouts.paginacaoPontos')

@endpush
