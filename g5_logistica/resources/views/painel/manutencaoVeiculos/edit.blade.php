@extends('layouts.layout.template')

@php
  use Carbon\Carbon;
@endphp

@section('content')


  <div class="row">
    <div class="col-md-6 text-left">
      <h4 class="breadcrumb-title">{{$user->nome or 'Nova Nota de Entrega'}} </h4>
    </div>
    <div class="col-md-6">
      <ol class="breadcrumb hidden-xs text-right mt1">
        <li><a href="{{url('/painel')}}">Home</a></li>
        <li><a href="{{url('/painel/escrtitores')}}">Entradas</a></li>
        <li class="active"><a href="#">{{$user->nome or 'Nova Nota de Entrega'}}</a></li>
      </ol>
    </div>
  </div>

  <div class="section">
    @include('layouts.success')
    @include('layouts.session')
  </div>


  {{-- <div class="container">
  @if( isset($user) )
  @include('layouts.userImageCard')
@endif
</div> --}}
<div class="col-md-4" style="margin-top: 3%;">
  <div class="ibox float-e-margins">

    <div class="ibox-content">
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Cod</th>
              <th>Nome </th>
              <th>Telefone </th>


            </tr>
          </thead>
          <tbody>
            @isset($data)
              <tr>
                <td>{{$user->entrada->id}}</td>
                <td>{{$user->entrada->nome}}</td>
                <td>{{$user->entrada->telefone}}</td>
              </tr>
            @else
              <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->nome}}</td>
                <td>{{$user->telefone}}</td>
              </tr>
            @endisset
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="col-md-8">
  <div class="row">
    <div class="col-lg-4 col-sm-4">
      <div class="circle-tile">
        <a href="#">
          <div class="circle-tile-heading dark-blue">
            <i class="fa fa-usd fa-fw fa-3x"></i>
          </div>
        </a>
        <div class="circle-tile-content dark-blue">
          <div class="circle-tile-description text-faded">
            Negócios
          </div>
          <div class="circle-tile-number text-faded">
            {{ number_format($user->sum('valor_total'),2,',','.') }}
            <span id="sparklineA"></span>
          </div>
          <a href="#" class="circle-tile-footer">Em R$
            <i class="fa fa-chevron-circle-right"></i></a>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-sm-4">
      <div class="circle-tile">
        <a href="#">
          <div class="circle-tile-heading blue">
            <i class="fa fa-bell fa-fw fa-3x"></i>
          </div>
        </a>
        <div class="circle-tile-content blue">
          <div class="circle-tile-description text-faded">
            Notas Abertas
          </div>
          <div class="circle-tile-number text-faded">
            {{$user->where('ativo', '1')->count()}}
          </div>
          <a href="#" class="circle-tile-footer">
            <i class="fa fa-chevron-circle-right"></i></a>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-sm-4">
      <div class="circle-tile">
        <a href="#">
          <div class="circle-tile-heading orange">
            <i class="fa fa-money fa-fw fa-3x"></i>
          </div>
        </a>
        <div class="circle-tile-content orange">
          <div class="circle-tile-description text-faded">
            Negócios Ativos
          </div>
          <div class="circle-tile-number text-faded">
            {{ number_format($user->where('ativo', '1')
            ->sum('valor_total'),2,',','.') }}
          </div>
          <a href="#" class="circle-tile-footer">Em R$ <i class="fa fa-chevron-circle-right"></i></a>
        </div>
      </div>
    </div>



  </div>

</div>


<div class="col-md-12">
  <div class="ibox float-e-margins">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="panel-head">
          <h3>Registrar Entrada</h3>
        </div>
        <div class="col-xs-12 col-sm-12">
          @include('painel.nota-entrega.form._formBase_nota')

        </div>
      </div>

    </div>


    
    {{-- <div class="col-sm-12 col-xs-12">
    <div class="col-sm-6 col-xs-6"><h5>Total Compras</h5></div>
    <div class="col-sm-6 col-xs-6 right"><h4>R$ {{ number_format($totalAdiantCliente,2,',','.') }}</h4></div>
  </div> --}}
</div>
</div>

@endsection

@push('stack-css')
  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="{{asset('assets/js/plugins/prism/prism.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{asset('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{asset('assets/js/plugins/chartist-js/chartist.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{asset('assets/js/plugins/dropify/css/dropify.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
@endpush

@push('stack-js')

  @include('layouts.paginacaoPontos')

  <script type="text/javascript" src="{{asset('/assets/js/jquery.maskedinput.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('/assets/js/jquery.maskMoney.js')}}"></script>
  <script type="text/javascript">
  jQuery(function($){

    $("#cpf").mask("999.999.999-99");

    $("#cnpj").mask("99.999.999/9999-99");
    $("#datetimepicker").mask("99/99/9999");
    $("#valor").maskMoney();


  });




  </script>



@endpush
