<?php
use Carbon\Carbon;
?>
@if( isset($user) )
  {!! Form::model($user, ['route' => ['entradas.update', $user->id], 'class' => 'form form-search form-ds', 'files' => true, 'method' => 'PUT']) !!}
  <input type="hidden"  value="{{$disabled = 1}}"/>

@else
  <input type="hidden"  value="{{$disabled = 0}}"/>
  {!! Form::open(['route' => 'entradas.store', 'class' => 'form form-search form-ds', 'files' => true]) !!}
@endif
<div class="panel panel-default col-md-12">
  @include('layouts.session')
  <div class="col-md-12">

    <div class="row">
      <div class="col-md-12">
        <h4>Entrada</h4>
      </div>
      <div class="col-md-8 form-group">
        <label for="first_name">Nome</label>
        {!! Form::text('nome', null, ['class' => 'form-control', 'id'=>'first_name']) !!}
      </div>
      <div class="col-md-4 form-group">
        <label for="email_empresa">Email</label>
        {!! Form::email('email', null, ['class' => 'form-control', 'id'=>'email']) !!}
      </div>
      <div class="col-md-4 form-group">
        <label for="sexo">Sexo</label>
        {!! Form::select('sexo', ['M' => 'M', 'F' => 'F'], null, ['class' => 'form-control','placeholder' => '']) !!}
      </div>
      <div class="col-md-4 form-group">
        <label for="email">Telefone</label>
        @if( isset($user) )
          <input id="telefone" type="telefone" class="form-control telefone" name="telefone" value="{{$user->telefone}}"  maxlength="15">
        @else
          <input id="telefone" type="telefone" class="form-control telefone" name="telefone" value="{{ old('telefone') }}"  maxlength="15" required>
        @endif
      </div>
      <div class="col-md-4 form-group">
        <label class="ano" for="nascimento">Nascimento</label>
        {{-- @if( isset($user) )
          {!! Form::date('nascimento',Carbon::parse($user->nascimento)->format('d/m/Y'),['class' => 'form-control']) !!}
          {!! Form::date('nascimento',$user->nascimento,['class' => 'form-control']) !!}
        @else --}}
        {{-- @endif --}}

          {!! Form::date('nascimento',null,['class' => 'form-control', 'placeholder'=>'Nascimento','id'=>'datetimepicker']) !!}
      </div>




      @include('layouts.enderecovue')



      <script type="text/javascript">
      /* Máscaras ER */
      function mascaratel(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascara()",1)
      }
      function execmascara(){
        v_obj.value=v_fun(v_obj.value)
      }
      function mtel(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
        return v;
      }
      function id( el ){
        return document.getElementById( el );
      }
      window.onload = function(){
        id('telefone').onkeyup = function(){
          mascaratel( this, mtel );
        }
      }
      </script>




      {{-- @if( isset($user) )
      <div class="col-md-6">
      <label for="test5">Caravana de Cadastro</label>
      {{ Form::select('caravana_id', $caravanas, $user->caravana->id, ['class' => 'form-control']) }}
    </div>
  @endif

  @if ($disabled == 0)
  <div class="col-md-6">
  <label for="test5">Caravana de Cadastro</label>
  {{ Form::select('caravana_id', $caravanas, ['class' => 'form-control']) }}
</div>

<div class="col-md-6">
<label for="test5">Email de Boas Vindas</label>
<input name="send_mail" type="checkbox" id="test5" value="1" />
</div>
@endif --}}
</div>


</div>


<div class="form-group col-md-12">
  {!! Form::submit('Enviar', ['class' => 'btn btn-primary', 'style' => 'float:right;']) !!}
</div>
{!! Form::close() !!}
