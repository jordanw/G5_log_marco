@extends('layouts.layout.template')
@section('content')
  @include('layouts.breadcrumb-painel')

  <div class="row">
    @include('layouts.status-delete-msg')


    <div class="col-lg-3">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <span class="label label-warning pull-right ty10"><i class="fa fa-external-link-square fa-fw fa-3x"></i></span>
          <h5>Acessos Ativos</h5>
        </div>
        <div class="ibox-content">
          <h1 class="no-margins">{{ $acessosAtivos }}</h1>
          {{-- <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div> --}}
          <small>N<sup>o</sup>  </small>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <span class="label label-success pull-right ty10"><i class="fa fa-users fa-fw fa-3x"></i></span>
          <h5>Visitantes Cadastrados</h5>
        </div>
        <div class="ibox-content">
          <h1 class="no-margins">{{ $totalVisita }}</h1>
          <small>Física ou Jurídica</small>
        </div>

        {{-- <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div> --}}
      </div>
    </div>

    <div class="col-lg-3">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <span class="label label-info pull-right ty10"><i class="fa fa-check-square-o fa-fw fa-3x"></i></span>
          <h5>Acessos Concluídos</h5>
        </div>
        <div class="ibox-content">
          <h1 class="no-margins">{{ $user4 }}</h1>
          {{-- <div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div> --}}
          <small>N<sup>o</sup></small>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <span class="label label-primary pull-right ty10"><i class="fa fa-universal-access fa-fw fa-3x"></i></span>
          <h5>Total Acessos</h5>
        </div>
        <div class="ibox-content">
          <h1 class="no-margins">{{$totalAcessos}}</h1>
          {{-- <div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i></div> --}}
          <small>N<sup>o</sup></small>
        </div>
      </div>
    </div>
  </div>




  @include('layouts.cadastrar-entrada')
  <div class="row">
    <div class="col-md-12" style="margin-top:10px;">
      <div class="panel panel-default">
        <div class="table-responsive">
          <h1 class="text-center">Acessos Ativos</h1>
          <table class="table table-striped table-hover table-responsive" id="myTable3">
            <thead>
              <tr>
                <th>Cod</th>
                <th class="text-center" class="center">Concluir</th>
                <th>Foto</th>
                <th>Nome</th>
                <th>Local</th>
                <th>Ap</th>
                <th>Tipo</th>
                <th>Autorizado:</th>
                <th>Data | Hora</th>
                <th>Tempo Estimado</th>
              </tr>
            </thead>

            <tbody>
              @forelse($dataAcesso as $user)
                <tr>
                  <td>{{$user->id}}</td>
                  <td class="text-center">
                    {{Form::open(['route'=>['acessos.destroy',$user->id], 'method'=>'DELETE'])}}

                    {{ Form::button('<i class="fa fa-check fa-fw fa-3x" aria-hidden="true"></i>', ['class' => 'btn btn-danger', 'title'=>'Concluir', 'role' => 'button', 'type' => 'submit', 'onclick'=>"return confirm('Tem certeza que deseja excluir o Acesso de: {$user->entrada->nome}?')"]) }}
                    {{Form::close()}}
                  </td>
                  <td style="text-align: center; vertical-align: middle, max-width: 50px">
                    @if($user->entrada->image == null )
                      <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/painel/imgs/no-image.png')}}" style="max-width:96px;"></div>
                    @else
                      <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/uploads/entradas/'.$user->entrada->image)}}" style="max-width:96px;"></div>
                    @endif
                  </td>
                  <td>{{$user->entrada->nome}}</td>
                  <td>{{$user->local_acesso}}</td>
                  <td>{{$user->local_especifico}}</td>
                  <td>{{$user->tipo_fornecedor}}</td>
                  <td>{{$user->concedido_acesso_por}}</td>
                  <td>{{date('H:i:s | d/m/Y', strtotime($user->date_concedido_acesso))}}</td>
                  @if ($user->tempo_estimado == null)
                    <td>Não Informado</td>
                  @else
                    <td>{{date('H:i:s | d/m/Y', strtotime($user->tempo_estimado))}}</td>
                  @endif


                </tr>
              @empty
                <div class="col-md-12 text-center">
                  <h1>
                    <p>Nenhum Acesso Ativo</p>
                  </h1>
                </div>
              @endforelse
            </tbody>
          </div>
        </div>
      </div>
    </div>
  </div>



  <div class="row">
    <div class="col-md-12" style="margin-top:10px;">
      <div class="panel panel-default">
        <div class="table-responsive">
          <table class="table table-striped table-hover table-responsive" id="myTable">
            <h1 class="text-center">Pesquisar Visitantes</h1>
            <thead>
              <tr>
                <th>Cod</th>
                <th>Acesso</th>
                <th>Foto</th>
                <th>Nome</th>
                <th>Telefone</th>
                <th>Identidade</th>
                <th>CPF</th>
                <th>CNPJ</th>
                <th class="text-center" class="center">Ações</th>
              </tr>
            </thead>

            <tbody>
              @forelse($data as $user)
                <tr>
                  <td>{{$user->id}}</td>
                  <td style="text-align: center; vertical-align: middle">
                    <a href="{{route('acessos.create',$user->id)}}"
                      class="btn btn-warning" title="Novo Acesso">
                      <i class="fa fa-external-link-square fa-fw fa-3x" aria-hidden="true"></i>
                    </a>
                  </td>
                  <td style="text-align: center; vertical-align: middle, max-width: 50px">
                    @if($user->image == null )
                      <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/painel/imgs/no-image.png')}}" style="max-width:96px;"></div>
                    @else
                      <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/uploads/entradas/'.$user->image)}}" style="max-width:96px;"></div>
                    @endif
                  </td>
                  <td>{{$user->nome}}</td>
                  <td>{{$user->telefone}}</td>
                  <td>{{$user->identidade}}</td>
                  <td>{{$user->cpf}}</td>
                  <td>{{$user->cnpj}}</td>

                  <td class="text-center" style="width:16%;">
                    {{Form::open(['route'=>['entradas.destroy',$user->id], 'method'=>'DELETE'])}}
                    <a class="btn btn-default btn-sm" title="Editar: {{$user->nome}}"   href='{{route('entradas.edit',$user->id)}}'><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                    <a class="btn btn-default btn-sm" title="Ver: {{$user->nome}}"   href='{{route('entradas.show',$user->id)}}'><i class="fa fa-eye" aria-hidden="true"></i></a>

                    {{ Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', ['class' => 'btn btn-default btn-sm', 'title'=>'Excluir', 'role' => 'button', 'type' => 'submit', 'onclick'=>"return confirm('Tem certeza que deseja excluir o visitante: $user->nome?')"]) }}
                    {{Form::close()}}
                  </td>
                </tr>
              @empty
                <div class="col-md-12 text-center">
                  <h1>
                    <p>Nenhum Visitante Cadastrado</p>
                  </h1>
                </div>
              @endforelse
            </tbody>
          </div>
        </div>
      </div>
    </div>
  </div>



@stop


@push('stack-js')
  @include('layouts.paginacaoPontos')
@endpush


</div>
