<div class="col-md-12" style="margin-top:10px;">
  <div class="panel panel-default">
    <div class="table-responsive">
      <table class="table table-striped table-hover table-responsive" id="myTable">
        <hr>
        <h1 class="text-center">Caminhões Cadastrados</h1>
        <thead>
          <tr>
            <th>Cod</th>
            <th class="text-center">Manutenção</th>
            <th>Placa</th>
            <th>Foto</th>
            <th>Cor</th>
            <th>Observação</th>
            <th>Modelo</th>
            <th>Cadastrado por:</th>
            <th>Data | Hora Cadastro</th>
            <th class="text-center" class="center">Ações</th>
          </tr>
        </thead>

        <tbody>
          @forelse($data as $user)
            <tr>
              <td>{{$user->id}}</td>
              <td style="text-align: center; vertical-align: middle">
                <a href="{{route('manutencao.veiculos.create',$user->id)}}"
                  class="btn btn-warning btn-lg" title="Cadastrar Manutenção">
                  <i class="fa fa-bolt fa-fw fa-3x" aria-hidden="true"></i>
                </a>
              </td>
              <td>{{$user->plate}}</td>
              <td style="text-align: center; vertical-align: middle, max-width: 50px">
                @if($user->image == null and $user->chassis == null )
                  <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/img/empi.png')}}" style="max-width:96px;"></div>
                @elseif ($user->image == null and $user->chassis == 'Caminhão')
                  <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/img/truck.png')}}" style="max-width:96px;"></div>
                @else
                  <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/uploads/veiculos/'.$user->image)}}" style="max-width:96px;"></div>
                @endif
              </td>
              <td>{{$user->color}}</td>
              <td>{!!$user->observacao!!}</td>
              <td>{{$user->model}}</td>

              <td>
                {!! $user->user->name!!}
              </td>
              <td>
                {!! \Carbon\Carbon::parse($user->created_at)->format('d/m/Y | H:i') !!}
              </td>

              <td class="text-center" style="width:16%;">
                {{Form::open(['route'=>['entradas.veiculo.destroy',$user->id], 'method'=>'DELETE'])}}
                <a class="btn btn-default btn-sm" title="Editar: {{$user->nome}}"   href='{{route('entradas.veiculo.edit',$user->id)}}'><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                <a class="btn btn-default btn-sm" title="Ver: {{$user->nome}}"   href='{{route('entradas.veiculo.show',$user->id)}}'><i class="fa fa-eye" aria-hidden="true"></i></a>

                {{ Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', ['class' => 'btn btn-default btn-sm', 'title'=>'Excluir', 'role' => 'button', 'type' => 'submit', 'onclick'=>"return confirm('Tem certeza que deseja excluir o veículo: $user->plate?')"]) }}
                {{Form::close()}}
              </td>
            </tr>
          @empty
            <div class="col-md-12 text-center">
              <h1>
                <p>Nenhum Caminhão Cadastrado</p>
              </h1>
            </div>
          @endforelse
        </tbody>
      </div>
    </div>
  </div>
