


<div class="title-pg">

    <div class="col-sm-12 form-group">

      <div class="col-sm-7">
        <h2><b>Filtrar por Período</b></h2>
        <label for=""></label>
      </div>
      {!!Form::open(['route' => ['chart.range']])!!}
      {{-- {!!Form::open(['method'=>'GET','route' => ['range.manutencao', $from, $to],'role'=>'search'])!!} --}}
      {{-- {!! Form::open(['method'=>'GET','url'=>'offices','class'=>'navbar-form navbar-left','role'=>'search'])  !!} --}}

      <div class="col-sm-2">
        <label for=""></label>
        {!! Form::text('start',null,['placeholder' => 'Data Início:', 'class' => 'form-control', 'id'=>'datetimepicker']) !!}
      </div>

      <div class="col-sm-2">
        <label for=""></label>
        {!! Form::text('end',null,['placeholder' => 'Data Fim:', 'class' => 'form-control', 'id'=>'datetimepicker2']) !!}
      </div>

      <div class="col-sm-1">
        <label for=""></label>
        {!! Form::submit('Filtrar', ['class' => 'btn btn-primary', 'style' => 'float:right;']) !!}
      </div>

    {!! Form::close() !!}

  </div><!--Content DinÃ¢mico-->







<div class="content-din">


  <input type="hidden"  value="{{$disabled = 0}}"/>
  {!! Form::open(['route' => 'usuarios.store', 'class' => 'form form-search form-ds', 'files' => true]) !!}

  <div class="panel panel-default col-md-12">
    <div class="mT5">
      @if( isset($errors) && count($errors) > 0 )
        <div class="alert alert-warning">
          @foreach($errors->all() as $error)
            <p>{{$error}}</p>
          @endforeach
        </div>
      @endif
    </div>



  </div><!--Content DinÃ¢mico-->
</div><!--Content DinÃ¢mico-->
</div><!--Content DinÃ¢mico-->

{{-- @include('painel.templates.telefone') --}}
<script type="text/javascript">

/* Máscaras ER */
function mascaratel(o,f){
  v_obj=o
  v_fun=f
  setTimeout("execmascara()",1)
}
function execmascara(){
  v_obj.value=v_fun(v_obj.value)
}
function mtel(v){
  v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
  v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
  v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
  return v;
}
function id( el ){
  return document.getElementById( el );
}
window.onload = function(){
  id('telefone').onkeyup = function(){
    mascaratel( this, mtel );
  }
}
</script>


@push('stack-js')




  <script type="text/javascript" src="{{asset('/assets/js/jquery.maskedinput.min.js')}}"></script>
  <script type="text/javascript">
  jQuery(function($){

    $("#datetimepicker").mask("99/99/9999");
    $("#datetimepicker2").mask("99/99/9999");

  });
  </script>
  <script type="text/javascript">


  function mascara(t, mask){
    var i = t.value.length;
    var saida = mask.substring(1,0);
    var texto = mask.substring(i)
    if (texto.substring(0,1) != saida){
      t.value += texto.substring(0,1);
    }
  }

  $( function() {
    $( "#datetimepicker" ).datepicker({
      format: 'dd/mm/yyyy',
      language: 'pt-BR'
    });
    $( "#datetimepicker2" ).datepicker({
      format: 'dd/mm/yyyy',
      language: 'pt-BR'
    });
  } );
</script>

@endpush
