@extends('layouts.layout.template')

@section('content')
  <h1>Gráficos <a href="{{ route('/painelVeiculos') }}" class="btn btn-default" style="float:right">Voltar</a>
</h1>

  <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Gráfico Manutenção</b></div>
                <div class="panel-body">
                  @include('painel.charts.formRange')
                    <canvas id="canvas" height="300" width="600"></canvas>
                    <div class="panel-heading"><b>Manutenção</b></div>
                    <div class=" table table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Quantidade</th>
                                    <th>Tipo de Falha</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($resultado as $key => $value)
                                    <tr>
                                        <td>{{$value->quantFalha}}</td>
                                        <td>{{$value->falha}}</td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('stack-js')
  {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/js/bootstrap-select.min.js" charset="utf-8"></script> --}}
  {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js" charset="utf-8"></script> --}}
  <script src="{{asset('assets/chartjs/Chart.bundle.js')}}" charset="utf-8"></script>
  {{-- <script type="text/javascript" src="{{asset('/assets/js/demo/chartjs-demo.js')}}"></script> --}}

  <script>
  var url = "{{url('dash/chart')}}";
  var Years = new Array();
  var Labels = new Array();
  var Prices = new Array();
  $(document).ready(function(){
      $.get(url, function(response){
          response.forEach(function(data){
              Years.push(data.quantFalha);
              Labels.push(data.falha);
              Prices.push(data.quantFalha);
          });
          var ctx = document.getElementById("canvas").getContext('2d');
          var myChart = new Chart(ctx, {
              type: 'polarArea',
              data: {
                  labels:Labels,
                  datasets: [{
                      label: 'Manutencão',
                      data: Years,
                      backgroundColor: [
                          'rgba(255, 99, 132, 0.2)',
                          'rgba(54, 162, 235, 0.2)',
                          'rgba(255, 206, 86, 0.2)',
                          'rgba(75, 192, 192, 0.2)',
                          'rgba(153, 102, 255, 0.2)',
                          'rgba(255, 159, 64, 0.2)',
                          'rgba(255, 99, 132, 0.2)',
                          'rgba(54, 162, 235, 0.2)',
                          'rgba(255, 206, 86, 0.2)',
                          'rgba(75, 192, 192, 0.2)',
                          'rgba(153, 102, 255, 0.2)',
                          'rgba(255, 99, 132, 0.2)',
                          'rgba(54, 162, 235, 0.2)',
                          'rgba(255, 206, 86, 0.2)',
                          'rgba(75, 192, 192, 0.2)',
                          'rgba(153, 102, 255, 0.2)',
                          'rgba(255, 159, 64, 0.2)',
                          'rgba(255, 99, 132, 0.2)',
                          'rgba(54, 162, 235, 0.2)',
                          'rgba(255, 206, 86, 0.2)',
                          'rgba(75, 192, 192, 0.2)',
                          'rgba(153, 102, 255, 0.2)',
                          'rgba(255, 159, 64, 0.2)'
                      ],
                      // borderWidth: 1
                  }]
              },
              options: {
                  scales: {
                      yAxes: [{
                          ticks: {
                              beginAtZero:true
                          }
                      }]
                  }
              }
          });
      });
  });

  </script>

@endpush
