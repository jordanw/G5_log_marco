@extends('layouts.layout.template')
@section('title', 'Usuários')

@section('content')

  <!-- Search for small screen -->

  @include('layouts.breadcrumb-entradas')


  <div class="section">
    @include('layouts.success')
    @include('layouts.session')

    @include('layouts.cadastrar-veiculos')

    <div class="col-md-12">
      @if( Session::has('success') )
        <div class="alert alert-success hide-msg" style="float: left; width: 100%; margin: 10px 0px;">
          {{Session::get('success')}}
        </div>
      @endif
    </div>
    @include('painel.entradasVeiculos.formIndex')

    @endsection
    @push('stack-js')
      @include('layouts.paginacaoPontos')
    @endpush
