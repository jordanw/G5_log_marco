<?php
use Carbon\Carbon;
?>
@if( isset($user) )
  {!! Form::model($user, ['route' => ['entradas.veiculo.update', $user->id],
                          'class' => 'form form-search form-ds', 'files' => true, 'method' => 'PUT']) !!}
  <input type="hidden"  value="{{$disabled = 1}}"/>

@else
  <input type="hidden"  value="{{$disabled = 0}}"/>
  {!! Form::open(['route' => 'entradas.veiculo.store', 'class' => 'form form-search form-ds', 'files' => true]) !!}
@endif
<div class="panel panel-default col-md-12">
  @include('layouts.session')
  <div class="col-md-12">

    <div class="row">
      <div class="col-md-12">
        @if ($disabled == 1)
          <h4>Editar Veículo: {{$user->nome}}</h4>
        @else
          <h4>Novo Veículo</h4>
        @endif
      </div>



      <div class="col-sm-12">

        <div class="title-pg text-center">
          <h2 class="title-pg"><b>Cadastro</b></h2>
        </div>

      </div>



      <div class="row">
        <div class="title-pg text-center">
          <h3 class="title-pg"><b>Dados do Veículo</b></h3>
        </div>



        <div class="col-md-2 form-group">
          <label for="nome">Placa</label>
          {!! Form::text('plate', null, ['class' => 'form-control', 'maxlength'=>'9']) !!}
        </div>
        <div class="col-md-2 form-group">
          <label for="cpf">Cidade <small></small></label>
          {!! Form::text('city', null, ['value' => 'São Luís','class' => 'form-control', 'maxlength'=>'20']) !!}
        </div>
        <div class="col-md-2 form-group">
          <label for="cpf">UF <small></small></label>
          {!! Form::select('state', [
            "AC"=>"AC",
            "AL"=>"AL",
            "AM"=>"AM",
            "AP"=>"AP",
            "BA"=>"BA",
            "CE"=>"CE",
            "DF"=>"DF",
            "ES"=>"ES",
            "GO"=>"GO",
            "MA"=>"MA",
            "MT"=>"MT",
            "MS"=>"MS",
            "MG"=>"MG",
            "PA"=>"PA",
            "PB"=>"PB",
            "PR"=>"PR",
            "PE"=>"PE",
            "PI"=>"PI",
            "RJ"=>"RJ",
            "RN"=>"RN",
            "RO"=>"RO",
            "RS"=>"RS",
            "RR"=>"RR",
            "SC"=>"SC",
            "SE"=>"SE",
            "SP"=>"SP",
            "TO"=>"TO"
          ], null, ['default' => "MA","class"=>"form-control"]) !!}
        </div>



        <div class="col-md-2 form-group">
          <label for="color">Cor</label>
          {!! Form::text('color', null, ['class' => 'form-control', 'maxlength'=>'20']) !!}
        </div>
        <div class="col-md-4 form-group">
          <label for="brand">Marca</label>
          {!! Form::text('brand', null, ['class' => 'form-control', 'maxlength'=>'20']) !!}
        </div>
        <div class="col-md-2 form-group">
          <label for="model">Modelo</label>
          {!! Form::text('model', null, ['class' => 'form-control', 'maxlength'=>'20']) !!}
        </div>
        <div class="col-md-2 form-group">
          <label for="model">Ano Modelo</label>
          {!! Form::text('model_year', null, ['class' => 'form-control', 'maxlength'=>'20']) !!}
        </div>
        <div class="col-md-2 form-group">
          <label for="model">Ano Veículo</label>
          {!! Form::text('year', null, ['class' => 'form-control', 'maxlength'=>'20']) !!}
        </div>


        <div class="col-md-3 form-group">
          <label for="status_message">Status</label>
          {!! Form::text('status_message', null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-3 form-group">
          <label for="responsavel">Cadastrado por: </label>
          {!! Form::text('responsavel', Auth::user()->name, ['class' => 'form-control', 'maxlength'=>'20']) !!}
        </div>
        <div class="col-md-6 form-group">
          <label for="observacao">Observação</label>
          {!! Form::textarea('observacao', null, ['class' => 'form-control', 'id' => 'editor1']) !!}
        </div>
        <div class="col-sm-12 col-md-6">

          <div class="title-pg text-center">
            <h4 class="title-pg"><b>Foto Veículo</b></h4>
          </div>
          {{-- <div class="col-sm-12 col-md-6 mB5">
            <div id="my_camera"></div>
            <input class="btn btn-danger mT5" type=button value="Webcam" onClick="configure()">
            <input class="btn btn-default mT5" type=button value="Click!" onClick="take_snapshot()">
            {{ Form::hidden('imagem', '', array('id' => 'imagem')) }}
          </div>
          <div class="col-sm-12 col-md-6">
            <div id="results" value="imagem"></div>
          </div>
          --}}

            {{-- {!! Form::file('image', ['class' => 'form-control', 'class' => 'btn btn-primary']) !!} --}}
            <div class="col-xs-12 col-sm-12">
              <div class="row">
                <div class="col-xs-12 col-sm-12">
                  <div class="col-xs-12 col-sm-12">
                    <div style="width: 50%; text-align:center;">
                      <div class="fileinput fileinput-new" data-provides="fileinput"
                      data-name="image" style="margin-top: 10px;">
                      <div class="fileinput-new" style="margin-bottom: 15px; margin-left: 25%;">
                        <img
                        src="{{asset('assets/img/logo-nova.png')}}"
                        class="m-t-xs img-responsive">
                      </div>
                      <div class="fileinput-preview fileinput-exists thumbnail"
                      style="margin-bottom: 15px; text-align:center;"></div>
                      <div class="text-center">
                        <h6>Tipos de Arquivo: jpeg, png, jpg - max:800px </h6>
                        <span class="btn btn-success btn-file"><span
                          class="fileinput-new">Selecionar foto</span><span
                          class="fileinput-exists">Mudar</span>
                          <input type="file" value=""/>
                        </span>
                        <a href="#" class="btn btn-danger fileinput-exists"
                        data-dismiss="fileinput">Remover</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>




        <script type="text/javascript">

        /* Máscaras ER */
        function mascaratel(o,f){
          v_obj=o
          v_fun=f
          setTimeout("execmascara()",1)
        }
        function execmascara(){
          v_obj.value=v_fun(v_obj.value)
        }
        function mtel(v){
          v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
          v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
          v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
          return v;
        }
        function id( el ){
          return document.getElementById( el );
        }
        window.onload = function(){
          id('telefone').onkeyup = function(){
            mascaratel( this, mtel );
          }
        }
        </script>

        <script type="text/javascript">
        CKEDITOR.replace( 'editor1', {
            language: 'pt-BR',
            uiColor: '#dedfe0'
        });
        CKEDITOR.replace( 'editor2', {
            language: 'pt-BR',
            uiColor: '#dedfe0'
        });
        </script>

      </div>
    </div>
  </div>


  <div class="form-group col-md-12">
    {!! Form::submit('Enviar', ['class' => 'btn btn-primary', 'style' => 'float:right;']) !!}
  </div>
  {!! Form::close() !!}
