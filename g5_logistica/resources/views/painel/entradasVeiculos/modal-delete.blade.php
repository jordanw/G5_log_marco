<!-- Modal -->

<div class="modal fade" id="myModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Finalizar Manutenção OS: {{$user->os_num}}</h4>
      </div>
      <div class="modal-body">

        {{Form::open(['route'=>['manutencao.veiculos.destroy',$user->id], 'id'=>'Register','method'=>'DELETE'])}}

        {!! Form::label("comentarios","Comentários:") !!}
        {!! Form::textarea("comentarios",null,["class"=>"form-control ckclass", 'id'=>"editor1"]) !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
        {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}

        {{ Form::button('Concluir', ['class' => 'btn btn-success', 'title'=>'Concluir', 'role' => 'button', 'type' => 'submit', 'id'=>'submitForm', 'onclick'=>"return confirm('Tem certeza que deseja concluir a Manutenção de: {$user->entrada->plate}?')"]) }}

        {{Form::close()}}
      </div>
    </div>
  </div>
</div>
