@extends('layouts.layout.template')
@push('ck-editor')
  <script src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
@endpush
@section('content')


  <div class="row">
    <div class="col-md-6 text-left">
      <h4 class="breadcrumb-title">{{$user->nome or 'Novo Veículo'}}</h4>
    </div>
    <div class="col-md-6">
      <ol class="breadcrumb hidden-xs text-right mt1">
        <li><a href="{{url('/painel')}}">Home</a></li>
        <li><a href="{{route('entradas.veiculo.index')}}">Veículos</a></li>
        <li class="active"><a href="#">{{$user->nome or 'Novo Veículo'}}</a></li>
      </ol>
    </div>
  </div>

  {{-- <div class="container">
  @if( isset($user) )
  @include('layouts.userImageCard')
@endif
</div> --}}
<div class="col-md-12">

  @include('painel.entradasVeiculos.form')

</div>

@endsection

@push('stack-css')
  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="{{asset('assets/js/plugins/prism/prism.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{asset('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{asset('assets/js/plugins/chartist-js/chartist.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{asset('assets/js/plugins/dropify/css/dropify.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
@endpush

@push('stack-js')


  <script type="text/javascript" src="{{asset('/assets/js/jquery.maskedinput.min.js')}}"></script>
  <script type="text/javascript">
  jQuery(function($){

    $("#cpf").mask("999.999.999-99");

    $("#cnpj").mask("99.999.999/9999-99");
    $("#datetimepicker").mask("99/99/9999");

  });
  </script>
  <script type="text/javascript">


  function mascara(t, mask){
    var i = t.value.length;
    var saida = mask.substring(1,0);
    var texto = mask.substring(i)
    if (texto.substring(0,1) != saida){
      t.value += texto.substring(0,1);
    }
  }

  $( function() {
    $( "#datetimepicker" ).datepicker({
        format: 'dd/mm/yyyy',
        language: 'pt-BR'
      });
  } );




  </script>


    <style>
    #my_camera{
      width: 213px;
      height: 160px;
      border: 1px solid #4577b3;
    }
  </style>


  <script type="text/javascript" src="{{asset('assets/webcamjs/webcam.min.js')}}"></script>


  <!-- Code to handle taking the snapshot and displaying it locally -->
  <script language="JavaScript">

  // Configure a few settings and attach camera
  function configure(){
    Webcam.set({
      width: 213,
      height: 160,
      image_format: 'jpeg',
      jpeg_quality: 90
    });
    Webcam.attach( '#my_camera' );
  }
  // A button for taking snaps


  // preload shutter audio clip
  var shutter = new Audio();
  shutter.autoplay = false;
  shutter.src = navigator.userAgent.match(/Firefox/) ? 'shutter.ogg' : 'shutter.mp3';

  function take_snapshot() {
    // play sound effect
    shutter.play();

    // take snapshot and get image data
    Webcam.snap( function(data_uri) {
      // display results in page
      document.getElementById('results').innerHTML =
      '<img id="imageprev" src="'+data_uri+'"/>';

      var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');

      document.getElementById('imagem').value = raw_image_data;
      document.getElementById('myform').submit();


    } );

  }

  function saveSnap(){
    // Get base64 value from <img id='imageprev'> source
    var base64image = document.getElementById("imageprev").src;

    Webcam.upload( base64image, '{{route('usuarios.store')}}', function(code, text) {
      console.log('Save successfully');
      console.log(text);
    });

  }
  </script>




@endpush
