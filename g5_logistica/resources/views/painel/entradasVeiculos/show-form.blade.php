<?php
use Carbon\Carbon;
?>

<div class="panel panel-default col-md-12" style="margin-top:10px;">
  @include('layouts.session')

  <div class="col-md-12">

    <div class="col-lg-4 col-md-offset-4">
      <div class="widget-head-color-box blue-bg p-lg text-center">
        <div class="m-b-md">
          <h1 class="font-bold no-margins">{!!$user->plate!!}</h1>
          {{-- <div style="margin-top: 0px;"><strong>Placa:</strong> {{$user->plate}}</div> --}}
          {{-- @include('painel.templates.userLevel') --}}
        </div>
        @if($user->image == null and $user->chassis == null )
          <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/img/empi.png')}}" style="max-width:96px;"></div>
        @elseif ($user->image == null and $user->chassis == 'Caminhão')
          <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/img/truck.png')}}" style="max-width:96px;"></div>
        @else
          <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/uploads/veiculos/'.$user->image)}}" style="max-width:96px;"></div>
        @endif
      </div>


      <div class="widget-text-box">

        <h4><strong>Cor:</strong> {!!$user->color!!}</h4>
        @if ($user->chassis)
          <h4><strong>Tipo:</strong> {!!$user->chassis!!}</h4>
        @endif
        <h4><strong>Modelo:</strong> {!!$user->model!!}</h4>
        <h4><strong>Marca:</strong> {!!$user->brand!!}</h4>
        <h4><strong>Ano Modelo:</strong> {!!$user->model_year!!}</h4>
        <h4><strong>Ano: </strong>{!!$user->year!!}</h4>
        <h4><strong>Status: </strong>{!!$user->status_message!!}</h4>
        <h4><strong>Observação: </strong>{!!$user->observacao!!}</h4>



        @if(Auth::user()->nivel == 1)
          <div class="rgt">
            {!! Form::open(['route' => ['entradas.veiculo.destroy', $user->id], 'method' => 'DELETE']) !!}
            <div class="form-group">

              <a class="btn btn-warning" title="Editar: {!!$user->nome!!}"   href='{{route('entradas.veiculo.edit',$user->id)}}'><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
              {!! Form::button('<i class="fa fa-trash-o fa-2x"></i> ', ['class' => 'btn btn-danger', 'role' => 'button', 'type' => 'submit', 'onclick'=>"return confirm('Tem certeza que deseja excluir o veículo: $user->plate ?')"]) !!}

          </div>
            {!! Form::close() !!}
          </div>
        @endif()
      </div>
    </div>
</div>
