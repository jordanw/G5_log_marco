<div class="row">
  <div class="col-md-12" style="margin-top:10px;">
    <div class="panel panel-default">
      <div class="table-responsive">
        <h1 class="text-center">Veículos em Manutenção</h1>
        <table class="table table-striped table-hover table-responsive" id="myTable3">
          <thead>
            <tr>
              <th>Cod</th>
              <th class="text-center">Concluir</th>
              <th>Placa</th>
              <th>Foto</th>
              <th>Os_Num</th>
              <th>Falha:</th>
              <th>Local:</th>
              <th>Autorizado:</th>
              <th>Data | Hora</th>
              <th>Tempo Estimado</th>
              <th>Tempo Restante</th>
            </tr>
          </thead>

          <tbody>
            @forelse($dataAcesso as $user)
              @include('painel.entradasVeiculos.modal-delete')

              <tr>
                <td>{{$user->id}}</td>
                <td style="text-align: center; vertical-align: middle">
                  <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#myModal">
                    <i class="fa fa-check fa-fw fa-3x" aria-hidden="true"></i>
                  </button>

                </td>
                <td>{{$user->entrada->plate}}</td>
                <td style="text-align: center; vertical-align: middle, max-width: 50px">
                  @if($user->image == null and $user->chassis == null )
                    <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/img/empi.png')}}" style="max-width:96px;"></div>
                  @elseif ($user->image == null and $user->chassis == 'Caminhão')
                    <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/img/truck.png')}}" style="max-width:96px;"></div>
                  @else
                    <div class="m-b-sm"><img alt="image" class="img-rounded" src="{{asset('assets/uploads/veiculos/'.$user->image)}}" style="max-width:96px;"></div>
                  @endif
                </td>
                <td>{{$user->os_num}}</td>
                <td>{!!$user->falha!!}</td>
                <td>{!!$user->local!!}</td>
                <td>{{$user->concedido_acesso_por}}</td>
                @php
                $date1=  new DateTime($user->date_concedido_acesso);
                $date2= new DateTime($user->tempo_estimado);
                $date3= new DateTime('now');

                $data1  = $date1->format('Y-m-d H:i:s');
                $data2  = $date2->format('Y-m-d H:i:s');
                $data3  = $date3->format('Y-m-d H:i:s');

                $diff = $date1->diff($date2);
                $horas = $diff->h + ($diff->days * 24);

                $diffNow = $date3->diff($date2);
                $horasNow = $diffNow->h + ($diffNow->days * 24);
                // echo $date1->diff($date2)->format("%d")

                @endphp


                <td>{{date('H:i:s | d/m/Y', strtotime($user->date_concedido_acesso))}}</td>

                @if ($user->tempo_estimado == null)
                  <td>Não Informado</td>
                @else
                  @if ($horas > 0 and $horas <= 1)
                    <td>{{"{$horas} hora"}}</td>
                  @elseif ($horas > 1)
                    <td>{{"{$horas} horas"}}</td>
                  @else
                    <td>{{"Prazo Esgotado"}}</td>
                  @endif
                @endif

                @if ($user->tempo_estimado == null)
                  <td>Não Informado</td>
                @else
                  @if ($horasNow > 0 and $horasNow <= 1)
                    <td>{{"{$horasNow} hora"}}</td>
                  @elseif ($horasNow > 1)
                    <td>{{"{$horasNow} horas"}}</td>
                  @else
                    <td style="background-color:#efafaf">{{"Prazo Esgotado"}}</td>
                  @endif
                @endif

              </tr>
            @empty
              <div class="col-md-12 text-center">
                <h1>
                  <p>Nenhum Veículo em Manutenção</p>
                </h1>
              </div>
            @endforelse
          </tbody>
        </div>
      </div>
    </div>
  </div>
