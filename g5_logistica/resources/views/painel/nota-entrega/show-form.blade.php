<?php
use Carbon\Carbon;
?>

<div class="panel panel-default col-md-12" style="margin-top:10px;">
  @include('layouts.session')

  <div class="col-md-12">

    <div class="row">
      <div class="col-md-12">
        <h4>Nota de Entrega</h4>
      </div>
      <div class="form-group">

        <div class="col-sm-2">
          {!! Form::label("quantidade","Quantidade:") !!}
          {!! Form::text("qtdade",$user->qtdade,["class"=>"form-control", 'id'=>'total', 'maxlength'=>'10', 'style' =>'background-color: #fff;', 'readonly']) !!}
        </div>

        <div class="col-sm-5">
          {!! Form::label("titulo","Título:") !!}
          {!! Form::text("titulo",$user->titulo,["class"=>"form-control", 'style' =>'background-color: #fff;', 'readonly']) !!}
        </div>

        <div class="col-sm-5">
          {!! Form::label("autor","Autor:") !!}
          {!! Form::text("autor",$user->autor,["class"=>"form-control", 'style' =>'background-color: #fff;', 'readonly']) !!}
        </div>


        <div class="col-sm-2">
          {!! Form::label("isbn","ISBN:") !!}
          {!! Form::text("isbn",$user->isbn,["class"=>"form-control", 'maxlength'=>'20', 'style' =>'background-color: #fff;', 'readonly']) !!}
        </div>

        <div class="col-sm-2">
          {!! Form::label("user_cadastrou","Responsável:") !!}
          {!! Form::text("user_cadastrou",$user->user_cadastrou,["class"=>"form-control", 'style' =>'background-color: #fff;', 'readonly']) !!}
        </div>

        <div class="col-sm-2">
          {!! Form::label("percent","Percentual %") !!}
          {!! Form::text('percent', number_format($user->valor_percent, 0, ',', '.'), ["class"=>"form-control", 'style' =>'background-color: #fff; text-align: right;', 'readonly']) !!}
        </div>

        <div class="col-sm-2">
          {!! Form::label("valor_unit","PVP: (em R$)") !!}
          {!! Form::text("valor_pvp", number_format($user->valor_pvp, 2, ',', '.'),["class"=>"form-control", 'maxlength'=>'20', 'style' =>'background-color: #fff; text-align: right;', 'readonly']) !!}
        </div>


        <div class="col-sm-2">
          {!! Form::label("valor_total","Total AMEI (em R$)") !!}
          {!! Form::text('valor_total', number_format(($user->valor_total*$user->valor_percent)/100, 2, ',', '.'), ["class"=>"form-control", 'style' =>'background-color: #fff; text-align: right;', 'readonly']) !!}
        </div>

        <div class="col-sm-2" style="margin-bottom: 3%;">
          {!! Form::label("valor_total","Total Nota: (em R$)") !!}
          {!! Form::text('valor_total', number_format($user->valor_total, 2, ',', '.'), ["class"=>"form-control", 'style' =>'background-color: #fff; text-align: right;', 'readonly']) !!}
        </div>

      </div>
    </div>
  </div>

</div>
</div>
