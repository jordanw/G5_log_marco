@extends('layouts.layout.template')

@php
  use Carbon\Carbon;
@endphp

@section('content')


  <div class="row">
    <div class="col-md-6 text-left">
      <h4 class="breadcrumb-title">{{$user->nome or 'Nova Nota de Entrega'}} </h4>
    </div>
    <div class="col-md-6">
      <ol class="breadcrumb hidden-xs text-right mt1">
        <li><a href="{{url('/painel')}}">Home</a></li>
        <li><a href="{{url('/painel/escrtitores')}}">Entradas</a></li>
        <li class="active"><a href="#">{{$user->nome or 'Nova Nota de Entrega'}}</a></li>
      </ol>
    </div>
  </div>

  <div class="section">
    @include('layouts.success')
    @include('layouts.session')
  </div>


  {{-- <div class="container">
  @if( isset($user) )
  @include('layouts.userImageCard')
@endif
</div> --}}
<div class="col-md-4" style="margin-top: 3%;">
  <div class="ibox float-e-margins">

    <div class="ibox-content">
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Cod</th>
              <th>Nome </th>
              <th>Telefone </th>


            </tr>
          </thead>
          <tbody>
            @isset($data)
              <tr>
                <td>{{$user->entrada->id}}</td>
                <td>{{$user->entrada->nome}}</td>
                <td>{{$user->entrada->telefone}}</td>
              </tr>
            @else
              <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->nome}}</td>
                <td>{{$user->telefone}}</td>
              </tr>
            @endisset
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="col-md-8">
  <div class="row">
    <div class="col-lg-3 col-sm-3">
      <div class="circle-tile">
        <a href="#">
          <div class="circle-tile-heading dark-blue">
            <i class="fa fa-usd fa-fw fa-3x"></i>
          </div>
        </a>
        <div class="circle-tile-content dark-blue">
          <div class="circle-tile-description text-faded">
            Negócios
          </div>
          <div class="circle-tile-number text-faded">
            {{ number_format($user->nota_entregas->sum('valor_total'),2,',','.') }}
            <span id="sparklineA"></span>
          </div>
          <a href="#" class="circle-tile-footer">Em R$ <i class="fa fa-chevron-circle-right"></i></a>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-sm-3">
      <div class="circle-tile">
        <a href="#">
          <div class="circle-tile-heading blue">
            <i class="fa fa-bell fa-fw fa-3x"></i>
          </div>
        </a>
        <div class="circle-tile-content blue">
          <div class="circle-tile-description text-faded">
            Notas Abertas
          </div>
          <div class="circle-tile-number text-faded">
            {{$user->nota_entregas->where('ativo', '1')->count()}}
          </div>
          <a href="#" class="circle-tile-footer"> <i class="fa fa-chevron-circle-right"></i></a>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-sm-3">
      <div class="circle-tile">
        <a href="#">
          <div class="circle-tile-heading orange">
            <i class="fa fa-money fa-fw fa-3x"></i>
          </div>
        </a>
        <div class="circle-tile-content orange">
          <div class="circle-tile-description text-faded">
            Negócios Ativos
          </div>
          <div class="circle-tile-number text-faded">
            {{ number_format($user->nota_entregas->where('ativo', '1')->sum('valor_total'),2,',','.') }}
          </div>
          <a href="#" class="circle-tile-footer">Em R$ <i class="fa fa-chevron-circle-right"></i></a>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-sm-3">
      <div class="circle-tile">
        <a href="#">
          <div class="circle-tile-heading green">
            <i class="fa fa-check-square-o fa-fw fa-3x"></i>
          </div>
        </a>
        <div class="circle-tile-content green">
          <div class="circle-tile-description text-faded">
            Prest. Contas
          </div>
          <div class="circle-tile-number text-faded">
            {{-- {{date("d/m/Y | H:i" , strtotime($user->date_prestado))}} --}}
          {{  Carbon::parse($prestacao)->diffInDays()}}
          </div>
          <a href="#" class="circle-tile-footer">Dias <i class="fa fa-chevron-circle-right"></i></a>
        </div>
      </div>
    </div>


  </div>

</div>


<div class="col-md-12">
  <div class="ibox float-e-margins">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="panel-head">
          <h3>Registrar Entrada</h3>
        </div>
        <div class="col-xs-12 col-sm-12">
          @include('painel.nota-entrega.form._formBase_nota')

        </div>
      </div>

    </div>

    <div class="ibox-content">
      <div class="panel-head">
        <h3>Entradas Registradas</h3>
      </div>
      <div class="table-responsive">
        <table class="table table-striped" id="myTable">
          <thead>
            <tr>
              <th>NF</th>
              <th>Data</th>
              <th>Qtdade</th>
              <th>Titulo</th>
              <th>Autor</th>
              <th class="ctr">Valor Unit </th>
              <th class="ctr">Valor Percent </th>
              <th class="ctr">PVP </th>
              <th class="ctr">Total </th>
              <th style="text-align: center">Ações</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($user->nota_entregas->where('ativo', '1') as $user)
              <tr>
                <td>{{$user->nf}}</td>
                <td>{{date("d/m/Y", strtotime($user->date))}}</td>
                <td>{{$user->qtdade}}</td>
                <td>{{$user->titulo}}</td>
                <td>{{$user->autor}}</td>
                <td class="rgt">R${{ number_format($user->valor_unit,2,',','.') }}</td>
                <td class="rgt">R${{ number_format($user->valor_percent,2,',','.') }}</td>
                <td class="rgt">R${{ number_format($user->valor_pvp,2,',','.') }}</td>
                <td class="rgt">R${{ number_format($user->valor_total,2,',','.') }}</td>
                <td class="text-center" style="width:16%;">
                  {{Form::open(['route'=>['nota-entrega.destroy',$user->id], 'method'=>'DELETE'])}}
                  <a class="btn btn-default btn-sm" title="Editar: {{$user->titulo}}"   href='{{route('editar.nota',$user->id)}}'><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                  <a class="btn btn-default btn-sm" title="Ver: {{$user->titulo}}"   href='{{route('ver.nota',$user->id )}}'><i class="fa fa-eye" aria-hidden="true"></i></a>

                  {{ Form::button('<i class="fa fa-check-square-o" aria-hidden="true"></i>', ['class' => 'btn btn-default btn-sm', 'title'=>'Concluir', 'role' => 'button', 'type' => 'submit', 'onclick'=>"return confirm('Tem certeza que deseja concluir a Entrada: $user->titulo?')"]) }}
                  {{Form::close()}}
                </td>


              </tr>
            @endforeach
          </tbody>
        </table>

      </div>
    </div>
    <div class="ibox-content">
      <div class="panel-head">
        <h3>Prestação de Contas</h3>
      </div>
      <div class="table-responsive">
        <table class="table" id="myTable2">
          <thead>
            <tr>
              <th>NF</th>
              <th>Data | Hora</th>
              <th>Prestado por</th>
              <th>Qtdade</th>
              <th>Titulo</th>
              <th>Autor</th>
              <th class="ctr">Valor Unit </th>
              <th class="ctr">Valor Percent </th>
              <th class="ctr">PVP </th>
              <th class="ctr">Total </th>
              <th style="text-align: center">Restaurar</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($user2->nota_entregas->where('ativo', '0') as $user)
              <tr class="strikeout" style="color: darkgray;">
                <td>{{$user->nf}}</td>
                <td>{{date("d/m/Y | H:i" , strtotime($user->date_prestado))}}</td>
                <td>{{$user->prestado_por}}</td>
                <td>{{$user->qtdade}}</td>
                <td>{{$user->titulo}}</td>
                <td>{{$user->autor}}</td>
                <td class="rgt sim">R${{ number_format($user->valor_unit,2,',','.') }}</td>
                <td class="rgt sim">R${{ number_format($user->valor_percent,2,',','.') }}</td>
                <td class="rgt sim">R${{ number_format($user->valor_pvp,2,',','.') }}</td>
                <td class="rgt sim">R${{ number_format($user->valor_total,2,',','.') }}</td>
                <td class="text-center">
                  {{Form::open(['route'=>['nota-entrega.restore',$user->id], 'method'=>'get'])}}

                  {{ Form::button('<i class="fa fa-check-square-o" aria-hidden="true"></i>', ['class' => 'btn btn-default btn-sm', 'title'=>'Restaurar', 'role' => 'button', 'type' => 'submit', 'onclick'=>"return confirm('Tem certeza que deseja restaurar a Entrada: $user->titulo?')"]) }}
                  {{Form::close()}}
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>

      </div>
    </div>
    {{-- <div class="col-sm-12 col-xs-12">
    <div class="col-sm-6 col-xs-6"><h5>Total Compras</h5></div>
    <div class="col-sm-6 col-xs-6 right"><h4>R$ {{ number_format($totalAdiantCliente,2,',','.') }}</h4></div>
  </div> --}}
</div>
</div>

@endsection

@push('stack-css')
  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="{{asset('assets/js/plugins/prism/prism.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{asset('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{asset('assets/js/plugins/chartist-js/chartist.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{asset('assets/js/plugins/dropify/css/dropify.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
@endpush

@push('stack-js')

  @include('layouts.paginacaoPontos')

  <script type="text/javascript" src="{{asset('/assets/js/jquery.maskedinput.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('/assets/js/jquery.maskMoney.js')}}"></script>
  <script type="text/javascript">
  jQuery(function($){

    $("#cpf").mask("999.999.999-99");

    $("#cnpj").mask("99.999.999/9999-99");
    $("#datetimepicker").mask("99/99/9999");
    $("#valor").maskMoney();


  });




  </script>



@endpush
