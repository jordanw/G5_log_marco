<?php
use Carbon\Carbon;
?>
@if( isset($data) )
  {!! Form::model($user, ['route' => ['atualizar.nota', $user->id],
  'class' => 'form form-search form-ds', 'files' => true, 'method' => 'PUT']) !!}
  <input type="hidden"  value="{{$disabled = 1}}"/>

@else
  <input type="hidden"  value="{{$disabled = 0}}"/>
  {!! Form::open(['route' => ['gravar.nota', $user->id],
  'class' => 'form form-search form-ds', 'files' => true]) !!}
@endif

<div class="col-xs-12 col-sm-12">

  <div class="form-group">

    <div class="col-sm-2">
      {!! Form::label("quantidade","Quantidade:") !!}
      {!! Form::text("qtdade",null,["class"=>"form-control",
      'id'=>'total', 'maxlength'=>'10']) !!}
    </div>

    <div class="col-sm-5">
      {!! Form::label("titulo","Título:") !!}
      {!! Form::text("titulo",null,["class"=>"form-control"]) !!}
    </div>

    <div class="col-sm-5">
      {!! Form::label("autor","Autor:") !!}
      {!! Form::text("autor",$user->nome,["class"=>"form-control"]) !!}
    </div>


    <div class="col-sm-2">
      {!! Form::label("isbn","ISBN:") !!}
      {!! Form::text("isbn",null,["class"=>"form-control", 'maxlength'=>'50']) !!}
    </div>

    <div class="col-sm-2">
      {!! Form::label("nf","NF:") !!}
      {!! Form::text("nf",null,["class"=>"form-control", 'maxlength'=>'50']) !!}
    </div>

    <div class="col-sm-3">
      {!! Form::label("user_cadastrou","Responsável Cadastro:") !!}
      {!! Form::text("user_cadastrou",Auth::user()->name,["class"=>"form-control"]) !!}
    </div>

    <div class="col-sm-2">
      {!! Form::label("percent","Percentual:") !!}
      {!! Form::select('percent', [
        "1"=>"Sem %",
        "25"=>"25 %",
        "30"=>"30 %",
        "35"=>"35 %",
        "40"=>"40 %",
      ], null, ['placeholder' => 'Selecione...',"class"=>"form-control"]) !!}
    </div>

    <div class="col-sm-3">
      {!! Form::label("valor_pvp","PVP: (em R$)") !!}
      @if( isset($data) )
      {!! Form::text("valor_pvp",number_format($user->valor_pvp, 2, ',', '.'),
      ["class"=>"form-control", 'id'=>'valor', 'maxlength'=>'15']) !!}
      @else
      {!! Form::text("valor_pvp",null,["class"=>"form-control", 'id'=>'valor', 'maxlength'=>'15']) !!}
      @endif
    </div>

  </div>

  <div class="col-sm-12 form-group">
    {{Form::submit('Salvar',['class'=>'btn btn-primary',
    'style'=>'font-weight:bold; margin-top:3%;'])}}
  </div>

</div>

{!! Form::close() !!}



{{-- @include('layouts.mascaras') --}}
