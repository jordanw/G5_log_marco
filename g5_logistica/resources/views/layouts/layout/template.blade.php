<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>{{ config('app.name') }}</title>
	@include('layouts.layout.inc-stylesheet')
	@yield('css')
	@stack('ck-editor')
<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
	<div id="wrapper">
		@include('layouts.layout.inc-left-sidebarGestor')
		<div id="page-wrapper" class="gray-bg dashbard-1">
			@include('layouts.layout.header')
			@yield('content')
		</div>
	</div>
	@include('layouts.layout.inc-scripts')
	@yield('js')
	@stack('stack-js')
	@stack('vuejs')
	@stack('stack-telefone')
</body>
</html>
