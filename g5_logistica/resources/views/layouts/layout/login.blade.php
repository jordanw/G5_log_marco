
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{asset('assets/img/fav-doc.png')}}" />
    <title>{{ config('app.name') }}</title>

    @include('layouts.layout.stylesheet')



</head>

<body class="gray-bg">

  @yield('content')


</body>

</html>
