<nav class="navbar-default navbar-static-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav" id="side-menu">
			<li class="nav-header">
				<div class="dropdown profile-element">
					@if(Auth::user()->image == null )
						<span><img alt="image" class="img-rounded maxW96" src="{{asset('/assets/img/profile_small.jpg')}}"></span>
					@else
						<span><img alt="image" class="img-rounded maxW96" src="{{asset('assets/uploads/users/'.Auth::user()->image)}}"></span>
					@endif
					<a data-toggle="dropdown" class="dropdown-toggle" href="{{ URL::to('#') }}">
						<span class="clear">
							<span class="block m-t-xs"><strong class="font-bold">{{ isset(Auth::user()->name) ? Auth::user()->name : 'Zé'}}</strong></span>
							@if (Auth::user()->nivel == 1)
								<span class="text-muted text-xs block twhite">Owner <b class="caret"></b></span>
							</span>
						@elseif (Auth::user()->nivel == 2)
							<span class="text-muted text-xs block twhite">Administrador<b class="caret"></b></span>
						</span>
					@else
						<span class="text-muted text-xs block twhite">Usuário<b class="caret"></b></span>
					</span>
				@endif
			</a>
			<ul class="dropdown-menu animated fadeInRight m-t-xs dropLeft">
				{{-- <li><a href="{{ route('/') }}">Profile</a></li> --}}

				<li >
					<a href="{{ route('logout') }}"
					onclick="event.preventDefault();
					document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>
					Sair
				</a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					{{ csrf_field() }}
				</form>
			</li>

		</ul>
	</div>
	<div class="logo-element">G5+</div>
</li>


@if (Auth::user()->nivel == 1 || Auth::user()->nivel == 2)
	{{-- <li>
		<a href="{{ route('/') }}"><i class="fa fa-sliders"></i> <span class="nav-label">Dashboard</span></a>
	</li> --}}
	<li>
		<a href="{{ route('/painelVeiculos') }}"><i class="fa fa-sliders"></i> <span class="nav-label">Dashboard</span></a>
	</li>
	<li>
		<a href="#"><i class="fa fa-user"></i> <span class="nav-label">Usuários</span> <span class="fa arrow"></span></a>
		<ul class="nav nav-second-level">
			<li><a href="{{route('usuarios.index')}}">Ver</a></li>
			<li><a href="{{route('usuarios.create')}}">Criar</a></li>
		</ul>
	</li>
	{{-- <li>
		<a href="#"><i class="fa fa-users"></i> <span class="nav-label">Visitantes</span> <span class="fa arrow"></span></a>
		<ul class="nav nav-second-level">
			<li><a href="{{route('entradas.index')}}">Ver</a></li>
			<li><a href="{{route('entradas.create')}}">Criar</a></li>
		</ul>
	</li>

	<li>
		<a href="#"><i class="fa fa-universal-access"></i> <span class="nav-label">Acessos Visitantes</span> <span class="fa arrow"></span></a>
		<ul class="nav nav-second-level">
			<li><a href="{{route('acessos.index')}}">Ver</a></li>
		</ul>
	</li> --}}

	<li>
		<a href="#"><i class="fa fa-car"></i> <span class="nav-label">Empilhadeiras</span> <span class="fa arrow"></span></a>
		<ul class="nav nav-second-level">
			<li><a href="{{route('entradas.veiculo.index')}}">Ver</a></li>
			<li><a href="{{route('entradas.veiculo.create')}}">Criar</a></li>
		</ul>
	</li>

	<li>
		<a href="#"><i class="fa fa-truck"></i> <span class="nav-label">Caminhões</span> <span class="fa arrow"></span></a>
		<ul class="nav nav-second-level">
			<li><a href="{{route('entradas.caminhao.index')}}">Ver</a></li>
			<li><a href="{{route('entradas.caminhao.create')}}">Criar</a></li>
		</ul>
	</li>
	<li>
		<a href="#"><i class="fa fa-building"></i> <span class="nav-label">Almoxarifado</span> <span class="fa arrow"></span></a>
		<ul class="nav nav-second-level">
			<li><a href="#">Ver</a></li>
			<li><a href="#">Criar</a></li>
		</ul>
	</li>


		<li>
			<a href="#"><i class="fa fa-check"></i> <span class="nav-label">Veículos / Manutenção</span> <span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				<li><a href="{{route('manutencao.veiculos.index')}}">Ver</a></li>
			</ul>
		</li>
	@else
		{{-- <li>
			<a href="{{ route('/') }}"><i class="fa fa-users"></i> <span class="nav-label">Visitantes</span></a>
		</li> --}}
		<li>
			<a href="{{ route('/painelVeiculos') }}"><i class="fa fa-car"></i> <span class="nav-label">Veículos</span></a>
		</li>

@endif

</nav>
