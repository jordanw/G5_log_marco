<div class="col-md-12">
  @if( Session::has('status') )
    <div class="alert alert-success hide-msg" style="float: left; width: 100%; margin: 10px 0px;">
      {{Session::get('status')}}
    </div>
  @endif
  @if( Session::has('delete') )
    <div class="alert alert-warning hide-msg" style="float: left; width: 100%; margin: 10px 0px;">
      {{Session::get('delete')}}
    </div>
  @endif
</div>
