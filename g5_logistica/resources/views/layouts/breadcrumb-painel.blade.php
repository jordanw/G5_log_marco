<div class="row">
      <div class="col-md-6 text-left">
          <h4 class="breadcrumb-title">{{$title}}</h4>
      </div>
      <div class="col-md-6">
          <ol class="breadcrumb hidden-xs text-right mt1">
              <li><a href="{{url('/painel')}}">Home</a></li>
          </ol>
      </div>
  </div>
