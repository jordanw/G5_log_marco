<?php
use Carbon\Carbon;
?>

<div class="col-md-12">
  <h4>Endereço</h4>
</div>

<div id="vue">

  <div class="col-md-3 form-group">
    <label for="cep">Cep</label>
    {!! Form::text('cep', null, ['class' => 'form-control','onkeypress'=>"mascara(this, '#####-###')", 'id'=>'cep','maxlength'=>'9']) !!}
  </div>
  <div class="col-md-2 form-group">
    <label for="numero">Número</label>
    {!! Form::text('numero', null, ['class' => 'form-control', 'id'=>'numero',  'maxlength'=>"15"]) !!}
  </div>


  <div class="col-md-7 form-group">
    <label for="endereco">Logradouro</label>
    {!! Form::text('endereco', null, ['class' => 'form-control', 'id'=>'rua']) !!}
  </div>

  <div class="col-md-5 form-group">
    @if( isset($user) )
      <label for="complemento">Complemento</label>
    @else
      <label for="complemento">Complemento <small>(opcional)</small></label>
    @endif
    {!! Form::text('complemento', null, ['class' => 'form-control', 'id'=>'complemento']) !!}
  </div>

  <div class="col-md-3">
    <label for="bairro">Bairro</label>
    {!! Form::text('bairro', null, ['class' => 'form-control', 'id'=>'bairro']) !!}
  </div>
  <div class="col-md-2 form-group">
    <label for="cidade">Cidade</label>
    {!! Form::text('cidade', null, ['class' => 'form-control', 'id'=>'cidade']) !!}
  </div>
  <div class="col-md-2 form-group">
    <label for="estado">Estado</label>
    {!! Form::text('estado', null, ['class' => 'form-control', 'id'=>'uf']) !!}
  </div>

</div>
@push('vuejs')


  <!-- Adicionando Javascript -->
  <script type="text/javascript" >


  $(document).ready(function() {

    function limpa_formulário_cep() {
      // Limpa valores do formulário de cep.
      $("#rua").val("");
      $("#bairro").val("");
      $("#cidade").val("");
      $("#uf").val("");

    }

    //Quando o campo cep perde o foco.
    $("#cep").blur(function() {

      //Nova variável "cep" somente com dígitos.
      var cep = $(this).val().replace(/\D/g, '');

      //Verifica se campo cep possui valor informado.
      if (cep != "") {

        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;

        //Valida o formato do CEP.
        if(validacep.test(cep)) {

          //Preenche os campos com "..." enquanto consulta webservice.
          $("#rua").val("...");
          $("#bairro").val("...");
          $("#cidade").val("...");
          $("#uf").val("...");


          //Consulta o webservice viacep.com.br/
          $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

            if (!("erro" in dados)) {
              //Atualiza os campos com os valores da consulta.
              $("#rua").val(dados.logradouro);
              $("#bairro").val(dados.bairro);
              $("#cidade").val(dados.localidade);
              $("#uf").val(dados.uf);

            } //end if.
            else {
              //CEP pesquisado não foi encontrado.
              limpa_formulário_cep();
              alert("CEP não encontrado.");
            }
          });
        } //end if.
        else {
          //cep é inválido.
          limpa_formulário_cep();
          alert("Formato de CEP inválido.");
        }
      } //end if.
      else {
        //cep sem valor, limpa formulário.
        limpa_formulário_cep();
      }
    });
  });

  </script>

@endpush
