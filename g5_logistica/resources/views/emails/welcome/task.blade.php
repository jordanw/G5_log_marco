@component('mail::message')
Seja bem vindo, {{$user->name}}!

O email de acesso é: {{$user->email}}

O username é: {{$user->username}}

Sua Senha é: {{$user->senha}}

@component('mail::button', ['url' => "http://g5logistica.soulcodigo.com.br/login"])
Clique aqui e acesse a plataforma!
@endcomponent

Obrigado por usar nosso App<br>
Atenciosamente,
G5 Logística.

@endcomponent
