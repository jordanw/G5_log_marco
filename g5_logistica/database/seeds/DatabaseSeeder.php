<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {

    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    App\User::truncate();
    App\Models\Entrada::truncate();
    App\Models\NotaEntrega::truncate();
    App\Models\Acesso::truncate();
    \App\Models\EntradaVeiculo::truncate();
    \App\Models\PecaManutencao::truncate();
    \App\Models\LocalManutencao::truncate();

    DB::table('users')->insert([
      'name'            => 'Erick Raposo',
      'email'           => 'azafilmes@gmail.com',
      'username'        => 'ericklive',
      'senha'           => '123456',
      'password'        => bcrypt('123456'),
      'telefone'        => '(98) 99195-6153',
      'nivel'           => 1,
      'image'          => null,
      'remember_token'  => str_random(10),
    ]);

    DB::table('users')->insert([
      'name'            => 'Lucas R. Ferreira',
      'email'           => 'lucasrferreira94@gmail.com',
      'username'        => 'LRF',
      'senha'           => 'c15861',
      'password'        => bcrypt('c15861'),
      'telefone'        => '(98) 98279-0870',
      'nivel'           => 3,
      'nascimento'      => '1998-01-01',
      'image'          => null,
      'remember_token'  => str_random(10),
    ]);

    DB::table('users')->insert([
      'name'            => 'WENER SANTOS',
      'email'           => 'wener.santos@hotmail.com',
      'username'        => 'WMTS',
      'senha'           => 'ceacad',
      'password'        => bcrypt('ceacad'),
      'telefone'        => '(98) 98291-0276',
      'nivel'           => 2,
      'nascimento'      => '1974-10-01',
      'image'          => null,
      'remember_token'  => str_random(10),
    ]);

    DB::table('users')->insert([
      'name'            => 'Thiago',
      'email'           => 'thiago.santos@g5sl.com.br',
      'username'        => 'TFS',
      'senha'           => '9a0400',
      'password'        => bcrypt('9a0400'),
      'telefone'        => '(98) 99116-4226',
      'nivel'           => 2,
      'nascimento'      => '1993-01-01',
      'image'          => null,
      'remember_token'  => str_random(10),
    ]);

    DB::table('users')->insert([
      'name'            => 'ADEILTON',
      'email'           => 'adeilton.almeida@g5sl.com.br',
      'username'        => 'ADE',
      'senha'           => '5dd3ec',
      'password'        => bcrypt('5dd3ec'),
      'telefone'        => '(98) 99175-5149',
      'nivel'           => 3,
      'nascimento'      => '1981-10-01',
      'image'          => null,
      'remember_token'  => str_random(10),
    ]);




    // $this->call(UsersTableSeeder::class);

    // factory('App\User', 2)->create();
    // factory('App\Models\Entrada', 4)->create();
    // factory('App\Models\Acesso', 20)->create();
    // factory('App\Models\EntradaVeiculo', 20)->create();
    // factory('App\Models\LocalManutencao', 5)->create();
    // factory('App\Models\PecaManutencao', 5)->create();

    DB::statement('SET FOREIGN_KEY_CHECKS=1;');
  }
}
