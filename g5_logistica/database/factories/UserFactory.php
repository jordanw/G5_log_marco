<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
  static $password;

  return [
    'name' => $faker->name,
    'email' => $faker->unique()->safeEmail,
    'password'        => bcrypt('123456'),
    'telefone'        => $faker->cellphoneNumber,
    'nivel'           => 1,
    'image'          => null,
    'remember_token'  => str_random(10),
  ];
});



$factory->define(App\Models\EntradaVeiculo::class, function (Faker $faker) {
  static $password;

  return [
    // 'image'           =>null,
    'user_id'         =>App\User::all()->random()->id,
    'city'            =>$faker->cityPrefix,
    'model_year'      =>$faker->year($max = 'now') ,
    'plate'           =>$faker->randomNumber($nbDigits = 7, $strict = false),
    'color'           =>$faker->colorName,
    // 'status_code'     =>null,
    'brand'           =>$faker->word,
    // 'return_code'     =>null,
    'date'            =>$faker->dateTimeAD($max = 'now', $timezone = 'America/Fortaleza'),
    'state'           =>$faker->stateAbbr,
    // 'chassis'         =>null,
    'year'            =>$faker->year($max = 'now') ,
    // 'return_message'  =>null,
    'model'           =>$faker->year($max = 'now'),
    'status_message'  =>$faker->text($maxNbChars = 10),
    'responsavel'     =>App\User::all()->random()->name,
    'observacao'      =>$faker->text($maxNbChars = 50),
    'ativo'           =>1,
  ];
});


$factory->define(App\Models\LocalManutencao::class, function (Faker $faker) {
  static $password;

  return [
    // 'veiculo_id'        =>App\Models\EntradaVeiculo::all()->random()->id,
    'local_manutencao'  => $faker->randomElement($array = array ('Porto','Loja')),
    'criado_por'        =>App\User::all()->random()->name,

  ];
});
$factory->define(App\Models\PecaManutencao::class, function (Faker $faker) {
  static $password;

  return [

    // 'veiculo_id'        =>App\Models\EntradaVeiculo::all()->random()->id,
    'peca_manutencao'   =>$faker->randomElement($array = array ('Correia Quebrada','Junta Tudo','Freio','Direcao')),
    'criado_por'        =>App\User::all()->random()->name,

  ];
});

$factory->define(App\Models\Entrada::class, function (Faker $faker) {
  static $password;

  return [

    'nome'          => $faker->name,
    'image'         => null,
    'user_id'       => App\User::all()->random()->id,
    'cpf'           => $faker->cpf,
    'identidade'    => $faker->cnpj,
    'telefone'      => $faker->cellphoneNumber,
    'sexo'          => $faker->randomElement($array = array ('M','F')),
    'tipo'          => $faker->randomElement($array = array ('Física','Jurídica')),
    'responsavel'   => App\User::all()->random()->name,
    'cnpj'          => $faker->cnpj,
    'email'         => $faker->unique()->safeEmail,
    'ativo'         => 1,
    'created_at'    => $faker->dateTimeBetween($startDate = '-6 months', $endDate = 'now', $timezone = date_default_timezone_get()),

  ];
});
$factory->define(App\Models\NotaEntrega::class, function (Faker $faker) {
  static $password;

  return [
    'entrada_id'    => App\Models\Entrada::all()->random()->id,
    'qtdade'        => $faker->numberBetween($min = 5, $max = 50),
    'date'          => $faker->dateTimeBetween($startDate = '-6 months', $endDate = 'now', $timezone = date_default_timezone_get()),
    'titulo'        => $faker->jobTitle,
    'autor'         => $faker->company,
    'isbn'          => $faker->ean8,
    'user_cadastrou'=> App\User::all()->random()->name,
    'percent'       => $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 40),
    'valor_unit'    => $faker->randomFloat($nbMaxDecimals = 2, $min = 15, $max = 45),
    'valor_percent' => $faker->numberBetween($min = 1, $max = 40),
    'valor_pvp'     => $faker->numberBetween($min = 50, $max = 100),
    'valor_total'   => $faker->numberBetween($min = 200, $max = 400),
    'created_at'    => $faker->dateTime($max = 'now', $timezone = date_default_timezone_get()),
  ];


});
$factory->define(App\Models\Acesso::class, function (Faker $faker) {
  static $password;

  return [
    'entrada_id'            => App\Models\Entrada::all()->random()->id,
    'tempo_estimado'        => $faker->numberBetween($min = 5, $max = 50),
    'local_acesso'          => $faker->randomElement($array = array ('Bloco I','Bloco II')),
    'local_especifico'          => $faker->randomElement($array = array (
      '101',
      '102',
      '103',
      '104',
      '201',
      '202',
      '203',
      '204',
      '301',
      '302',
      '303',
      '304',
      '401',
      '402',
      '403',
      '404',
      '501',
      '502',
      '503',
      '504',
      'Condomínio'
    )),
    'tipo_fornecedor'       => $faker->randomElement($array = array (
      'Água',
      'Amigo',
      'Delivery',
      'Dedetização',
      'Família',
      'Gás',
      'Justiça',
      'Loja',
      'Médico',
      'Montador',
      'Mudança',
      'Saúde',
      'Outros'
    )),
    'tempo_estimado'        => $faker->randomElement($array = array (
      'até 10 minutos',
      'até 30 minutos',
      'até 60 minutos',
      'até 5 horas',
      'não sabe / não informou'
    )),
    'concedido_acesso_por'  => App\User::all()->random()->name,
    'date_concedido_acesso' => $faker->dateTimeBetween($startDate = '-6 months', $endDate = 'now', $timezone = date_default_timezone_get()),
    'concluido_acesso_por'  => App\User::all()->random()->name,
    'date_concluido_acesso' => $faker->dateTimeBetween($startDate = '-6 months', $endDate = 'now', $timezone = date_default_timezone_get()),
    'created_at'            => $faker->dateTime($max = 'now', $timezone = date_default_timezone_get()),
    'ativo'                 => 1,
  ];


});
