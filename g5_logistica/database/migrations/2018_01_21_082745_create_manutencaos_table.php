<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManutencaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manutencaos', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('entrada_veiculo_id')->unsigned();

          $table->foreign('entrada_veiculo_id')
              ->references('id')
              ->on('entrada_veiculos');

          $table->string('os_num')->nullable();
          $table->string('falha')->nullable();
          $table->string('local')->nullable();
          $table->string('tempo_estimado');
          $table->string('tipo_manutencao');
          $table->string('concedido_acesso_por');
          $table->text('observacao')->nullable();
          $table->string('date_concedido_acesso');
          $table->string('concluido_acesso_por')->nullable();
          $table->text('comentarios')->nullable();
          $table->string('date_concluido_acesso')->nullable();
          $table->boolean('ativo')->default(1);
          $table->tinyInteger('ativo_view');
          $table->timestamps();
          $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manutencaos');
    }
}
