<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlmoxarifadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('almoxarifados', function (Blueprint $table) {
            $table->increments('id');
            $table->date('_date');
            $table->string('produto', 255);
            $table->text('nota_fiscal');
            $table->string('responsavel', 255);
            $table->decimal('custo_unitario', 7, 2);
            $table->integer('quantidade');
            $table->string('fornecedor', 255);
            $table->integer('estoque_estimado');
            $table->date('data_de_entrega');
            $table->decimal('total_de_compra', 20,2);
            $table->decimal('total_de_saida',10,2);
            $table->string('numero_os', 10);
            $table->timestamps();

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('almoxarifado');
    }
}
