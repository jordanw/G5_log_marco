<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcessosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acessos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('entrada_id')->unsigned();
            $table->foreign('entrada_id')
                ->references('id')
                ->on('entradas');
            $table->string('local_acesso');
            $table->string('local_especifico');
            $table->string('tipo_fornecedor');
            $table->string('tempo_estimado');
            $table->text('observacao')->nullable();
            $table->string('concedido_acesso_por');
            $table->string('date_concedido_acesso');
            $table->string('concluido_acesso_por')->nullable();
            $table->string('date_concluido_acesso')->nullable();
            $table->boolean('ativo')->default(1);
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acessos');
    }
}
