<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('users', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name', 155)->unique();
      $table->string('email', 155);
      $table->string('username', 155)->unique();
      $table->string('password');
      $table->string('senha');
      $table->string('telefone');
      $table->integer('status')->default(1);
      $table->integer('nivel');
      $table->boolean('send_mail')->default(0);
      $table->string('image')->nullable();
      $table->string('nascimento')->nullable();
      $table->rememberToken();
      $table->timestamps();
      $table->engine = 'InnoDB';
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('users');
  }
}
