<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotaEntregasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nota_entregas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('qtdade');
            $table->string('date');
            $table->string('nf')->nullable();
            $table->integer('entrada_id')->unsigned();
            $table->foreign('entrada_id')
                ->references('id')
                ->on('entradas')
                ->onDelete('cascade');
            $table->string('titulo', 150);
            $table->string('autor', 100);
            $table->string('isbn', 80);
            $table->string('user_cadastrou', 50);
            $table->float('valor_pvp', 20);
            $table->float('percent', 10);
            $table->float('valor_unit', 10);
            $table->float('valor_percent', 10);
            $table->float('valor_total', 25);
            $table->boolean('ativo')->default(1);
            $table->string('prestado_por', 50)->nullable();
            $table->string('date_prestado')->nullable();
            $table->string('restaurado_por', 50)->nullable();
            $table->string('date_restaurado')->nullable();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nota_entregas');
    }
}
