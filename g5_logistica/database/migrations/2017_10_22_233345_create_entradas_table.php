<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntradasTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('entradas', function (Blueprint $table) {
      $table->increments('id');
      $table->string('nome', 150);
      $table->string('image')->nullable();
      $table->integer('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users');
      $table->string('cpf', 50)->nullable();
      $table->string('identidade', 20)->nullable();
      $table->string('telefone', 20)->nullable();
      $table->string('sexo', 1);
      $table->string('tipo', 50);
      $table->string('responsavel', 150)->nullable();
      $table->string('cnpj', 50)->nullable();
      $table->string('email', 150)->nullable();
      $table->boolean('ativo')->default(1);
      $table->timestamps();
      $table->softDeletes();
      $table->engine = 'InnoDB';
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('entradas');
  }
}
