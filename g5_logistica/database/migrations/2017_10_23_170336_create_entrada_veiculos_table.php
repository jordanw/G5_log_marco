<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntradaVeiculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entrada_veiculos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            $table->string('city', 80)->nullable();
            $table->string('model_year', 80)->nullable();
            $table->string('plate', 50);
            $table->string('color', 20)->nullable();
            $table->string('status_code', 150)->nullable();
            $table->string('brand', 80)->nullable();
            $table->string('return_code', 80)->nullable();
            $table->string('date', 80)->nullable();
            $table->string('state', 80)->nullable();
            $table->string('chassis', 150)->nullable();
            $table->string('year', 80)->nullable();
            $table->string('return_message', 150)->nullable();
            $table->string('model', 150)->nullable();
            $table->string('status_message', 150)->nullable();
            $table->string('responsavel', 150);
            $table->text('observacao')->nullable();
            $table->boolean('ativo')->default(1);
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entrada_veiculos');
    }
}
