<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVeiculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('veiculos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('entrada_veiculo_id')->unsigned();
            $table->foreign('entrada_veiculo_id')
                ->references('id')
                ->on('entrada_veiculos');
            $table->string('os_num')->nullable();
            $table->text('falhas')->nullable();
            $table->string('tempo_estimado');
            $table->string('concedido_acesso_por');
            $table->text('observacao')->nullable();
            $table->string('date_concedido_acesso');
            $table->string('concluido_acesso_por')->nullable();
            $table->text('comentarios')->nullable();
            $table->string('date_concluido_acesso')->nullable();
            $table->boolean('ativo')->default(1);
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('veiculos');
    }
}
